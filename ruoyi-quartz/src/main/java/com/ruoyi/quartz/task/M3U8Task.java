package com.ruoyi.quartz.task;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.http.HttpUtils;
import com.ruoyi.core.domain.CmsBroadcastInfo;
import com.ruoyi.core.domain.vo.StreamVo;
import com.ruoyi.core.mapper.CmsBroadcastInfoMapper;
import com.ruoyi.core.service.ICmsApkInfoService;
import com.ruoyi.core.service.IUploadFile2YunService;
import com.ruoyi.core.utils.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component("m3u8Task")
public class M3U8Task {
    @Autowired
    private CmsBroadcastInfoMapper cmsBroadcastInfoMapper;
    @Autowired
    private ICmsApkInfoService cmsApkInfoService;
    @Autowired
    private IUploadFile2YunService uploadFile2YunService;
    @Autowired
    private RedisCache redisCache;


    public void  cmsGlassesBroadcast(){

        String loaclPath = RuoYiConfig.getDownloadPath();
        String  OSS_PATH="broadcast";

        String s = HttpUtils.sendGet("https://live.fanmingming.com/tv/m3u/global.m3u");
        System.out.println(s);
        s= s.replaceAll("#EXT","\\\n#EXT").replaceAll("http:","\nhttp:");
        String m3u8Content = s;
//        m3u8Content="#EXTINF:-1 tvg-id=\"CCTV1\" tvg-name=\"CCTV1\" tvg-logo=\"https://live.fanmingming.com/tv/CCTV1.png\" group-title=\"央视\",CCTV-1综合\n" +
//                "https://cntv.sbs/tv?auth=230830&id=cctv1\n";
        String regex = "#EXTINF:-1 tvg-id=\"([^\"]+)\" tvg-name=\"([^\"]+)\" tvg-logo=\"([^\"]+)\".*\n(http[^\\s]+)";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(m3u8Content);

        while (matcher.find()) {
            String tvgId = matcher.group(1);
            String tvgName = matcher.group(2);
            String tvgLogo = matcher.group(3);
            String playUrl = matcher.group(4);

            System.out.print("     tvgId: " + tvgId);
            System.out.print("     TVG Name: " + tvgName);
            System.out.print("     tvgLogo: " + tvgLogo);
            System.out.print("      Play URL: " + playUrl);


            try{
                CmsBroadcastInfo cmsBroadcastInfo = new CmsBroadcastInfo();
                cmsBroadcastInfo.setTitle(tvgName);
                List<CmsBroadcastInfo> cmsBroadcastList = cmsBroadcastInfoMapper.selectCmsBroadcastInfoList(cmsBroadcastInfo);
                if( ! cmsBroadcastList.isEmpty()){
                    String streaming = cmsBroadcastList.get(0).getStreaming();
                    List<StreamVo> streamVos = JSON.parseArray(streaming, StreamVo.class);
                    streamVos.get(0).setUrl(playUrl);
                    cmsBroadcastList.get(0).setStreaming(JSON.toJSONString(streamVos));
                    cmsBroadcastInfoMapper.updateCmsBroadcastInfo(cmsBroadcastList.get(0));

                    removeRedis(cmsBroadcastList.get(0).getId());
                }else{
                    //添加一个数据信息
                    List<StreamVo> streamVos = new ArrayList<>();
                    StreamVo streamVo =new StreamVo();
                    streamVo.setUrl(playUrl);
                    streamVos.add(streamVo);

                    cmsBroadcastInfo.setSubTitle(tvgId);
                    cmsBroadcastInfo.setStreaming(JSON.toJSONString(streamVos));
                    cmsBroadcastInfo.setCreateBy("emdor");
                    cmsBroadcastInfo.setCreateTime(DateUtils.getNowDate());

                    cmsBroadcastInfoMapper.insertCmsBroadcastInfo(cmsBroadcastInfo);
                    //  tvgLogo
                    Long id = cmsBroadcastInfo.getId();
                    boolean b = FileUtil.downloadFromIntNet(tvgLogo, id + ".png", loaclPath);
                    if(b){
                        String imageUrl= uploadFile2YunService.uploadFile2Oss(String.valueOf(cmsBroadcastInfo.getId()), loaclPath+id+".png", OSS_PATH);
                        if(StringUtils.isNotEmpty(imageUrl)){
                            cmsBroadcastInfo.setImage(imageUrl);
                            cmsBroadcastInfoMapper.updateCmsBroadcastInfo(cmsBroadcastInfo);
                        }
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * 删除缓存
     * @param id
     */
    private void removeRedis(Long id){
        String broadcastKey = RedisKey.getBroadcastKey(id);
        System.out.println("删除缓存："+broadcastKey);
        redisCache.deleteObject(broadcastKey);
    }
}
