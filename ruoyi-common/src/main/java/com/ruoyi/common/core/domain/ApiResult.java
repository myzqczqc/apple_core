package com.ruoyi.common.core.domain;

import com.ruoyi.common.constant.HttpStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "ApiResult", description = "ApiResult实体")
public class ApiResult<T> {

    /** 状态码 */
    @ApiModelProperty("状态码")
    private    Integer code;

    /** 返回内容 */
    @ApiModelProperty("返回信息")
    private   String msg ;

    /** 数据对象 */
    @ApiModelProperty("返回数据")
    private   T data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public ApiResult() {
    }

    public ApiResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ApiResult(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static ApiResult success()
    {
        return ApiResult.success("成功");
    }

    /**
     * 返回成功数据
     *
     * @return 成功消息
     */
    public static ApiResult success(Object data)
    {
        return ApiResult.success("成功", data);
    }
    public static ApiResult success(String msg, Object data)
    {
        return new ApiResult(HttpStatus.SUCCESS, msg, data);
    }

    public static ApiResult error()
    {
        return ApiResult.error("失败");
    }

    /**
     * 返回错误消息
     *
     * @param msg 返回内容
     * @return 警告消息
     */
    public static ApiResult error(String msg)
    {
        return ApiResult.error(msg, null);
    }
    public static ApiResult error(String msg, Object data)
    {
        return new ApiResult(HttpStatus.ERROR, msg, data);
    }

    public static ApiResult error(Integer code,String msg)
    {
        return new ApiResult(code, msg, null);
    }

    @Override
    public String toString() {
        return "ApiResult{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
