package com.ruoyi.common.core.domain;

/**
 * 设备调用的接口，header中传递的数据信息
 */
public class RequestHeader {
    //合作商id，用户表的sn（必传）
    private String parentId ;

    //渠道的id，渠道的sn（必传）
    private String chennelId ;

    //手机助手登录之后生成的id
    private String userId;

    //精度
    private String accuracy ;

    //维度
    private String dimension;


    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getChennelId() {
        return chennelId;
    }

    public void setChennelId(String chennelId) {
        this.chennelId = chennelId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }
}
