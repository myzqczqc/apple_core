package com.ruoyi.common.utils;


import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.PhoneUtil;
import cn.hutool.core.util.RandomUtil;
import com.ruoyi.common.core.redis.RedisCache;

import java.util.Date;

/**
 * 处理并记录日志文件
 *
 * @author ruoyi
 */
public class LogUtils {
    public static String getBlock(Object msg)
    {
        if (msg == null)
        {
            msg = "";
        }
        return "[" + msg.toString() + "]";
    }
    public static void main(String[] args) {
//        System.out.println(StringUtils.join("a","b"));
//        System.out.println(RandomUtil.randomNumbers(6));
        System.out.println(PhoneUtil.isPhone("11931361229"));
    }
}
