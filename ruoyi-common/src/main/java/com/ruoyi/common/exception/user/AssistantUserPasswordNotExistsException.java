package com.ruoyi.common.exception.user;

/**
 * 用户密码未设置异常类
 *
 * @author ruoyi
 */
public class AssistantUserPasswordNotExistsException extends UserException
{
    private static final long serialVersionUID = 1L;

    public AssistantUserPasswordNotExistsException()
    {
        super("assistantuser.password.not.exists", null);
    }
}
