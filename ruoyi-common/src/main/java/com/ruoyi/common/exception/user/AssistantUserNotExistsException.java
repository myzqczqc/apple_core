package com.ruoyi.common.exception.user;

/**
 * 用户不存在异常类
 *
 * @author ruoyi
 */
public class AssistantUserNotExistsException extends UserException
{
    private static final long serialVersionUID = 1L;

    public AssistantUserNotExistsException()
    {
        super("assistantuser.not.exists", null);
    }
}
