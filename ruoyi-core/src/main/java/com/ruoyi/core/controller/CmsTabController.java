package com.ruoyi.core.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.core.domain.CmsTab;
import com.ruoyi.core.service.ICmsTabService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * tab信息Controller
 * 
 * @author zqc
 * @date 2023-11-08
 */
@RestController
@RequestMapping("/core/tab")
public class CmsTabController extends BaseController
{
    @Autowired
    private ICmsTabService cmsTabService;

    /**
     * 查询tab信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CmsTab cmsTab)
    {
        //如果获取的是默认渠道的数据，返回共享账号的数据
        if(cmsTab.getChannelId().equals(UserConstants.CHANNEL_ID)){
            cmsTab.setCreateBy(UserConstants.SHARE_USER);
        }else{
            cmsTab.setCreateBy(getUsername());
        }

        startPage();
        List<CmsTab> list = cmsTabService.selectCmsTabList(cmsTab);
        return getDataTable(list);
    }

    /**
     * 导出tab信息列表
     */
    @Log(title = "tab信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsTab cmsTab)
    {
        List<CmsTab> list = cmsTabService.selectCmsTabList(cmsTab);
        ExcelUtil<CmsTab> util = new ExcelUtil<CmsTab>(CmsTab.class);
        util.exportExcel(response, list, "tab信息数据");
    }

    /**
     * 获取tab信息详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cmsTabService.selectCmsTabById(id));
    }

    /**
     * 新增tab信息
     */
    @Log(title = "tab信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsTab cmsTab)
    {
        cmsTab.setCreateBy(getUsername());
        cmsTab.setCreateTime(DateUtils.getNowDate());
        cmsTab.setLoginUser(getLoginUser());

        int i = cmsTabService.insertCmsTab(cmsTab);
        if(i==-1){
            return AjaxResult.error("不能添加");
        }
        return toAjax(i);
    }

    /**
     * 修改tab信息
     */
    @Log(title = "tab信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsTab cmsTab)
    {
        cmsTab.setUpdateBy(getUsername());
        cmsTab.setUpdateTime(DateUtils.getNowDate());
        cmsTab.setLoginUser(getLoginUser());
        int i = cmsTabService.updateCmsTab(cmsTab);
        if(i==-1){
            return AjaxResult.error("不能修改");
        }
        return toAjax(i);
    }

    /**
     * 删除tab信息
     */
    @Log(title = "tab信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        return toAjax(cmsTabService.deleteCmsTabById(id));
    }
}
