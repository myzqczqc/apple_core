package com.ruoyi.core.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.core.domain.CmsRecommend;
import com.ruoyi.core.service.ICmsRecommendService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * tab的recommend信息Controller
 * 
 * @author zqc
 * @date 2023-11-08
 */
@RestController
@RequestMapping("/core/recommend")
public class CmsRecommendController extends BaseController
{
    @Autowired
    private ICmsRecommendService cmsRecommendService;

    /**
     * 查询tab的recommend信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CmsRecommend cmsRecommend)
    {
        startPage();
        List<CmsRecommend> list = cmsRecommendService.selectCmsRecommendList(cmsRecommend);
        return getDataTable(list);
    }

    /**
     * 导出tab的recommend信息列表
     */
    @Log(title = "tab的recommend信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsRecommend cmsRecommend)
    {
        List<CmsRecommend> list = cmsRecommendService.selectCmsRecommendList(cmsRecommend);
        ExcelUtil<CmsRecommend> util = new ExcelUtil<CmsRecommend>(CmsRecommend.class);
        util.exportExcel(response, list, "tab的recommend信息数据");
    }

    /**
     * 获取tab的recommend信息详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cmsRecommendService.selectCmsRecommendById(id));
    }

    /**
     * 新增tab的recommend信息
     */
    @Log(title = "tab的recommend信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsRecommend cmsRecommend) throws IOException {
        cmsRecommend.setCreateBy(getUsername());
        cmsRecommend.setCreateTime(DateUtils.getNowDate());
        int i = cmsRecommendService.insertCmsRecommend(cmsRecommend);
        if(i==-1){
            return AjaxResult.error("不能添加");
        }
        return toAjax(i);
    }

    /**
     * 修改tab的recommend信息
     */
    @Log(title = "tab的recommend信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsRecommend cmsRecommend) throws IOException {
        cmsRecommend.setUpdateBy(getUsername());
        cmsRecommend.setUpdateTime(DateUtils.getNowDate());
        int i = cmsRecommendService.updateCmsRecommend(cmsRecommend);
        if(i==-1){
            return AjaxResult.error("不能修改");
        }
        return toAjax(i);
    }

    /**
     * 删除tab的recommend信息
     */
    @Log(title = "tab的recommend信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        int i = cmsRecommendService.deleteCmsRecommendById(id, getUsername());
        if(i==-1){
            return AjaxResult.error("不能删除");
        }
        return toAjax(i);
    }
    /**
     * 删除tab的recommend信息
     */
    @Log(title = "tab的recommend信息", businessType = BusinessType.UPDATE)
    @PostMapping("/updateSeq/{id}/{seq}")
    public AjaxResult updateSeq(@PathVariable Long id,@PathVariable Integer seq)
    {
        int i = cmsRecommendService.updateSeq(id, seq, getUsername());
        if(i==-1){
            return AjaxResult.error("不能修改顺序");
        }
        return toAjax(i);
    }
}
