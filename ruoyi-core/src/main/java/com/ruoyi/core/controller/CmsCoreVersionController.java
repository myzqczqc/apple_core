package com.ruoyi.core.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.core.domain.CmsCoreVersion;
import com.ruoyi.core.service.ICmsCoreVersionService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * core应用历史版本信息Controller
 * 
 * @author zqc
 * @date 2023-11-23
 */
@RestController
@RequestMapping("/core/version")
public class CmsCoreVersionController extends BaseController
{
    @Autowired
    private ICmsCoreVersionService cmsCoreVersionService;

    /**
     * 查询core应用历史版本信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CmsCoreVersion cmsCoreVersion)
    {
        startPage();
        List<CmsCoreVersion> list = cmsCoreVersionService.selectCmsCoreVersionList(cmsCoreVersion);
        return getDataTable(list);
    }

    /**
     * 导出core应用历史版本信息列表
     */
    @Log(title = "core应用历史版本信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsCoreVersion cmsCoreVersion)
    {
        List<CmsCoreVersion> list = cmsCoreVersionService.selectCmsCoreVersionList(cmsCoreVersion);
        ExcelUtil<CmsCoreVersion> util = new ExcelUtil<CmsCoreVersion>(CmsCoreVersion.class);
        util.exportExcel(response, list, "core应用历史版本信息数据");
    }

    /**
     * 获取core应用历史版本信息详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cmsCoreVersionService.selectCmsCoreVersionById(id));
    }

    /**
     * 新增core应用历史版本信息
     */
    @Log(title = "core应用历史版本信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsCoreVersion cmsCoreVersion) throws IOException {
        cmsCoreVersion.setCreateTime(DateUtils.getNowDate());
        cmsCoreVersion.setCreateBy(getUsername());
        // 校验包名
        int typeId = cmsCoreVersion.getTypeId().intValue();
        if (typeId <= 5 && !Constants.CORE_PACKAGE_NAME[typeId].equals(cmsCoreVersion.getPackageName())) {
            return error("上传apk包名与预期不符，请检查后重新上传");
        }
        return toAjax(cmsCoreVersionService.insertCmsCoreVersion(cmsCoreVersion));
    }

    /**
     * 修改core应用历史版本信息
     */
    @Log(title = "core应用历史版本信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsCoreVersion cmsCoreVersion) throws IOException {
        cmsCoreVersion.setUpdateTime(DateUtils.getNowDate());
        cmsCoreVersion.setUpdateBy(getUsername());
        return toAjax(cmsCoreVersionService.updateCmsCoreVersion(cmsCoreVersion));
    }

    /**
     * 删除core应用历史版本信息
     */
    @Log(title = "core应用历史版本信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cmsCoreVersionService.deleteCmsCoreVersionByIds(ids));
    }
}
