package com.ruoyi.core.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.core.domain.CmsMtype;
import com.ruoyi.core.service.ICmsMtypeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 影视类型Controller
 * 
 * @author zqc
 * @date 2023-11-06
 */
@RestController
@RequestMapping("/core/mtype")
public class CmsMtypeController extends BaseController
{
    @Autowired
    private ICmsMtypeService cmsMtypeService;

    /**
     * 查询影视类型列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CmsMtype cmsMtype)
    {
        startPage();
        List<CmsMtype> list = cmsMtypeService.selectCmsMtypeList(cmsMtype);
        return getDataTable(list);
    }

    /**
     * 导出影视类型列表
     */
    @Log(title = "影视类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsMtype cmsMtype)
    {
        List<CmsMtype> list = cmsMtypeService.selectCmsMtypeList(cmsMtype);
        ExcelUtil<CmsMtype> util = new ExcelUtil<CmsMtype>(CmsMtype.class);
        util.exportExcel(response, list, "影视类型数据");
    }

    /**
     * 获取影视类型详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cmsMtypeService.selectCmsMtypeById(id));
    }

    /**
     * 新增影视类型
     */
    @Log(title = "影视类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsMtype cmsMtype)
    {
        return toAjax(cmsMtypeService.insertCmsMtype(cmsMtype));
    }

    /**
     * 修改影视类型
     */
    @Log(title = "影视类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsMtype cmsMtype)
    {
        return toAjax(cmsMtypeService.updateCmsMtype(cmsMtype));
    }

    /**
     * 删除影视类型
     */
    @Log(title = "影视类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        return toAjax(cmsMtypeService.deleteCmsMtypeById(id));
    }
}
