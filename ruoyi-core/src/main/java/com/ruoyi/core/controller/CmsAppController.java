package com.ruoyi.core.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.core.domain.CmsApp;
import com.ruoyi.core.service.ICmsAppService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * app配置Controller
 *
 * @author zqc
 * @date 2023-11-17
 */
@RestController
@RequestMapping("/core/cmsApp")
public class CmsAppController extends BaseController
{
    @Autowired
    private ICmsAppService cmsAppService;

    /**
     * 查询app配置列表
     */
    @PreAuthorize("@ss.hasPermi('core:cmsApp:list')")
    @GetMapping("/list")
    public TableDataInfo list(CmsApp cmsApp)
    {
        startPage();
        List<CmsApp> list = cmsAppService.selectCmsAppList(cmsApp);
        return getDataTable(list);
    }

    /**
     * 导出app配置列表
     */
    @Log(title = "app配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsApp cmsApp)
    {
        List<CmsApp> list = cmsAppService.selectCmsAppList(cmsApp);
        ExcelUtil<CmsApp> util = new ExcelUtil<CmsApp>(CmsApp.class);
        util.exportExcel(response, list, "app配置数据");
    }

    /**
     * 获取app配置详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cmsAppService.selectCmsAppById(id));
    }

    /**
     * 新增app配置
     */
    @Log(title = "app配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsApp cmsApp)
    {
        cmsApp.setCreateBy(getUsername());
        cmsApp.setCreateTime(DateUtils.getNowDate());
        cmsApp.setLoginUser(getLoginUser());
        int i = cmsAppService.insertCmsApp(cmsApp);
        if(i==-1){
            return AjaxResult.error("不能添加");
        }
        return toAjax(i);
    }

    /**
     * 修改app配置
     */
    @Log(title = "app配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsApp cmsApp)
    {
//        return toAjax(cmsAppService.updateCmsApp(cmsApp));
        return null;
    }
    @Log(title = "app配置", businessType = BusinessType.UPDATE)
    @PutMapping("/updateRelations")
    public AjaxResult updateRelations(@RequestBody List<CmsApp> entities) {
        int i = cmsAppService.updateCmsApps(entities, getLoginUser());
        if(i==-1){
            return AjaxResult.error("不能修改");
        }
        return toAjax(i);
    }
    /**
     * 删除app配置
     */
    @Log(title = "app配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable Long id){
        int i = cmsAppService.deleteCmsAppById(id, getLoginUser());
        if(i==-1){
            return AjaxResult.error("不能删除");
        }
        return toAjax(i);
    }
}
