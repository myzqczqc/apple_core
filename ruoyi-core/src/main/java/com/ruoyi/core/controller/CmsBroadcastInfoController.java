package com.ruoyi.core.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.core.domain.vo.CmsBroadcastInfoVo;
import com.ruoyi.core.enums.ContentTypeEnums;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.core.domain.CmsBroadcastInfo;
import com.ruoyi.core.service.ICmsBroadcastInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 直播流信息Controller
 * 
 * @author zqc
 * @date 2023-11-06
 */
@RestController
@RequestMapping("/core/broadcast")
public class CmsBroadcastInfoController extends BaseController
{
    @Autowired
    private ICmsBroadcastInfoService cmsBroadcastInfoService;

    /**
     * 查询直播流信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CmsBroadcastInfo cmsBroadcastInfo)
    {
        if(ContentTypeEnums.SHARE.getType()==cmsBroadcastInfo.getSource()){
            //公共资源
            cmsBroadcastInfo.setTypeId(ContentTypeEnums.SHARE.getType());
        }else if(ContentTypeEnums.PRIVATE.getType()==cmsBroadcastInfo.getSource()){
            //我的资源
            cmsBroadcastInfo.setCreateBy(getUsername());
        }else if(ContentTypeEnums.SEARCH.getType()==cmsBroadcastInfo.getSource()){
            //搜索
            cmsBroadcastInfo.setCreateBy(getUsername());
        }

        startPage();
        List<CmsBroadcastInfo> list = cmsBroadcastInfoService.selectCmsBroadcastInfoList(cmsBroadcastInfo);
        return getDataTable(list);
    }

    /**
     * 导出直播流信息列表
     */
    @Log(title = "直播流信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsBroadcastInfo cmsBroadcastInfo)
    {
        List<CmsBroadcastInfo> list = cmsBroadcastInfoService.selectCmsBroadcastInfoList(cmsBroadcastInfo);
        ExcelUtil<CmsBroadcastInfo> util = new ExcelUtil<CmsBroadcastInfo>(CmsBroadcastInfo.class);
        util.exportExcel(response, list, "直播流信息数据");
    }

    /**
     * 获取直播流信息详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cmsBroadcastInfoService.selectCmsBroadcastInfoById(id).broadcast2Vo());
    }

    /**
     * 新增直播流信息
     */
    @Log(title = "直播流信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsBroadcastInfoVo cmsBroadcastInfoVo) throws IOException {
        CmsBroadcastInfo cmsBroadcastInfo = cmsBroadcastInfoVo.vo2broadcast();
        cmsBroadcastInfo.setCreateBy(getUsername());
        cmsBroadcastInfo.setCreateTime(DateUtils.getNowDate());
        // 如果是emdor用户的数据，默认是共享的
        if (UserConstants.SHARE_USER.equals(getUsername())) {
            cmsBroadcastInfo.setTypeId(1);
        }
        return toAjax(cmsBroadcastInfoService.insertCmsBroadcastInfo(cmsBroadcastInfo));
    }

    /**
     * 修改直播流信息
     */
    @Log(title = "直播流信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsBroadcastInfoVo cmsBroadcastInfoVo) throws IOException {
        CmsBroadcastInfo cmsBroadcastInfo = cmsBroadcastInfoVo.vo2broadcast();
        cmsBroadcastInfo.setUpdateBy(getUsername());
        cmsBroadcastInfo.setUpdateTime(DateUtils.getNowDate());
        // 如果是emdor用户的数据，默认是共享的
        if (UserConstants.SHARE_USER.equals(getUsername())) {
            cmsBroadcastInfo.setTypeId(1);
        }
        int i = cmsBroadcastInfoService.updateCmsBroadcastInfo(cmsBroadcastInfo);
        if(i==-1){
            return AjaxResult.error("不能修改");
        }
        return toAjax(i);
    }

    /**
     * 删除直播流信息
     */
    @Log(title = "直播流信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{id}")
    @Deprecated
    public AjaxResult remove(@PathVariable Long id)
    {
        return toAjax(cmsBroadcastInfoService.deleteCmsBroadcastInfoById(id));
    }
}
