package com.ruoyi.core.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.core.domain.CmsBanner;
import com.ruoyi.core.service.ICmsBannerService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * tab的banner信息Controller
 * 
 * @author zqc
 * @date 2023-11-08
 */
@RestController
@RequestMapping("/core/banner")
public class CmsBannerController extends BaseController
{
    @Autowired
    private ICmsBannerService cmsBannerService;

    /**
     * 查询tab的banner信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CmsBanner cmsBanner)
    {
        startPage();
        List<CmsBanner> list = cmsBannerService.selectCmsBannerList(cmsBanner);
        return getDataTable(list);
    }

    /**
     * 导出tab的banner信息列表
     */
    @Log(title = "tab的banner信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsBanner cmsBanner)
    {
        List<CmsBanner> list = cmsBannerService.selectCmsBannerList(cmsBanner);
        ExcelUtil<CmsBanner> util = new ExcelUtil<CmsBanner>(CmsBanner.class);
        util.exportExcel(response, list, "tab的banner信息数据");
    }

    /**
     * 获取tab的banner信息详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cmsBannerService.selectCmsBannerById(id));
    }

    /**
     * 新增tab的banner信息
     */
    @Log(title = "tab的banner信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsBanner cmsBanner) throws IOException {
        cmsBanner.setCreateBy(getUsername());
        cmsBanner.setCreateTime(DateUtils.getNowDate());
        int i = cmsBannerService.insertCmsBanner(cmsBanner);
        if(i==-1){
            return AjaxResult.error("不能添加");
        }
        if(i==-2){
            return AjaxResult.error("该banner信息已添加，请勿重复添加");
        }
        return toAjax(i);
    }

    /**
     * 修改tab的banner信息
     */
    @Log(title = "tab的banner信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsBanner cmsBanner) throws IOException {
        cmsBanner.setUpdateBy(getUsername());
        cmsBanner.setUpdateTime(DateUtils.getNowDate());
        int i = cmsBannerService.updateCmsBanner(cmsBanner);
        if(i==-1){
            return AjaxResult.error("不能修改");
        }
        return toAjax(i);
    }

    /**
     * 删除tab的banner信息
     */
    @Log(title = "tab的banner信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        int i = cmsBannerService.deleteCmsBannerById(id, getUsername());
        if(i==-1){
            return AjaxResult.error("不能删除");
        }
        return toAjax(i);
    }
    /**
     * 删除tab的recommend信息
     */
    @Log(title = "tab的banner信息", businessType = BusinessType.DELETE)
    @PostMapping("/updateSeq/{id}/{seq}")
    public AjaxResult updateSeq(@PathVariable Long id,@PathVariable Integer seq)
    {
        int i = cmsBannerService.updateSeq(id, seq, getUsername());
        if(i==-1){
            return AjaxResult.error("不能修改顺序");
        }
        return toAjax(i);
    }
}
