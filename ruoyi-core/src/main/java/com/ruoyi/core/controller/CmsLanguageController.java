package com.ruoyi.core.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.core.domain.CmsLanguage;
import com.ruoyi.core.service.ICmsLanguageService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 语言字典信息Controller
 * 
 * @author zqc
 * @date 2023-11-21
 */
@RestController
@RequestMapping("/core/language")
public class CmsLanguageController extends BaseController
{
    @Autowired
    private ICmsLanguageService cmsLanguageService;

    /**
     * 查询语言字典信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CmsLanguage cmsLanguage)
    {
        startPage();
        List<CmsLanguage> list = cmsLanguageService.selectCmsLanguageList(cmsLanguage);
        return getDataTable(list);
    }

    /**
     * 导出语言字典信息列表
     */
    @Log(title = "语言字典信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsLanguage cmsLanguage)
    {
        List<CmsLanguage> list = cmsLanguageService.selectCmsLanguageList(cmsLanguage);
        ExcelUtil<CmsLanguage> util = new ExcelUtil<CmsLanguage>(CmsLanguage.class);
        util.exportExcel(response, list, "语言字典信息数据");
    }

    /**
     * 获取语言字典信息详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cmsLanguageService.selectCmsLanguageById(id));
    }

    /**
     * 新增语言字典信息
     */
    @Log(title = "语言字典信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsLanguage cmsLanguage)
    {
        return toAjax(cmsLanguageService.insertCmsLanguage(cmsLanguage));
    }

    /**
     * 修改语言字典信息
     */
    @Log(title = "语言字典信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsLanguage cmsLanguage)
    {
        return toAjax(cmsLanguageService.updateCmsLanguage(cmsLanguage));
    }

    /**
     * 删除语言字典信息
     */
    @Log(title = "语言字典信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cmsLanguageService.deleteCmsLanguageByIds(ids));
    }
}
