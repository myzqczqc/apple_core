package com.ruoyi.core.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.core.domain.CmsAppType;
import com.ruoyi.core.service.ICmsAppTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * app子分类Controller
 *
 * @author zqc
 * @date 2023-11-25
 */
@RestController
@RequestMapping("/core/appType")
public class CmsAppTypeController extends BaseController {
    @Autowired
    private ICmsAppTypeService cmsAppTypeService;

    /**
     * 查询app子分类列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CmsAppType cmsAppType) {
        startPage();
        List<CmsAppType> list = cmsAppTypeService.selectCmsAppTypeList(cmsAppType);
        return getDataTable(list);
    }

    /**
     * 导出app子分类列表
     */
    @Log(title = "app子分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsAppType cmsAppType) {
        List<CmsAppType> list = cmsAppTypeService.selectCmsAppTypeList(cmsAppType);
        ExcelUtil<CmsAppType> util = new ExcelUtil<CmsAppType>(CmsAppType.class);
        util.exportExcel(response, list, "app子分类数据");
    }

    /**
     * 获取app子分类详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(cmsAppTypeService.selectCmsAppTypeById(id));
    }

    /**
     * 新增app子分类
     */
    @Log(title = "app子分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsAppType cmsAppType) {
        cmsAppType.setCreateBy(getUsername());
        cmsAppType.setCreateTime(DateUtils.getNowDate());
        return toAjax(cmsAppTypeService.insertCmsAppType(cmsAppType));
    }

    /**
     * 修改app子分类
     */
    @Log(title = "app子分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsAppType cmsAppType) {
        return toAjax(cmsAppTypeService.updateCmsAppType(cmsAppType));
    }

    /**
     * 删除app子分类
     */
    @Log(title = "app子分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable Long id) {
        return toAjax(cmsAppTypeService.deleteCmsAppTypeById(id));
    }

    @Log(title = "更新app子分类排序号", businessType = BusinessType.DELETE)
    @PostMapping("/updateSeq/{id}/{seq}")
    public AjaxResult updateSeq(@PathVariable Long id, @PathVariable Integer seq) {
        return toAjax(cmsAppTypeService.updateSeq(id, seq));
    }
}
