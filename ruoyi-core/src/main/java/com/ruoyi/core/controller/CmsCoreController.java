package com.ruoyi.core.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.core.domain.CmsCore;
import com.ruoyi.core.service.ICmsCoreService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 核心应用信息Controller
 * 
 * @author zqc
 * @date 2023-11-23
 */
@RestController
@RequestMapping("/core/core")
public class CmsCoreController extends BaseController
{
    @Autowired
    private ICmsCoreService cmsCoreService;

    /**
     * 查询核心应用信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CmsCore cmsCore)
    {
        startPage();
        List<CmsCore> list = cmsCoreService.selectCmsCoreList(cmsCore);
        return getDataTable(list);
    }

    /**
     * 导出核心应用信息列表
     */
    @Log(title = "核心应用信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsCore cmsCore)
    {
        List<CmsCore> list = cmsCoreService.selectCmsCoreList(cmsCore);
        ExcelUtil<CmsCore> util = new ExcelUtil<CmsCore>(CmsCore.class);
        util.exportExcel(response, list, "核心应用信息数据");
    }

    /**
     * 获取核心应用信息详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cmsCoreService.selectCmsCoreById(id));
    }

    /**
     * 新增核心应用信息
     */
    @Log(title = "核心应用信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsCore cmsCore)
    {
        return toAjax(cmsCoreService.insertCmsCore(cmsCore));
    }

    /**
     * 修改核心应用信息
     */
    @Log(title = "核心应用信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsCore cmsCore)
    {
        return toAjax(cmsCoreService.updateCmsCore(cmsCore));
    }

    /**
     * 删除核心应用信息
     */
    @Log(title = "核心应用信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cmsCoreService.deleteCmsCoreByIds(ids));
    }
}
