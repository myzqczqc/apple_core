package com.ruoyi.core.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.core.domain.CmsEpisode;
import com.ruoyi.core.service.ICmsEpisodeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 剧集信息Controller
 * 
 * @author zqc
 * @date 2023-11-06
 */
@RestController
@RequestMapping("/core/episode")
public class CmsEpisodeController extends BaseController
{
    @Autowired
    private ICmsEpisodeService cmsEpisodeService;

    /**
     * 查询剧集信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CmsEpisode cmsEpisode)
    {
        startPage();
        List<CmsEpisode> list = cmsEpisodeService.selectCmsEpisodeList(cmsEpisode);
        return getDataTable(list);
    }

    /**
     * 导出剧集信息列表
     */
    @Log(title = "剧集信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsEpisode cmsEpisode)
    {
        List<CmsEpisode> list = cmsEpisodeService.selectCmsEpisodeList(cmsEpisode);
        ExcelUtil<CmsEpisode> util = new ExcelUtil<CmsEpisode>(CmsEpisode.class);
        util.exportExcel(response, list, "剧集信息数据");
    }

    /**
     * 获取剧集信息详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cmsEpisodeService.selectCmsEpisodeById(id));
    }

    /**
     * 新增剧集信息
     */
    @Log(title = "剧集信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsEpisode cmsEpisode)
    {
        cmsEpisode.setCreateBy(getUsername());
        cmsEpisode.setCreateTime(DateUtils.getNowDate());
        int i = cmsEpisodeService.insertCmsEpisode(cmsEpisode);
        if(i==-1){
            return AjaxResult.error("不能添加");
        }
        return toAjax(i);
    }

    /**
     * 修改剧集信息
     */
    @Log(title = "剧集信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsEpisode cmsEpisode)
    {
        cmsEpisode.setUpdateBy(getUsername());
        cmsEpisode.setUpdateTime(DateUtils.getNowDate());
        int i = cmsEpisodeService.updateCmsEpisode(cmsEpisode);
        if(i==-1){
            return AjaxResult.error("不能修改");
        }
        return toAjax(i);
    }

    /**
     * 删除剧集信息
     */
    @Log(title = "剧集信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        int i = cmsEpisodeService.deleteCmsEpisodeById(id, getUsername());
        if(i==-1){
            return AjaxResult.error("不能删除");
        }
        return toAjax(i);
    }

    /**
     * 删除tab的recommend信息
     */
    @Log(title = "修改顺序号", businessType = BusinessType.UPDATE)
    @PostMapping("/updateSeq/{id}/{seq}")
    public AjaxResult updateSeq(@PathVariable Long id,@PathVariable Integer seq)
    {
        int i = cmsEpisodeService.updateSeq(id, seq, getUsername());
        if(i==-1){
            return AjaxResult.error("不能修改");
        }
        return toAjax(i);
    }
}
