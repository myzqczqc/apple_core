package com.ruoyi.core.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.core.domain.CmsApkInfo;
import com.ruoyi.core.domain.vo.CmsApkInfoVo;
import com.ruoyi.core.enums.ContentTypeEnums;
import com.ruoyi.core.service.ICmsApkInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * apk信息Controller
 *
 * @author zqc
 * @date 2023-11-06
 */
@RestController
@RequestMapping("/core/apkInfo")
public class CmsApkInfoController extends BaseController {
    @Autowired
    private ICmsApkInfoService cmsApkInfoService;

    /**
     * 查询apk信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CmsApkInfo cmsApkInfo) {
        if (ContentTypeEnums.SHARE.getType() == cmsApkInfo.getSource()) {
            //公共资源
            cmsApkInfo.setTypeId(ContentTypeEnums.SHARE.getType());
        } else if (ContentTypeEnums.PRIVATE.getType() == cmsApkInfo.getSource()) {
            //我的资源
            cmsApkInfo.setCreateBy(getUsername());
        } else if (ContentTypeEnums.SEARCH.getType() == cmsApkInfo.getSource()) {
            //搜索
            cmsApkInfo.setCreateBy(getUsername());
        }

        startPage();
        List<CmsApkInfo> list = cmsApkInfoService.selectCmsApkInfoList(cmsApkInfo);
        return getDataTable(list);
    }

    /**
     * 导出apk信息列表
     */
    @Log(title = "apk信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsApkInfo cmsApkInfo) {
        List<CmsApkInfo> list = cmsApkInfoService.selectCmsApkInfoList(cmsApkInfo);
        ExcelUtil<CmsApkInfo> util = new ExcelUtil<CmsApkInfo>(CmsApkInfo.class);
        util.exportExcel(response, list, "apk信息数据");
    }

    /**
     * 获取apk信息详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(cmsApkInfoService.selectCmsApkInfoById(id).vo2CmsApkInfo());
    }

    /**
     * 新增apk信息
     */
    @Log(title = "apk信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsApkInfoVo cmsApkInfoVo) throws IOException {
        cmsApkInfoVo.setCreateBy(getUsername());
        cmsApkInfoVo.setCreateTime(DateUtils.getNowDate());

        CmsApkInfo cmsApkInfo = cmsApkInfoVo.vo2CmsApkInfo();

        // 如果是emdor用户（默认渠道）上传的apk，默认是共享的
        if (UserConstants.SHARE_USER.equals(getUsername())) {
            cmsApkInfo.setTypeId(1);
        }

        int i = cmsApkInfoService.insertCmsApkInfo(cmsApkInfo);
        if (i == -1) {
            return AjaxResult.error("apk已经上传");
        }
        return toAjax(i);
    }

    /**
     * 修改apk信息
     */
    @Log(title = "apk信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsApkInfoVo cmsApkInfoVo) throws IOException {
        cmsApkInfoVo.setUpdateBy(getUsername());
        cmsApkInfoVo.setUpdateTime(DateUtils.getNowDate());

        CmsApkInfo cmsApkInfo = cmsApkInfoVo.vo2CmsApkInfo();
        // 如果是emdor用户（默认渠道）上传的apk，默认是共享的
        if (UserConstants.SHARE_USER.equals(getUsername())) {
            cmsApkInfo.setTypeId(1);
        }
        int i = cmsApkInfoService.updateCmsApkInfo(cmsApkInfo);
        if (i == -1) {
            return AjaxResult.error("apk不能修改");
        }
        return toAjax(i);
    }

    /**
     * 删除apk信息
     */
//    @PreAuthorize("@ss.hasPermi('core:apkInfo:remove')")
    @Log(title = "apk信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable Long id) {
        int i = cmsApkInfoService.deleteCmsApkInfoById(id, getUsername());
        if (i == -1) {
            return AjaxResult.error("apk不能删除");
        }
        return toAjax(i);
    }
}
