package com.ruoyi.core.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.core.domain.CmsAssistantUser;
import com.ruoyi.core.service.ICmsAssistantUserService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 手机助手用户Controller
 * 
 * @author zqc
 * @date 2023-11-21
 */
@RestController
@RequestMapping("/core/assistantUser")
public class CmsAssistantUserController extends BaseController
{
    @Autowired
    private ICmsAssistantUserService cmsAssistantUserService;

    /**
     * 查询手机助手用户列表
     */
    @PreAuthorize("@ss.hasPermi('core:assistantUser:list')")
    @GetMapping("/list")
    public TableDataInfo list(CmsAssistantUser cmsAssistantUser)
    {
        startPage();
        List<CmsAssistantUser> list = cmsAssistantUserService.selectCmsAssistantUserList(cmsAssistantUser);
        return getDataTable(list);
    }

    /**
     * 导出手机助手用户列表
     */
    @PreAuthorize("@ss.hasPermi('core:assistantUser:export')")
    @Log(title = "手机助手用户", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsAssistantUser cmsAssistantUser)
    {
        List<CmsAssistantUser> list = cmsAssistantUserService.selectCmsAssistantUserList(cmsAssistantUser);
        ExcelUtil<CmsAssistantUser> util = new ExcelUtil<CmsAssistantUser>(CmsAssistantUser.class);
        util.exportExcel(response, list, "手机助手用户数据");
    }

    /**
     * 获取手机助手用户详细信息
     */
    @PreAuthorize("@ss.hasPermi('core:assistantUser:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cmsAssistantUserService.selectCmsAssistantUserById(id));
    }

    /**
     * 新增手机助手用户
     */
    @PreAuthorize("@ss.hasPermi('core:assistantUser:add')")
    @Log(title = "手机助手用户", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsAssistantUser cmsAssistantUser)
    {
        return toAjax(cmsAssistantUserService.insertCmsAssistantUser(cmsAssistantUser));
    }

    /**
     * 修改手机助手用户
     */
    @PreAuthorize("@ss.hasPermi('core:assistantUser:edit')")
    @Log(title = "手机助手用户", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsAssistantUser cmsAssistantUser)
    {
        return toAjax(cmsAssistantUserService.updateCmsAssistantUser(cmsAssistantUser));
    }

    /**
     * 删除手机助手用户
     */
    @PreAuthorize("@ss.hasPermi('core:assistantUser:remove')")
    @Log(title = "手机助手用户", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cmsAssistantUserService.deleteCmsAssistantUserByIds(ids));
    }
}
