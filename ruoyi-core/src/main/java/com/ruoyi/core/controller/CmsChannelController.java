package com.ruoyi.core.controller;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.core.domain.CmsChannel;
import com.ruoyi.core.service.ICmsChannelService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * @description: 渠道商管理模块
 * @author java_liuzhibo
 * @date 2023年11月8日17:19:20
 * @version 1.0
 */
@RestController
@RequestMapping("/core/channel")
public class CmsChannelController extends BaseController
{
    @Autowired
    private ICmsChannelService cmsChannelService;

    @Autowired
    private RedisCache redisCache;
    @Value("${partner-sn-key}")
    private String partnerSnKey;
    @Value("${channel-sn-key}")
    private String channelSnKey;

    private final static String regex = "\\d+";

    /**
     * 查询渠道商管理列表
     */
    @PreAuthorize("@ss.hasPermi('core:channel:list')")
    @GetMapping("/list")
    public TableDataInfo list(CmsChannel cmsChannel)
    {
        startPage();
        List<CmsChannel> list = cmsChannelService.selectCmsChannelList(cmsChannel);
        return getDataTable(list);
    }

    /**
     * 导出渠道商管理列表
     */
    @PreAuthorize("@ss.hasPermi('core:channel:export')")
    @Log(title = "渠道商管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsChannel cmsChannel)
    {
        List<CmsChannel> list = cmsChannelService.selectCmsChannelList(cmsChannel);
        ExcelUtil<CmsChannel> util = new ExcelUtil<CmsChannel>(CmsChannel.class);
        util.exportExcel(response, list, "渠道商管理数据");
    }

    /**
     * 获取渠道商管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('core:channel:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cmsChannelService.selectCmsChannelById(id));
    }

    /**
     * 新增渠道商管理
     */
    @PreAuthorize("@ss.hasPermi('core:channel:add')")
    @Log(title = "渠道商管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsChannel cmsChannel)
    {
        Boolean isExistsChannelSnKey = redisCache.hasKey(channelSnKey);
        if (!isExistsChannelSnKey) {
            CmsChannel queryChannel = new CmsChannel();
            queryChannel.setPartnerId(cmsChannel.getPartnerId());
            CmsChannel lastChannel = cmsChannelService.selectLastChannelSn(queryChannel);
            if (ObjectUtil.isNotNull(lastChannel) && StringUtils.isNotBlank(lastChannel.getChannelSn())) {
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(lastChannel.getChannelSn());
                if (matcher.find()) {
                    int querySnNumber = Integer.parseInt(matcher.group(0));
                    redisCache.setCacheObject(channelSnKey, querySnNumber);
                }
            }
        }
        String inviteCodeForChannel = redisCache.generateInviteCode(channelSnKey);
        String snNumberForChannel = org.apache.commons.lang3.StringUtils.leftPad(inviteCodeForChannel, 4, "0");
        cmsChannel.setChannelSn(StringUtils.join(cmsChannel.getPartnerSn(), snNumberForChannel));
        return toAjax(cmsChannelService.insertCmsChannel(cmsChannel));
    }

    /**
     * 修改渠道商管理
     */
    @PreAuthorize("@ss.hasPermi('core:channel:edit')")
    @Log(title = "渠道商管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsChannel cmsChannel)
    {
        return toAjax(cmsChannelService.updateCmsChannel(cmsChannel));
    }

    /**
     * 删除渠道商管理
     */
    @PreAuthorize("@ss.hasPermi('core:channel:remove')")
    @Log(title = "渠道商管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cmsChannelService.deleteCmsChannelByIds(ids));
    }
}
