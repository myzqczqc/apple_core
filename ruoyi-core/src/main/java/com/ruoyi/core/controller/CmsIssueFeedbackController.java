package com.ruoyi.core.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.core.domain.CmsIssueFeedback;
import com.ruoyi.core.service.ICmsIssueFeedbackService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 手机助手问题反馈Controller
 * 
 * @author zqc
 * @date 2023-11-27
 */
@RestController
@RequestMapping("/core/issueFeedback")
public class CmsIssueFeedbackController extends BaseController
{
    @Autowired
    private ICmsIssueFeedbackService cmsIssueFeedbackService;

    /**
     * 查询手机助手问题反馈列表
     */
    @PreAuthorize("@ss.hasPermi('core:issueFeedback:list')")
    @GetMapping("/list")
    public TableDataInfo list(CmsIssueFeedback cmsIssueFeedback)
    {
        startPage();
        List<CmsIssueFeedback> list = cmsIssueFeedbackService.selectCmsIssueFeedbackList(cmsIssueFeedback);
        return getDataTable(list);
    }

    /**
     * 导出手机助手问题反馈列表
     */
    @PreAuthorize("@ss.hasPermi('core:issueFeedback:export')")
    @Log(title = "手机助手问题反馈", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsIssueFeedback cmsIssueFeedback)
    {
        List<CmsIssueFeedback> list = cmsIssueFeedbackService.selectCmsIssueFeedbackList(cmsIssueFeedback);
        ExcelUtil<CmsIssueFeedback> util = new ExcelUtil<CmsIssueFeedback>(CmsIssueFeedback.class);
        util.exportExcel(response, list, "手机助手问题反馈数据");
    }

    /**
     * 获取手机助手问题反馈详细信息
     */
    @PreAuthorize("@ss.hasPermi('core:issueFeedback:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cmsIssueFeedbackService.selectCmsIssueFeedbackById(id));
    }

    /**
     * 新增手机助手问题反馈
     */
    @PreAuthorize("@ss.hasPermi('core:issueFeedback:add')")
    @Log(title = "手机助手问题反馈", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsIssueFeedback cmsIssueFeedback)
    {
        return toAjax(cmsIssueFeedbackService.insertCmsIssueFeedback(cmsIssueFeedback));
    }

    /**
     * 修改手机助手问题反馈
     */
    @PreAuthorize("@ss.hasPermi('core:issueFeedback:edit')")
    @Log(title = "手机助手问题反馈", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsIssueFeedback cmsIssueFeedback)
    {
        return toAjax(cmsIssueFeedbackService.updateCmsIssueFeedback(cmsIssueFeedback));
    }

    /**
     * 删除手机助手问题反馈
     */
    @PreAuthorize("@ss.hasPermi('core:issueFeedback:remove')")
    @Log(title = "手机助手问题反馈", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cmsIssueFeedbackService.deleteCmsIssueFeedbackByIds(ids));
    }
}
