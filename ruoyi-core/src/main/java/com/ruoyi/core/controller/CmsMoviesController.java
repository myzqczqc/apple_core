package com.ruoyi.core.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.core.domain.vo.CmsMoviesVo;
import com.ruoyi.core.enums.ContentTypeEnums;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.core.domain.CmsMovies;
import com.ruoyi.core.service.ICmsMoviesService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 影视数据信息Controller
 * 
 * @author zqc
 * @date 2023-11-06
 */
@RestController
@RequestMapping("/core/movies")
public class CmsMoviesController extends BaseController
{
    @Autowired
    private ICmsMoviesService cmsMoviesService;

    /**
     * 查询影视数据信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(CmsMovies cmsMovies)
    {
        if(ContentTypeEnums.SHARE.getType()==cmsMovies.getSource()){
            //公共资源
            cmsMovies.setTypeId(ContentTypeEnums.SHARE.getType());
        }else if(ContentTypeEnums.PRIVATE.getType()==cmsMovies.getSource()){
            //我的资源
            cmsMovies.setCreateBy(getUsername());
        }else if(ContentTypeEnums.SEARCH.getType()==cmsMovies.getSource()){
            //搜索
            cmsMovies.setCreateBy(getUsername());
        }
        startPage();
        List<CmsMovies> list = cmsMoviesService.selectCmsMoviesList(cmsMovies);
        return getDataTable(list);
    }

    /**
     * 导出影视数据信息列表
     */
    @Log(title = "影视数据信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CmsMovies cmsMovies)
    {
        List<CmsMovies> list = cmsMoviesService.selectCmsMoviesList(cmsMovies);
        ExcelUtil<CmsMovies> util = new ExcelUtil<CmsMovies>(CmsMovies.class);
        util.exportExcel(response, list, "影视数据信息数据");
    }

    /**
     * 获取影视数据信息详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cmsMoviesService.selectCmsMoviesById(id));
    }

    /**
     * 新增影视数据信息
     */
    @Log(title = "影视数据信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CmsMoviesVo cmsMoviesVo ) throws IOException {
        cmsMoviesVo.setCreateTime(DateUtils.getNowDate());
        cmsMoviesVo.setCreateBy(getUsername());
        // 如果是emdor用户的数据，默认是共享的
        if (UserConstants.SHARE_USER.equals(getUsername())) {
            cmsMoviesVo.setTypeId(1);
        }
        return toAjax(cmsMoviesService.insertCmsMovies(cmsMoviesVo));
    }

    /**
     * 修改影视数据信息
     */
    @Log(title = "影视数据信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CmsMoviesVo cmsMoviesVo) throws IOException {
        cmsMoviesVo.setUpdateTime(DateUtils.getNowDate());
        cmsMoviesVo.setUpdateBy(getUsername());
        // 如果是emdor用户的数据，默认是共享的
        if (UserConstants.SHARE_USER.equals(getUsername())) {
            cmsMoviesVo.setTypeId(1);
        }
        int i = cmsMoviesService.updateCmsMovies(cmsMoviesVo);
        if(i==-1){
            return AjaxResult.error("不能修改");
        }
        return toAjax(i);
    }

    /**
     * 删除影视数据信息
     */
    @Log(title = "影视数据信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        int i = cmsMoviesService.deleteCmsMoviesById(id, getUsername());
        if(i==-1){
            return AjaxResult.error("不能删除");
        }
        return toAjax(i);
    }
}
