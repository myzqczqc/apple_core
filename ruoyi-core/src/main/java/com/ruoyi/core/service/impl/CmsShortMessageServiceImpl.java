package com.ruoyi.core.service.impl;

import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.teaopenapi.models.Config;
import com.ruoyi.core.domain.CmsShortMessage;
import com.ruoyi.core.mapper.CmsShortMessageMapper;
import com.ruoyi.core.service.ICmsShortMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class CmsShortMessageServiceImpl implements ICmsShortMessageService {

    private static final Logger logger = LoggerFactory.getLogger(CmsShortMessageServiceImpl.class);

    @Autowired
    private CmsShortMessageMapper cmsShortMessageMapper;

    private Client client;

    /**
     * 发送短信验证码
     *
     * @param phone
     * @return
     */
    @Override
    public boolean sendShortMessage(String phone, String code) {
        CmsShortMessage cmsShortMessage = cmsShortMessageMapper.selectCmsShortMessageById(1);
        try {
            SendSmsRequest sendSmsRequest = new SendSmsRequest()
                    .setPhoneNumbers(phone)
                    .setTemplateCode(cmsShortMessage.getTemplateCode())
                    .setSignName(cmsShortMessage.getSignName())
                    .setTemplateParam("{'code':'" + code + "'}");
            SendSmsResponse sendSmsResponse = client.sendSms(sendSmsRequest);
            return sendSmsResponse.getBody().getCode().equals("OK");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @PostConstruct
    public void createClient() throws Exception {
        logger.info("短信验证码信息配置开始");
        CmsShortMessage cmsShortMessage = cmsShortMessageMapper.selectCmsShortMessageById(1);
        Config config = new Config()
                .setAccessKeyId(cmsShortMessage.getAccessKeyId())
                .setAccessKeySecret(cmsShortMessage.getAccessKeySecret());
        config.endpoint = "dysmsapi.aliyuncs.com";
        client = new com.aliyun.dysmsapi20170525.Client(config);
        logger.info("短信验证码信息配置结束");
    }
}
