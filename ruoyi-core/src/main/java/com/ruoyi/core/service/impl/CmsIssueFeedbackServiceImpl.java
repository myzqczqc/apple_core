package com.ruoyi.core.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.core.mapper.CmsIssueFeedbackMapper;
import com.ruoyi.core.domain.CmsIssueFeedback;
import com.ruoyi.core.service.ICmsIssueFeedbackService;

/**
 * 手机助手问题反馈Service业务层处理
 * 
 * @author zqc
 * @date 2023-11-27
 */
@Service
public class CmsIssueFeedbackServiceImpl implements ICmsIssueFeedbackService 
{
    @Autowired
    private CmsIssueFeedbackMapper cmsIssueFeedbackMapper;

    /**
     * 查询手机助手问题反馈
     * 
     * @param id 手机助手问题反馈主键
     * @return 手机助手问题反馈
     */
    @Override
    public CmsIssueFeedback selectCmsIssueFeedbackById(Long id)
    {
        return cmsIssueFeedbackMapper.selectCmsIssueFeedbackById(id);
    }

    /**
     * 查询手机助手问题反馈列表
     * 
     * @param cmsIssueFeedback 手机助手问题反馈
     * @return 手机助手问题反馈
     */
    @Override
    public List<CmsIssueFeedback> selectCmsIssueFeedbackList(CmsIssueFeedback cmsIssueFeedback)
    {
        return cmsIssueFeedbackMapper.selectCmsIssueFeedbackList(cmsIssueFeedback);
    }

    /**
     * 新增手机助手问题反馈
     * 
     * @param cmsIssueFeedback 手机助手问题反馈
     * @return 结果
     */
    @Override
    public int insertCmsIssueFeedback(CmsIssueFeedback cmsIssueFeedback)
    {
        cmsIssueFeedback.setCreateTime(DateUtils.getNowDate());
        return cmsIssueFeedbackMapper.insertCmsIssueFeedback(cmsIssueFeedback);
    }

    /**
     * 修改手机助手问题反馈
     * 
     * @param cmsIssueFeedback 手机助手问题反馈
     * @return 结果
     */
    @Override
    public int updateCmsIssueFeedback(CmsIssueFeedback cmsIssueFeedback)
    {
        cmsIssueFeedback.setUpdateTime(DateUtils.getNowDate());
        return cmsIssueFeedbackMapper.updateCmsIssueFeedback(cmsIssueFeedback);
    }

    /**
     * 批量删除手机助手问题反馈
     * 
     * @param ids 需要删除的手机助手问题反馈主键
     * @return 结果
     */
    @Override
    public int deleteCmsIssueFeedbackByIds(Long[] ids)
    {
        return cmsIssueFeedbackMapper.deleteCmsIssueFeedbackByIds(ids);
    }

    /**
     * 删除手机助手问题反馈信息
     * 
     * @param id 手机助手问题反馈主键
     * @return 结果
     */
    @Override
    public int deleteCmsIssueFeedbackById(Long id)
    {
        return cmsIssueFeedbackMapper.deleteCmsIssueFeedbackById(id);
    }
}
