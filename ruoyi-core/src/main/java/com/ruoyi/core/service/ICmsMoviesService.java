package com.ruoyi.core.service;

import java.io.IOException;
import java.util.List;
import com.ruoyi.core.domain.CmsMovies;
import com.ruoyi.core.domain.vo.CmsMoviesVo;

/**
 * 影视数据信息Service接口
 * 
 * @author zqc
 * @date 2023-11-06
 */
public interface ICmsMoviesService 
{
    /**
     * 查询影视数据信息
     * 
     * @param id 影视数据信息主键
     * @return 影视数据信息
     */
    public CmsMoviesVo selectCmsMoviesById(Long id);

    /**
     * 查询影视数据信息列表
     * 
     * @param cmsMovies 影视数据信息
     * @return 影视数据信息集合
     */
    public List<CmsMovies> selectCmsMoviesList(CmsMovies cmsMovies);

    /**
     * 新增影视数据信息
     * 
     * @param cmsMoviesVo 影视数据信息
     * @return 结果
     */
    public int insertCmsMovies(CmsMoviesVo cmsMoviesVo) throws IOException;

    /**
     * 修改影视数据信息
     * 
     * @param cmsMoviesVo 影视数据信息
     * @return 结果
     */
    public int updateCmsMovies(CmsMoviesVo cmsMoviesVo) throws IOException;

    /**
     * 批量删除影视数据信息
     * 
     * @param ids 需要删除的影视数据信息主键集合
     * @return 结果
     */
    public int deleteCmsMoviesByIds(Long[] ids);

    /**
     * 删除影视数据信息信息
     * 
     * @param id 影视数据信息主键
     * @return 结果
     */
    public int deleteCmsMoviesById(Long id,String userName);
}
