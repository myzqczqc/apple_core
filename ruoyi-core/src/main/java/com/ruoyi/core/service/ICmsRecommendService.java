package com.ruoyi.core.service;

import java.io.IOException;
import java.util.List;
import com.ruoyi.core.domain.CmsRecommend;

/**
 * tab的recommend信息Service接口
 * 
 * @author zqc
 * @date 2023-11-08
 */
public interface ICmsRecommendService 
{
    /**
     * 查询tab的recommend信息
     * 
     * @param id tab的recommend信息主键
     * @return tab的recommend信息
     */
    public CmsRecommend selectCmsRecommendById(Long id);

    /**
     * 查询tab的recommend信息列表
     * 
     * @param cmsRecommend tab的recommend信息
     * @return tab的recommend信息集合
     */
    public List<CmsRecommend> selectCmsRecommendList(CmsRecommend cmsRecommend);

    /**
     * 新增tab的recommend信息
     * 
     * @param cmsRecommend tab的recommend信息
     * @return 结果
     */
    public int insertCmsRecommend(CmsRecommend cmsRecommend) throws IOException;

    /**
     * 修改tab的recommend信息
     * 
     * @param cmsRecommend tab的recommend信息
     * @return 结果
     */
    public int updateCmsRecommend(CmsRecommend cmsRecommend) throws IOException;

    /**
     * 批量删除tab的recommend信息
     * 
     * @param ids 需要删除的tab的recommend信息主键集合
     * @return 结果
     */
    public int deleteCmsRecommendByIds(Long[] ids);

    /**
     * 删除tab的recommend信息信息
     * 
     * @param id tab的recommend信息主键
     * @return 结果
     */
    public int deleteCmsRecommendById(Long id ,String userName);

    /**
     * 修改指定的数据到 顺序号seq上
     * @param id
     * @param seq
     * @return
     */
    public int updateSeq(Long id,Integer seq,String userName);

    /**
     * 根据apkId删除推荐配置
     * @param apkId
     */
    int deleteCmsRecommendByApkId(Long apkId);
}
