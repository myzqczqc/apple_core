package com.ruoyi.core.service.impl;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.core.service.IUploadFile2YunService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.core.mapper.CmsCoreVersionMapper;
import com.ruoyi.core.domain.CmsCoreVersion;
import com.ruoyi.core.service.ICmsCoreVersionService;
import org.springframework.transaction.annotation.Transactional;

/**
 * core应用历史版本信息Service业务层处理
 * 
 * @author zqc
 * @date 2023-11-23
 */
@Service
@Transactional
public class CmsCoreVersionServiceImpl implements ICmsCoreVersionService 
{
    @Autowired
    private CmsCoreVersionMapper cmsCoreVersionMapper;
    @Autowired
    private IUploadFile2YunService uploadFile2YunService;
    @Autowired
    private RedisCache redisCache;

    private static final String PATH = "core";
    /**
     * 查询core应用历史版本信息
     * 
     * @param id core应用历史版本信息主键
     * @return core应用历史版本信息
     */
    @Override
    public CmsCoreVersion selectCmsCoreVersionById(Long id)
    {
        return cmsCoreVersionMapper.selectCmsCoreVersionById(id);
    }

    /**
     * 查询core应用历史版本信息列表
     * 
     * @param cmsCoreVersion core应用历史版本信息
     * @return core应用历史版本信息
     */
    @Override
    public List<CmsCoreVersion> selectCmsCoreVersionList(CmsCoreVersion cmsCoreVersion)
    {
        return cmsCoreVersionMapper.selectCmsCoreVersionList(cmsCoreVersion);
    }

    /**
     * 新增core应用历史版本信息
     * 
     * @param cmsCoreVersion core应用历史版本信息
     * @return 结果
     */
    @Override
    public int insertCmsCoreVersion(CmsCoreVersion cmsCoreVersion) throws IOException {
        //查看是否有和 versionCode 重复的数据，如果有，直接删除
        CmsCoreVersion temp = new CmsCoreVersion();
        temp.setTypeId(cmsCoreVersion.getTypeId());
        temp.setVersionCode(cmsCoreVersion.getVersionCode());
        List<CmsCoreVersion> cmsGlassesCoreVersions = cmsCoreVersionMapper.selectCmsCoreVersionList(temp);
        if( ! cmsGlassesCoreVersions.isEmpty()){
            Long[] ids = cmsGlassesCoreVersions.stream().map(a -> a.getId()).collect(Collectors.toList()).toArray(new Long[cmsGlassesCoreVersions.size()]);
            cmsCoreVersionMapper.deleteCmsCoreVersionByIds(ids);
        }
        //添加数，生成主键id
        cmsCoreVersionMapper.insertCmsCoreVersion(cmsCoreVersion);
        //上传文件到oss
        upload2Oss(cmsCoreVersion);
        //更新文件地址
        cmsCoreVersionMapper.updateCmsCoreVersion(cmsCoreVersion);
        //删除缓存
        removeRedies(cmsCoreVersion.getTypeId().intValue());
        return 1 ;
    }

    /**
     * apk的包和icon，都要上传oss
     * @param cmsCoreVersion
     */
    private void upload2Oss(CmsCoreVersion cmsCoreVersion) throws IOException {
        String url = cmsCoreVersion.getUrl();
        String apkUrl= uploadFile2YunService.uploadFile2Oss(String.valueOf(cmsCoreVersion.getId()), url, PATH);

        if(StringUtils.isNotEmpty(apkUrl)){
            cmsCoreVersion.setUrl(apkUrl);
        }
        //icon上传到oss
        String icon = cmsCoreVersion.getIcon();

        String iconUrl = uploadFile2YunService.uploadFile2Oss(String.valueOf(cmsCoreVersion.getId()), icon, PATH);
        if(StringUtils.isNotEmpty(iconUrl)){
            cmsCoreVersion.setIcon(iconUrl);
        }
    }
    /**
     * 修改core应用历史版本信息
     * 
     * @param cmsCoreVersion core应用历史版本信息
     * @return 结果
     */
    @Override
    public int updateCmsCoreVersion(CmsCoreVersion cmsCoreVersion) throws IOException {
        //上传文件到oss
        upload2Oss(cmsCoreVersion);
        return cmsCoreVersionMapper.updateCmsCoreVersion(cmsCoreVersion);
    }

    /**
     * 批量删除core应用历史版本信息
     * 
     * @param ids 需要删除的core应用历史版本信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsCoreVersionByIds(Long[] ids)
    {
        return cmsCoreVersionMapper.deleteCmsCoreVersionByIds(ids);
    }

    /**
     * 删除core应用历史版本信息信息
     * 
     * @param id core应用历史版本信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsCoreVersionById(Long id)
    {
        return cmsCoreVersionMapper.deleteCmsCoreVersionById(id);
    }


    private void removeRedies(Integer typeId){
        String coreVersionKey = RedisKey.getCoreVersionKey(typeId);
        redisCache.deleteObject(coreVersionKey);
    }
}
