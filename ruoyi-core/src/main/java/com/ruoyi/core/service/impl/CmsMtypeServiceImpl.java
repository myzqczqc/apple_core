package com.ruoyi.core.service.impl;

import java.util.List;

import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.common.core.redis.RedisCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.core.mapper.CmsMtypeMapper;
import com.ruoyi.core.domain.CmsMtype;
import com.ruoyi.core.service.ICmsMtypeService;

/**
 * 影视类型Service业务层处理
 * 
 * @author zqc
 * @date 2023-11-06
 */
@Service
public class CmsMtypeServiceImpl implements ICmsMtypeService 
{
    @Autowired
    private CmsMtypeMapper cmsMtypeMapper;
    @Autowired
    private RedisCache redisCache;
    /**
     * 查询影视类型
     * 
     * @param id 影视类型主键
     * @return 影视类型
     */
    @Override
    public CmsMtype selectCmsMtypeById(Long id)
    {
        return cmsMtypeMapper.selectCmsMtypeById(id);
    }

    /**
     * 查询影视类型列表
     * 
     * @param cmsMtype 影视类型
     * @return 影视类型
     */
    @Override
    public List<CmsMtype> selectCmsMtypeList(CmsMtype cmsMtype)
    {
        return cmsMtypeMapper.selectCmsMtypeList(cmsMtype);
    }

    /**
     * 新增影视类型
     * 
     * @param cmsMtype 影视类型
     * @return 结果
     */
    @Override
    public int insertCmsMtype(CmsMtype cmsMtype)
    {
        //删除缓存
        removeRedis();
        return cmsMtypeMapper.insertCmsMtype(cmsMtype);
    }

    /**
     * 修改影视类型
     * 
     * @param cmsMtype 影视类型
     * @return 结果
     */
    @Override
    public int updateCmsMtype(CmsMtype cmsMtype)
    {
        //删除缓存
        removeRedis();
        return cmsMtypeMapper.updateCmsMtype(cmsMtype);
    }

    /**
     * 批量删除影视类型
     * 
     * @param ids 需要删除的影视类型主键
     * @return 结果
     */
    @Override
    public int deleteCmsMtypeByIds(Long[] ids)
    {
        return cmsMtypeMapper.deleteCmsMtypeByIds(ids);
    }

    /**
     * 删除影视类型信息
     * 
     * @param id 影视类型主键
     * @return 结果
     */
    @Override
    public int deleteCmsMtypeById(Long id)
    {
        //删除缓存
        removeRedis();
        return cmsMtypeMapper.deleteCmsMtypeById(id);
    }

    /**
     * 删除缓存
     */
    private void removeRedis(){
        String moviesTypeKey = RedisKey.getMTypeKey();
        redisCache.deleteObject(moviesTypeKey);
    }
}
