package com.ruoyi.core.service.impl;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.core.domain.CmsApkInfo;
import com.ruoyi.core.mapper.CmsApkInfoMapper;
import com.ruoyi.core.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * apk信息Service业务层处理
 *
 * @author zqc
 * @date 2023-11-06
 */
@Service
@Transactional
public class CmsApkInfoServiceImpl implements ICmsApkInfoService {
    @Autowired
    private CmsApkInfoMapper cmsApkInfoMapper;
    @Autowired
    private IUploadFile2YunService uploadFile2YunService;
    @Autowired
    private RedisCache redisCache;
    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;
    @Autowired
    private ICmsBannerService cmsBannerService;
    @Autowired
    private ICmsRecommendService cmsRecommendService;
    @Autowired
    private ICmsAppService cmsAppService;

    private static final String PATH = "apk";

    /**
     * 查询apk信息
     *
     * @param id apk信息主键
     * @return apk信息
     */
    @Override
    public CmsApkInfo selectCmsApkInfoById(Long id) {
        return cmsApkInfoMapper.selectCmsApkInfoById(id);
    }

    /**
     * 查询apk信息列表
     *
     * @param cmsApkInfo apk信息
     * @return apk信息
     */
    @Override
    public List<CmsApkInfo> selectCmsApkInfoList(CmsApkInfo cmsApkInfo) {
        return cmsApkInfoMapper.selectCmsApkInfoList(cmsApkInfo);
    }

    /**
     * 新增apk信息
     *
     * @param cmsApkInfo apk信息
     * @return 结果
     */
    @Override
    public int insertCmsApkInfo(CmsApkInfo cmsApkInfo) throws IOException {
        cmsApkInfo.setCreateTime(DateUtils.getNowDate());

//        CmsApkInfo temp = cmsApkInfoMapper.selectCmsApkInfoByPackageName(cmsApkInfo.getPackageName());
//        if( StringUtils.isNotNull(temp)){
//            //apk包存在，则不再添加
//            if( ! temp.getCreateBy().equals(cmsApkInfo)){
//                //不是自己的包，不可以上传
//                return -1;
//            }
//        }else{
        //添加
        cmsApkInfoMapper.insertCmsApkInfo(cmsApkInfo);
//        }

        //上传文件到oss
        upload2Oss(cmsApkInfo);
        //更新文件地址
        cmsApkInfoMapper.updateCmsApkInfo(cmsApkInfo);

        //删除缓存
        removeRedis(cmsApkInfo.getId(), cmsApkInfo.getApType().longValue());
        return 1;
    }

    /**
     * apk的包和icon，都要上传oss
     *
     * @param cmsApkInfo
     */
    private void upload2Oss(CmsApkInfo cmsApkInfo) throws IOException {
        String image = cmsApkInfo.getImage();
        if (StringUtils.isNotEmpty(image)) {
            List<String> res = new ArrayList<>();
            List<String> ims = JSON.parseArray(image, String.class);
            for (int i = 0; i < ims.size(); i++) {
                String imsUrl = uploadFile2YunService.uploadFile2Oss("image_" + i + "_" + cmsApkInfo.getId(), ims.get(i), PATH);
                if (StringUtils.isNotEmpty(imsUrl)) {
                    res.add(imsUrl);
                } else {
                    res.add(ims.get(i));
                }
            }
            if (!res.isEmpty()) {
                cmsApkInfo.setImage(JSON.toJSONString(res));
            }
        }
        // 上传apk包和icon图标
        taskExecutor.execute(() -> {
            String icon = cmsApkInfo.getIcon();
            String url = cmsApkInfo.getUrl();
            String apkUrl, iconUrl = null;
            try {
                iconUrl = uploadFile2YunService.uploadFile2Oss(String.valueOf(cmsApkInfo.getId()), icon, PATH);
                apkUrl = uploadFile2YunService.uploadFile2Oss(String.valueOf(cmsApkInfo.getId()), url, PATH);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            if (StringUtils.isNotEmpty(apkUrl)) {
                cmsApkInfo.setUrl(apkUrl);
            }
            if (StringUtils.isNotEmpty(iconUrl)) {
                cmsApkInfo.setIcon(iconUrl);
            }
            cmsApkInfoMapper.updateCmsApkInfo(cmsApkInfo);
        });
    }

    /**
     * 修改apk信息
     *
     * @param cmsApkInfo apk信息
     * @return 结果
     */
    @Override
    public int updateCmsApkInfo(CmsApkInfo cmsApkInfo) throws IOException {
        CmsApkInfo temp = cmsApkInfoMapper.selectCmsApkInfoById(cmsApkInfo.getId());
        if (!temp.getCreateBy().equals(cmsApkInfo.getCreateBy())) {
            return -1;
        }
        if (!cmsApkInfo.getPackageName().equals(temp.getPackageName())) {
            return -1;
        }
        //上传文件到oss，
        upload2Oss(cmsApkInfo);
        cmsApkInfoMapper.updateCmsApkInfo(cmsApkInfo);

        //删除缓存
        removeRedis(cmsApkInfo.getId(), cmsApkInfo.getApType().longValue());
        return 1;
    }

    /**
     * 批量删除apk信息
     *
     * @param ids 需要删除的apk信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsApkInfoByIds(Long[] ids) {
        return cmsApkInfoMapper.deleteCmsApkInfoByIds(ids);
    }

    /**
     * 删除apk信息信息
     *
     * @param id apk信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsApkInfoById(Long id, String userName) {
        CmsApkInfo apkInfo = cmsApkInfoMapper.selectCmsApkInfoById(id);
        if (!apkInfo.getCreateBy().equals(userName)) {
            return -1;
        }
        // 删除banner配置
        cmsBannerService.deleteCmsBannerByApkId(id);
        // 删除recommend推荐配置
        cmsRecommendService.deleteCmsRecommendByApkId(id);
        // 删除默认配置中的 app配置表
        cmsAppService.deleteCmsAppByApkId(id);
        // 删除apk信息
        cmsApkInfoMapper.deleteCmsApkInfoById(id);
        // 删除缓存
        removeRedis(id, apkInfo.getApType().longValue());
        return 1;
    }


    /**
     * 删除缓存
     *
     * @param id
     */
    private void removeRedis(Long id, Long apType) {
        //某个apk的详细信息
        String apkInfoKey = RedisKey.getApkKey(id);
        redisCache.deleteObject(apkInfoKey);
        //子分类列表的信息
        String appListKey = RedisKey.getAppListKey(apType);
        Collection<String> keys = redisCache.keys(appListKey + "*");
        redisCache.deleteObject(keys);
    }
}
