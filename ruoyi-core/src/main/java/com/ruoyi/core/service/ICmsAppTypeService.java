package com.ruoyi.core.service;

import com.ruoyi.core.domain.CmsAppType;

import java.util.List;

/**
 * app子分类Service接口
 *
 * @author zqc
 * @date 2023-11-25
 */
public interface ICmsAppTypeService {
    /**
     * 查询app子分类
     *
     * @param id app子分类主键
     * @return app子分类
     */
    public CmsAppType selectCmsAppTypeById(Long id);

    /**
     * 查询app子分类列表
     *
     * @param cmsAppType app子分类
     * @return app子分类集合
     */
    public List<CmsAppType> selectCmsAppTypeList(CmsAppType cmsAppType);

    /**
     * 新增app子分类
     *
     * @param cmsAppType app子分类
     * @return 结果
     */
    public int insertCmsAppType(CmsAppType cmsAppType);

    /**
     * 修改app子分类
     *
     * @param cmsAppType app子分类
     * @return 结果
     */
    public int updateCmsAppType(CmsAppType cmsAppType);

    /**
     * 批量删除app子分类
     *
     * @param ids 需要删除的app子分类主键集合
     * @return 结果
     */
    public int deleteCmsAppTypeByIds(Long[] ids);

    /**
     * 删除app子分类信息
     *
     * @param id app子分类主键
     * @return 结果
     */
    public int deleteCmsAppTypeById(Long id);

    /**
     * 修改指定的数据到 顺序号seq上
     *
     * @param id
     * @param seq
     * @return
     */
    int updateSeq(Long id, Integer seq);

}
