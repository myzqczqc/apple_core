package com.ruoyi.core.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.api.model.AssistantUser;
import com.ruoyi.common.constant.CacheConstants;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.enums.UserStatus;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.exception.user.AssistantUserPasswordNotExistsException;
import com.ruoyi.common.exception.user.AssistantUserPasswordNotMatchException;
import com.ruoyi.common.exception.user.UserNotExistsException;
import com.ruoyi.common.exception.user.UserPasswordNotMatchException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.MessageUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.ruoyi.core.mapper.CmsAssistantUserMapper;
import com.ruoyi.core.domain.CmsAssistantUser;
import com.ruoyi.core.service.ICmsAssistantUserService;

/**
 * 手机助手用户Service业务层处理
 *
 * @author zqc
 * @date 2023-11-21
 */
@Service
public class CmsAssistantUserServiceImpl implements ICmsAssistantUserService
{
    @Autowired
    private CmsAssistantUserMapper cmsAssistantUserMapper;

    // 令牌自定义标识
    @Value("${token.header}")
    private String header;

    // 令牌秘钥
    @Value("${token.secret}")
    private String secret;

    // 令牌有效期（默认30分钟）
    @Value("${token.expireTime}")
    private int expireTime;

    protected static final long MILLIS_SECOND = 1000;

    protected static final long MILLIS_MINUTE = 60 * MILLIS_SECOND;

    private static final Long MILLIS_MINUTE_TEN = 20 * 60 * 1000L;

    @Autowired
    private RedisCache redisCache;

    /**
     * 查询手机助手用户
     *
     * @param id 手机助手用户主键
     * @return 手机助手用户
     */
    @Override
    public CmsAssistantUser selectCmsAssistantUserById(Long id)
    {
        return cmsAssistantUserMapper.selectCmsAssistantUserById(id);
    }

    /**
     * 根据手机号查询用户
     * @param phone 手机号
     * @return 手机助手用户
     */
    @Override
    public CmsAssistantUser selectCmsAssistantUserByPhone(String phone) {
        return cmsAssistantUserMapper.selectCmsAssistantUserByPhone(phone);
    }

    /**
     * 查询手机助手用户列表
     *
     * @param cmsAssistantUser 手机助手用户
     * @return 手机助手用户
     */
    @Override
    public List<CmsAssistantUser> selectCmsAssistantUserList(CmsAssistantUser cmsAssistantUser)
    {
        return cmsAssistantUserMapper.selectCmsAssistantUserList(cmsAssistantUser);
    }
    public boolean checkUserNameUnique(CmsAssistantUser user) {
        Long userId = StringUtils.isNull(user.getId()) ? -1L : user.getId();
        CmsAssistantUser cmsAssistantUser = cmsAssistantUserMapper.checkUserNameUnique(user);
        if (ObjectUtil.isNotNull(cmsAssistantUser) && cmsAssistantUser.getId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }
    /**
     * 新增手机助手用户
     *
     * @param cmsAssistantUser 手机助手用户
     * @return 结果
     */
    @Override
    public int insertCmsAssistantUser(CmsAssistantUser cmsAssistantUser)
    {
        cmsAssistantUser.setCreateTime(DateUtils.getNowDate());
        return cmsAssistantUserMapper.insertCmsAssistantUser(cmsAssistantUser);
    }

    /**
     * 修改手机助手用户
     *
     * @param cmsAssistantUser 手机助手用户
     * @return 结果
     */
    @Override
    public int updateCmsAssistantUser(CmsAssistantUser cmsAssistantUser)
    {
        cmsAssistantUser.setUpdateTime(DateUtils.getNowDate());
        return cmsAssistantUserMapper.updateCmsAssistantUser(cmsAssistantUser);
    }
    /**
     * 修改用户头像
     *
     * @param userName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    @Override
    public boolean updateUserAvatar(String userName, String avatar)
    {
        return cmsAssistantUserMapper.updateUserAvatar(userName, avatar) > 0;
    }
    @Override
    public int unbindCmsAssistantPhone(CmsAssistantUser cmsAssistantUser)
    {
        return cmsAssistantUserMapper.unbindCmsAssistantPhone(cmsAssistantUser);
    }
    /**
     * 批量删除手机助手用户
     *
     * @param ids 需要删除的手机助手用户主键
     * @return 结果
     */
    @Override
    public int deleteCmsAssistantUserByIds(Long[] ids)
    {
        return cmsAssistantUserMapper.deleteCmsAssistantUserByIds(ids);
    }

    /**
     * 删除手机助手用户信息
     *
     * @param id 手机助手用户主键
     * @return 结果
     */
    @Override
    public int deleteCmsAssistantUserById(Long id)
    {
        return cmsAssistantUserMapper.deleteCmsAssistantUserById(id);
    }

    /**
     * 修改用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    @Override
    public int updateUserPwd(String userName, String password)
    {
        return cmsAssistantUserMapper.updateUserPwd(userName, password);
    }

    /**
     * 登录验证
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    public AjaxResult login(String userName, String password,String...isPhoneLogin) {
        String token = IdUtils.fastUUID();
        // TODO: 2023年11月21日 手机助手用户与token关系绑定逻辑处理
        CmsAssistantUser assistantUser = new CmsAssistantUser();
        assistantUser.setUserName(userName);
        assistantUser = cmsAssistantUserMapper.selectUserByUserName(assistantUser.getUserName());
        if (isPhoneLogin.length == 0) {
            loginPreCheck(userName, password);
            loginAfterCheck(userName, password,assistantUser);
        }
        AssistantUser user = new AssistantUser();
        user.setToken(token);
        user.setUser(assistantUser);
        refreshToken(user);
        // 生成token
        Map<String, Object> claims = new HashMap<>();
        claims.put(Constants.ASSISTANT_USER_KEY, token);
        String assistantToken = Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret).compact();
        AjaxResult ajax = AjaxResult.success();
        ajax.put(Constants.TOKEN, assistantToken);
        ajax.put("uid",user.getUser().getId());
        ajax.put("userInfo",user.getUser());
        return ajax;
    }
    /**
     * 登录前置校验
     * @param userName 用户名
     * @param password 用户密码
     */
    public void loginPreCheck(String userName, String password) {
        // 用户名或密码为空 错误
        if (StringUtils.isEmpty(userName) || StringUtils.isEmpty(password))
        {
            throw new UserNotExistsException();
        }
        // 密码如果不在指定范围内 错误
        if (password.length() < UserConstants.PASSWORD_MIN_LENGTH
                || password.length() > UserConstants.PASSWORD_MAX_LENGTH)
        {
            throw new UserPasswordNotMatchException();
        }
        // 用户名不在指定范围内 错误
        if (userName.length() < UserConstants.USERNAME_MIN_LENGTH
                || userName.length() > UserConstants.USERNAME_MAX_LENGTH)
        {
            throw new UserPasswordNotMatchException();
        }
    }
    /**
     * 后置校验
     * @param userName 用户名
     * @param password 用户密码
     */
    public void loginAfterCheck(String userName, String password,CmsAssistantUser user) {
        if (StringUtils.isNull(user))
        {
            throw new ServiceException(MessageUtils.message("assistantuser.not.exists"));
        }
        else if (UserStatus.DELETED.getCode().equals(user.getDelFlag()))
        {
            throw new ServiceException(MessageUtils.message("user.password.delete"));
        }
        else if (StringUtils.isBlank(user.getPassword())) {
            throw new AssistantUserPasswordNotExistsException();
        }

        if (!SecurityUtils.matchesPassword(password, user.getPassword()))
        {
            throw new AssistantUserPasswordNotMatchException();
        }
    }
    public void refreshToken(AssistantUser assistantUser)
    {
        assistantUser.setLoginTime(System.currentTimeMillis());
        assistantUser.setExpireTime(assistantUser.getLoginTime() + expireTime * MILLIS_MINUTE);
        // 根据uuid将loginUser缓存
        String userKey = getTokenKey(assistantUser.getToken());
        redisCache.setCacheObject(userKey, assistantUser, expireTime, TimeUnit.MINUTES);
    }
    private String getTokenKey(String uuid)
    {
        return CacheConstants.LOGIN_TOKEN_KEY + uuid;
    }


}
