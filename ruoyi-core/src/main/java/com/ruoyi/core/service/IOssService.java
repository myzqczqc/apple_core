package com.ruoyi.core.service;

import java.io.File;
import java.io.IOException;

public interface IOssService {

    /**
     * 上传图片
     * @param file
     * @param filePath
     * @return
     * @throws IOException
     */
    String uploadImageToOss(File file, String filePath) throws IOException;



    /**
     * 上传文件到 oss
     * @param file
     * @param filePath
     * @return
     * @throws IOException
     */
    String uploadFileToOss(File file, String filePath) throws IOException;
}