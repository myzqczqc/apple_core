package com.ruoyi.core.service;

import java.io.IOException;
import java.util.List;
import com.ruoyi.core.domain.CmsApkInfo;

/**
 * apk信息Service接口
 * 
 * @author zqc
 * @date 2023-11-06
 */
public interface ICmsApkInfoService 
{
    /**
     * 查询apk信息
     * 
     * @param id apk信息主键
     * @return apk信息
     */
    public CmsApkInfo selectCmsApkInfoById(Long id);

    /**
     * 查询apk信息列表
     * 
     * @param cmsApkInfo apk信息
     * @return apk信息集合
     */
    public List<CmsApkInfo> selectCmsApkInfoList(CmsApkInfo cmsApkInfo);

    /**
     * 新增apk信息
     * 
     * @param cmsApkInfo apk信息
     * @return 结果
     */
    public int insertCmsApkInfo(CmsApkInfo cmsApkInfo) throws IOException;

    /**
     * 修改apk信息
     * 
     * @param cmsApkInfo apk信息
     * @return 结果
     */
    public int updateCmsApkInfo(CmsApkInfo cmsApkInfo) throws IOException;

    /**
     * 批量删除apk信息
     * 
     * @param ids 需要删除的apk信息主键集合
     * @return 结果
     */
    public int deleteCmsApkInfoByIds(Long[] ids);

    /**
     * 删除apk信息信息
     * 
     * @param id apk信息主键
     * @return 结果
     */
    public int deleteCmsApkInfoById(Long id, String userName);
}
