package com.ruoyi.core.service;

import java.io.File;

/**
 * 腾讯云上传文件
 */
public interface ICosService {
    /**
     * 上传文件
     * @param file
     * @param filePath
     * @return
     */
    public String uploadFile(File file, String filePath);


}
