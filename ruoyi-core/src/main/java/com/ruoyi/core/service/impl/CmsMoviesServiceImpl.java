package com.ruoyi.core.service.impl;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.core.domain.CmsMoviesType;
import com.ruoyi.core.domain.CmsMtype;
import com.ruoyi.core.domain.vo.CmsMoviesVo;
import com.ruoyi.core.domain.vo.IntroductionVo;
import com.ruoyi.core.mapper.CmsMoviesTypeMapper;
import com.ruoyi.core.mapper.CmsMtypeMapper;
import com.ruoyi.core.service.ICmsMtypeService;
import com.ruoyi.core.service.IUploadFile2YunService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.core.mapper.CmsMoviesMapper;
import com.ruoyi.core.domain.CmsMovies;
import com.ruoyi.core.service.ICmsMoviesService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 影视数据信息Service业务层处理
 *
 * @author zqc
 * @date 2023-11-06
 */
@Service
@Transactional
public class CmsMoviesServiceImpl implements ICmsMoviesService
{
    @Autowired
    private CmsMoviesMapper cmsMoviesMapper;
    @Autowired
    private CmsMoviesTypeMapper cmsMoviesTypeMapper;
    @Autowired
    private ICmsMtypeService cmsMtypeService;
    @Autowired
    private IUploadFile2YunService uploadFile2YunService;
    //oos的存储目录
    private static final String PATH = "movies";
    @Autowired
    private RedisCache redisCache;


    /**
     * 查询影视数据信息
     *
     * @param id 影视数据信息主键
     * @return 影视数据信息
     */
    @Override
    public CmsMoviesVo selectCmsMoviesById(Long id)
    {
        CmsMovies cmsMovies = cmsMoviesMapper.selectCmsMoviesById(id);

        CmsMoviesVo cmsMoviesVo = new CmsMoviesVo();
        BeanUtils.copyBeanProp(cmsMoviesVo,cmsMovies);
        //简介
        List<IntroductionVo> introductionVos = JSON.parseArray(cmsMovies.getIntroduction(), IntroductionVo.class);
        cmsMoviesVo.setIntroduction(introductionVos);
        //类型
        List<CmsMoviesType> cmsMoviesTypes = cmsMoviesTypeMapper.selectCmsMoviesTypeByMoviesId(id);
        List<Integer> list = cmsMoviesTypes.stream().map(a -> a.getTypeId().intValue()).collect(Collectors.toList());
        cmsMoviesVo.setType(list.toArray(new Integer[list.size()]));

        return cmsMoviesVo;
    }

    /**
     * 查询影视数据信息列表
     *
     * @param cmsMovies 影视数据信息
     * @return 影视数据信息
     */
    @Override
    public List<CmsMovies> selectCmsMoviesList(CmsMovies cmsMovies)
    {
        List<CmsMovies> result = cmsMoviesMapper.selectCmsMoviesList(cmsMovies);
        if(result.isEmpty()){
            return result;
        }
        //字典信息
        List<CmsMtype> cmsMtypes = cmsMtypeService.selectCmsMtypeList(null);

        List<Long> moviesIds = result.stream().map(a -> a.getId()).collect(Collectors.toList());
        //专辑的所有类型信息
        List<CmsMoviesType> moviesTypeList = cmsMoviesTypeMapper.selectCmsMoviesTypeByMoviesIds(moviesIds.toArray(new Long[moviesIds.size()]));

        for (int i = 0; i < result.size(); i++) {
            CmsMovies temp = result.get(i);
            List<CmsMtype> mTypeList = moviesTypeList.stream().filter(a -> a.getMoviesId() == temp.getId().longValue()).map(a -> cmsMtypes.stream()
                    .filter(b -> b.getId() == a.getTypeId()).findFirst().orElse(null)).collect(Collectors.toList());
            temp.setMtypeList(mTypeList);
        }

        return result ;
    }

    /**
     * 新增影视数据信息
     *
     * @param cmsMoviesVo 影视数据信息
     * @return 结果
     */
    @Override
    public int insertCmsMovies(CmsMoviesVo cmsMoviesVo) throws IOException {
        CmsMovies cmsMovies = new CmsMovies();
        BeanUtils.copyBeanProp(cmsMovies,cmsMoviesVo);
        //影视剧描述
        cmsMovies.setIntroduction(JSON.toJSONString(cmsMoviesVo.getIntroduction()));
        //添加
        cmsMoviesMapper.insertCmsMovies(cmsMovies);
        //影视剧id
        Long id = cmsMovies.getId();
        //上传图片信息
        String imageUrl= uploadFile2YunService.uploadFile2Oss(String.valueOf(id), cmsMovies.getImage(), PATH);
        if(StringUtils.isNotEmpty(imageUrl)){
            cmsMovies.setImage(imageUrl);
            cmsMoviesMapper.updateCmsMovies(cmsMovies);
        }
        //影视剧和类型的关系表
        List<CmsMoviesType> list = Arrays.stream(cmsMoviesVo.getType()).map(a -> {
            CmsMoviesType cmsMoviesType = new CmsMoviesType();
            cmsMoviesType.setTypeId(a.longValue());
            cmsMoviesType.setMoviesId(id);
            return cmsMoviesType;
        }).collect(Collectors.toList());
        cmsMoviesTypeMapper.insertCmsMoviesType(list);
        return 1;
    }

    /**
     * 修改影视数据信息
     *
     * @param cmsMoviesVo 影视数据信息
     * @return 结果
     */
    @Override
    public int updateCmsMovies(CmsMoviesVo cmsMoviesVo) throws IOException {
        //查看创建者是不是修改者
        CmsMovies temp = cmsMoviesMapper.selectCmsMoviesById(cmsMoviesVo.getId());
        if( ! temp.getCreateBy().equals(cmsMoviesVo.getUpdateBy())){
            return -1;
        }

        CmsMovies cmsMovies = new CmsMovies();
        BeanUtils.copyBeanProp(cmsMovies,cmsMoviesVo);
        //影视剧描述
        cmsMovies.setIntroduction(JSON.toJSONString(cmsMoviesVo.getIntroduction()));
        //影视剧id
        Long id = cmsMoviesVo.getId();
        //上传可能更换的图片
        String imageUrl= uploadFile2YunService.uploadFile2Oss(String.valueOf(id), cmsMovies.getImage(), PATH);
        if(StringUtils.isNotEmpty(imageUrl)){
            cmsMovies.setImage(imageUrl);
        }
        //修改
        cmsMoviesMapper.updateCmsMovies(cmsMovies);

        //影视剧和类型的关系表
        //修改
        cmsMoviesTypeMapper.deleteCmsMoviesTypeByMoviesId(id);
        //添加
        List<CmsMoviesType> list = Arrays.stream(cmsMoviesVo.getType()).map(a -> {
            CmsMoviesType cmsMoviesType = new CmsMoviesType();
            cmsMoviesType.setTypeId(a.longValue());
            cmsMoviesType.setMoviesId(id);
            return cmsMoviesType;
        }).collect(Collectors.toList());
        cmsMoviesTypeMapper.insertCmsMoviesType(list);
        //删除缓存
        removeRedis(cmsMoviesVo.getId());
        return 1 ;
    }

    /**
     * 批量删除影视数据信息
     *
     * @param ids 需要删除的影视数据信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsMoviesByIds(Long[] ids)
    {
        cmsMoviesMapper.deleteCmsMoviesByIds(ids);

        for (int i = 0; i < ids.length; i++) {
            cmsMoviesTypeMapper.deleteCmsMoviesTypeByMoviesId(ids[i]);
            //删除缓存
            removeRedis(ids[i]);
        }

        return 1 ;
    }

    /**
     * 删除影视数据信息信息
     *
     * @param id 影视数据信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsMoviesById(Long id ,String userName)
    {
        CmsMovies cmsMovies = cmsMoviesMapper.selectCmsMoviesById(id);
        if( ! cmsMovies.getCreateBy().equals(userName)){
            return -1;
        }
        cmsMoviesMapper.deleteCmsMoviesById(id);
        cmsMoviesTypeMapper.deleteCmsMoviesTypeByMoviesId(id);
        //删除缓存
        removeRedis(id);
        return 1;
    }

    /**
     * 删除缓存
     * @param id
     */
    private void removeRedis(Long id){
        //影视剧的缓存
        redisCache.deleteObject(RedisKey.getMoviesKey(id));
        //影视剧类型缓存
        redisCache.deleteObject(RedisKey.getMoviesTypeKey(id));
    }
}
