package com.ruoyi.core.service.impl;

import java.util.List;

import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.core.domain.CmsChannel;
import com.ruoyi.core.mapper.CmsChannelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.core.mapper.CmsTabMapper;
import com.ruoyi.core.domain.CmsTab;
import com.ruoyi.core.service.ICmsTabService;

/**
 * tab信息Service业务层处理
 * 
 * @author zqc
 * @date 2023-11-08
 */
@Service
public class CmsTabServiceImpl implements ICmsTabService 
{
    @Autowired
    private CmsTabMapper cmsTabMapper;
    @Autowired
    private CmsChannelMapper cmsChannelMapper;;
    @Autowired
    private RedisCache redisCache;;


    /**
     * 查询tab信息
     * 
     * @param id tab信息主键
     * @return tab信息
     */
    @Override
    public CmsTab selectCmsTabById(Long id)
    {
        return cmsTabMapper.selectCmsTabById(id);
    }

    /**
     * 查询tab信息列表
     * 
     * @param cmsTab tab信息
     * @return tab信息
     */
    @Override
    public List<CmsTab> selectCmsTabList(CmsTab cmsTab)
    {
        return cmsTabMapper.selectCmsTabList(cmsTab);
    }

    /**
     * 新增tab信息
     * 
     * @param cmsTab tab信息
     * @return 结果
     */
    @Override
    public int insertCmsTab(CmsTab cmsTab)
    {
        boolean b = judgeUserChannelId(cmsTab.getLoginUser(), cmsTab.getChannelId());
        if(!b){
            return -1;
        }
        //删除缓存
        removeRedis(cmsTab);

        return cmsTabMapper.insertCmsTab(cmsTab);
    }

    /**
     * 判断 渠道号，是不是当前用户的数据信息
     * @return
     */
    private boolean judgeUserChannelId(LoginUser loginUser,String channelId){
        CmsChannel cmsChannel = new CmsChannel();
        cmsChannel.setChannelSn(channelId);
        List<CmsChannel> cmsChannels = cmsChannelMapper.selectCmsChannelList(cmsChannel);
        return  cmsChannels.get(0).getPartnerId() == loginUser.getUserId().longValue();
    }

    /**
     * 修改tab信息
     * 
     * @param cmsTab tab信息
     * @return 结果
     */
    @Override
    public int updateCmsTab(CmsTab cmsTab)
    {
        boolean b = judgeUserChannelId(cmsTab.getLoginUser(), cmsTab.getChannelId());
        if(!b){
            return -1;
        }
        //删除缓存
        removeRedis(cmsTab);
        return cmsTabMapper.updateCmsTab(cmsTab);
    }

    /**
     * 批量删除tab信息
     * 
     * @param ids 需要删除的tab信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsTabByIds(Long[] ids)
    {
        return cmsTabMapper.deleteCmsTabByIds(ids);
    }

    /**
     * 删除tab信息信息
     * 
     * @param id tab信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsTabById(Long id)
    {
        CmsTab cmsTab = cmsTabMapper.selectCmsTabById(id);
        //删除缓存
        removeRedis(cmsTab);
        return cmsTabMapper.deleteCmsTabById(id);
    }

    /**
     * 删除缓存
     * @param cmsTab
     */
    private void removeRedis(CmsTab cmsTab){
        String tabKey = RedisKey.getTabKey(cmsTab.getType(),cmsTab.getChannelId());
        redisCache.deleteObject(tabKey);
    }

}
