package com.ruoyi.core.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.core.domain.CmsMovies;
import com.ruoyi.core.mapper.CmsMoviesMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.core.mapper.CmsEpisodeMapper;
import com.ruoyi.core.domain.CmsEpisode;
import com.ruoyi.core.service.ICmsEpisodeService;

/**
 * 剧集信息Service业务层处理
 * 
 * @author zqc
 * @date 2023-11-06
 */
@Service
public class CmsEpisodeServiceImpl implements ICmsEpisodeService 
{
    @Autowired
    private CmsEpisodeMapper cmsEpisodeMapper;
    @Autowired
    private CmsMoviesMapper cmsMoviesMapper;

    /**
     * 查询剧集信息
     * 
     * @param id 剧集信息主键
     * @return 剧集信息
     */
    @Override
    public CmsEpisode selectCmsEpisodeById(Long id)
    {
        return cmsEpisodeMapper.selectCmsEpisodeById(id);
    }

    /**
     * 查询剧集信息列表
     * 
     * @param cmsEpisode 剧集信息
     * @return 剧集信息
     */
    @Override
    public List<CmsEpisode> selectCmsEpisodeList(CmsEpisode cmsEpisode)
    {
        return cmsEpisodeMapper.selectCmsEpisodeList(cmsEpisode);
    }

    /**
     * 新增剧集信息
     * 
     * @param cmsEpisode 剧集信息
     * @return 结果
     */
    @Override
    public int insertCmsEpisode(CmsEpisode cmsEpisode)
    {
        if( ! judgeMoviesId(cmsEpisode.getMoviesId(), cmsEpisode.getCreateBy())){
            return -1;
        }
        //设置顺序号
        CmsEpisode para = new CmsEpisode();
        para.setMoviesId(cmsEpisode.getMoviesId());
        List<CmsEpisode> cmsGlassesEpisodes = cmsEpisodeMapper.selectCmsEpisodeList(para);
        Integer seq = cmsGlassesEpisodes.size() + 1;
        //设置顺序号
        cmsEpisode.setSeq(seq);

        return cmsEpisodeMapper.insertCmsEpisode(cmsEpisode);
    }

    /**
     * 修改剧集信息
     * 
     * @param cmsEpisode 剧集信息
     * @return 结果
     */
    @Override
    public int updateCmsEpisode(CmsEpisode cmsEpisode)
    {
        if( ! judge(cmsEpisode.getId(), cmsEpisode.getCreateBy())){
            return -1;
        }
        return cmsEpisodeMapper.updateCmsEpisode(cmsEpisode);
    }


    /**
     * 删除剧集信息信息
     * 
     * @param id 剧集信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsEpisodeById(Long id,String userName)
    {
        if( ! judge(id,userName)){
            return -1;
        }
        //删除数据，修改其他未删除的数据的顺序号
        CmsEpisode cmsEpisode = cmsEpisodeMapper.selectCmsEpisodeById(id);
        cmsEpisodeMapper.deleteSeq(cmsEpisode);

        return  cmsEpisodeMapper.deleteCmsEpisodeById(id);
    }

    /**
     * 修改指定的数据到 顺序号seq上
     * @param id
     * @param seq
     * @return
     */
    public int updateSeq(Long id,Integer seq,String userName) {
        if( ! judge(id,userName)){
            return -1;
        }


        CmsEpisode cmsEpisode = cmsEpisodeMapper.selectCmsEpisodeById(id);
        if(cmsEpisode.getSeq() == seq.intValue()){
            return 1;
        }

        Map para = new HashMap<>();
        Integer count = 1 ;
        if(cmsEpisode.getSeq() < seq.intValue()){
            count = -1 ;
            para.put("start",cmsEpisode.getSeq()) ;
            para.put("end",seq) ;
        }else{
            para.put("start",seq) ;
            para.put("end",cmsEpisode.getSeq()) ;
        }
        para.put("count",count) ;
        para.put("moviesId",cmsEpisode.getMoviesId()) ;
        //修改其他记录的顺序
        cmsEpisodeMapper.updateSeq(para);

        //修改当前记录的顺序
        cmsEpisode.setSeq(seq);
        cmsEpisodeMapper.updateCmsEpisode(cmsEpisode);

        return 1 ;
    }


    /**
     * 判断当前用户是不是这个剧集的所有者
     * @param id    剧集id
     * @param userName  用户账号
     * @return
     */
    private boolean judge(Long id,String userName){
        CmsEpisode cmsEpisode = cmsEpisodeMapper.selectCmsEpisodeById(id);
        CmsMovies cmsMovies = cmsMoviesMapper.selectCmsMoviesById(cmsEpisode.getMoviesId());
        return cmsMovies.getCreateBy().equals(userName);
    }

    /**
     * 判断当前用户是不是这个剧集的所有者
     * @param id        影视id
     * @param userName  账号
     * @return
     */
    private boolean judgeMoviesId(Long id,String userName){
        CmsMovies cmsMovies = cmsMoviesMapper.selectCmsMoviesById(id);
        return cmsMovies.getCreateBy().equals(userName);
    }
}
