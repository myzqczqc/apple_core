package com.ruoyi.core.service;

public interface ICmsShortMessageService {

    /**
     * 发送短信验证码
     * @param phone
     * @return
     */
    boolean sendShortMessage(String phone, String code);
}
