package com.ruoyi.core.service.impl;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.model.CannedAccessControlList;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.PutObjectResult;
import com.ruoyi.core.service.ICosService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;


@Service
public class CosServiceImpl implements ICosService {

    @Value("${cos.secretId}")
    private   String secretId;

    @Value("${cos.secretKey}")
    private  String secretKey;

    @Value("${cos.bucket}")
    private  String bucket;

    @Value("${cos.region}")
    private  String region;
    //南京的配置
    @Value("${cos.bucket2}")
    private  String bucket2;
    @Value("${cos.region2}")
    private  String region2;

    public static void main(String[] args) {
//        String s = CosServiceImpl.uploadFile(new File("C:\\Users\\86159\\Downloads\\111.txt"));
//        System.out.println(s);
    }
    @Override
    public  String uploadFile(File file, String filePath) {
        System.out.println("开始文件上传:"+file.getName());
        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        ClientConfig clientConfig = new ClientConfig(new com.qcloud.cos.region.Region(region));
        COSClient cosClient = new COSClient(cred,clientConfig);
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, filePath,  file);
        putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
        PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);
        System.out.println(putObjectResult.getETag());
        //返回南京的地址
        return "https://"+bucket2+".cos."+region2+".myqcloud.com/"+filePath;
    }
}
