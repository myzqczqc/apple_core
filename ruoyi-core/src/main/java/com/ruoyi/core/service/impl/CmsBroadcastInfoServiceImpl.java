package com.ruoyi.core.service.impl;

import java.io.IOException;
import java.util.List;

import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.core.service.IUploadFile2YunService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.core.mapper.CmsBroadcastInfoMapper;
import com.ruoyi.core.domain.CmsBroadcastInfo;
import com.ruoyi.core.service.ICmsBroadcastInfoService;

/**
 * 直播流信息Service业务层处理
 * 
 * @author zqc
 * @date 2023-11-06
 */
@Service
public class CmsBroadcastInfoServiceImpl implements ICmsBroadcastInfoService 
{
    @Autowired
    private CmsBroadcastInfoMapper cmsBroadcastInfoMapper;
    @Autowired
    private IUploadFile2YunService uploadFile2YunService;
    @Autowired
    private RedisCache redisCache;

    private static final String PATH = "broadcast";

    /**
     * 查询直播流信息
     * 
     * @param id 直播流信息主键
     * @return 直播流信息
     */
    @Override
    public CmsBroadcastInfo selectCmsBroadcastInfoById(Long id)
    {
        return cmsBroadcastInfoMapper.selectCmsBroadcastInfoById(id);
    }

    /**
     * 查询直播流信息列表
     * 
     * @param cmsBroadcastInfo 直播流信息
     * @return 直播流信息
     */
    @Override
    public List<CmsBroadcastInfo> selectCmsBroadcastInfoList(CmsBroadcastInfo cmsBroadcastInfo)
    {
        return cmsBroadcastInfoMapper.selectCmsBroadcastInfoList(cmsBroadcastInfo);
    }

    /**
     * 新增直播流信息
     * 
     * @param cmsBroadcastInfo 直播流信息
     * @return 结果
     */
    @Override
    public int insertCmsBroadcastInfo(CmsBroadcastInfo cmsBroadcastInfo) throws IOException {

        cmsBroadcastInfoMapper.insertCmsBroadcastInfo(cmsBroadcastInfo);
        String imageUrl= uploadFile2YunService.uploadFile2Oss(String.valueOf(cmsBroadcastInfo.getId()), cmsBroadcastInfo.getImage(), PATH);
        if(StringUtils.isNotEmpty(imageUrl)){
            cmsBroadcastInfo.setImage(imageUrl);
            cmsBroadcastInfoMapper.updateCmsBroadcastInfo(cmsBroadcastInfo);
        }
        return 1;
    }

    /**
     * 修改直播流信息
     * 
     * @param cmsBroadcastInfo 直播流信息
     * @return 结果
     */
    @Override
    public int updateCmsBroadcastInfo(CmsBroadcastInfo cmsBroadcastInfo) throws IOException {
        //查看修改着是不是创建者
        CmsBroadcastInfo temp = cmsBroadcastInfoMapper.selectCmsBroadcastInfoById(cmsBroadcastInfo.getId());
        if( ! temp.getCreateBy().equals(cmsBroadcastInfo.getUpdateBy())){
            return -1;
        }


        String imageUrl= uploadFile2YunService.uploadFile2Oss(String.valueOf(cmsBroadcastInfo.getId()), cmsBroadcastInfo.getImage(), PATH);
        if(StringUtils.isNotEmpty(imageUrl)){
            cmsBroadcastInfo.setImage(imageUrl);
        }
        //删除缓存
        removeRedis(cmsBroadcastInfo.getId());
        return cmsBroadcastInfoMapper.updateCmsBroadcastInfo(cmsBroadcastInfo);
    }

    /**
     * 批量删除直播流信息
     * 
     * @param ids 需要删除的直播流信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsBroadcastInfoByIds(Long[] ids)
    {
        return cmsBroadcastInfoMapper.deleteCmsBroadcastInfoByIds(ids);
    }

    /**
     * 删除直播流信息信息
     * 
     * @param id 直播流信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsBroadcastInfoById(Long id)
    {
        //删除缓存
        removeRedis(id);
        return cmsBroadcastInfoMapper.deleteCmsBroadcastInfoById(id);
    }


    /**
     * 删除缓存
     * @param id
     */
    private void removeRedis(Long id){
        String broadcastKey = RedisKey.getBroadcastKey(id);
        redisCache.deleteObject(broadcastKey);
    }
}
