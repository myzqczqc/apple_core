package com.ruoyi.core.service.impl;

import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.core.domain.CmsAppType;
import com.ruoyi.core.mapper.CmsAppTypeMapper;
import com.ruoyi.core.service.ICmsAppTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * app子分类Service业务层处理
 *
 * @author zqc
 * @date 2023-11-25
 */
@Service
public class CmsAppTypeServiceImpl implements ICmsAppTypeService {
    @Autowired
    private CmsAppTypeMapper cmsAppTypeMapper;
    @Autowired
    private RedisCache redisCache;


    /**
     * 查询app子分类
     *
     * @param id app子分类主键
     * @return app子分类
     */
    @Override
    public CmsAppType selectCmsAppTypeById(Long id) {
        return cmsAppTypeMapper.selectCmsAppTypeById(id);
    }

    /**
     * 查询app子分类列表
     *
     * @param cmsAppType app子分类
     * @return app子分类
     */
    @Override
    public List<CmsAppType> selectCmsAppTypeList(CmsAppType cmsAppType) {
        return cmsAppTypeMapper.selectCmsAppTypeList(cmsAppType);
    }

    /**
     * 新增app子分类
     *
     * @param cmsAppType app子分类
     * @return 结果
     */
    @Override
    public int insertCmsAppType(CmsAppType cmsAppType) {
        //获取最大顺序号
        Integer maxSeq = cmsAppTypeMapper.selectAppTypeMaxSeq(cmsAppType.getAp());
        cmsAppType.setSeq(Objects.isNull(maxSeq) ? 1 : maxSeq + 1);
        //删除缓存
        removeRedis(cmsAppType.getAp());
        return cmsAppTypeMapper.insertCmsAppType(cmsAppType);
    }

    /**
     * 修改app子分类
     *
     * @param cmsAppType app子分类
     * @return 结果
     */
    @Override
    public int updateCmsAppType(CmsAppType cmsAppType) {
        //删除缓存
        removeRedis(cmsAppType.getAp());
        return cmsAppTypeMapper.updateCmsAppType(cmsAppType);
    }

    /**
     * 批量删除app子分类
     *
     * @param ids 需要删除的app子分类主键
     * @return 结果
     */
    @Override
    public int deleteCmsAppTypeByIds(Long[] ids) {
        return cmsAppTypeMapper.deleteCmsAppTypeByIds(ids);
    }

    /**
     * 删除app子分类信息
     *
     * @param id app子分类主键
     * @return 结果
     */
    @Override
    public int deleteCmsAppTypeById(Long id) {
        //删除缓存
        CmsAppType cmsAppType = cmsAppTypeMapper.selectCmsAppTypeById(id);
        removeRedis(cmsAppType.getAp());
        return cmsAppTypeMapper.deleteCmsAppTypeById(id);
    }

    /**
     * 修改指定的数据到 顺序号seq上
     *
     * @param id
     * @param seq
     * @return
     */
    @Override
    public int updateSeq(Long id, Integer seq) {
        CmsAppType cmsAppType = cmsAppTypeMapper.selectCmsAppTypeById(id);
        Map<String, Object> para = new HashMap<>();
        int count = 1;
        if (cmsAppType.getSeq() < seq) {
            // 序号调大了，那么从原序号往后到新序号的n条数据，序号依次-1
            // 例如3->8，那么4-8一共5条数据的序号依次-1，然后原来的3更新为8
            count = -1;
            para.put("start", cmsAppType.getSeq() + 1);
            para.put("end", seq);
        } else {
            // 序号调小了，那么从原序号往前到新序号的n条数据，序号依次+1
            // 例如8->3，那么3-7一共5条数据的序号依次+1
            para.put("start", seq);
            para.put("end", cmsAppType.getSeq() - 1);
        }
        para.put("count", count);
        para.put("ap", cmsAppType.getAp());
        cmsAppTypeMapper.updateAppTypeSeq(para);

        // 更新
        cmsAppType.setSeq(seq);
        cmsAppTypeMapper.updateCmsAppType(cmsAppType);

        removeRedis(cmsAppType.getAp());
        return 1;
    }

    /**
     * 删除缓存
     *
     * @param ap
     */
    private void removeRedis(Integer ap) {
        redisCache.deleteObject(RedisKey.getApListKey(ap));
    }

}
