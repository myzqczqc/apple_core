package com.ruoyi.core.service;

import java.util.List;
import com.ruoyi.core.domain.CmsEpisode;

/**
 * 剧集信息Service接口
 * 
 * @author zqc
 * @date 2023-11-06
 */
public interface ICmsEpisodeService 
{
    /**
     * 查询剧集信息
     * 
     * @param id 剧集信息主键
     * @return 剧集信息
     */
    public CmsEpisode selectCmsEpisodeById(Long id);

    /**
     * 查询剧集信息列表
     * 
     * @param cmsEpisode 剧集信息
     * @return 剧集信息集合
     */
    public List<CmsEpisode> selectCmsEpisodeList(CmsEpisode cmsEpisode);

    /**
     * 新增剧集信息
     * 
     * @param cmsEpisode 剧集信息
     * @return 结果
     */
    public int insertCmsEpisode(CmsEpisode cmsEpisode);

    /**
     * 修改剧集信息
     * 
     * @param cmsEpisode 剧集信息
     * @return 结果
     */
    public int updateCmsEpisode(CmsEpisode cmsEpisode);

    /**
     * 批量删除剧集信息
     * 
     * @param id 需要删除的剧集信息主键集合
     * @return 结果
     */
    public int deleteCmsEpisodeById(Long id,String userName) ;
    /**
     * 修改指定的数据到 顺序号seq上
     * @param id
     * @param seq
     * @return
     */
    public int updateSeq(Long id,Integer seq,String userName) ;
}
