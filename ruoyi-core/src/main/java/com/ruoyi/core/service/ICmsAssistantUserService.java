package com.ruoyi.core.service;

import java.util.List;

import com.ruoyi.api.model.AssistantUser;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.core.domain.CmsAssistantUser;

/**
 * 手机助手用户Service接口
 *
 * @author zqc
 * @date 2023-11-21
 */
public interface ICmsAssistantUserService
{
    /**
     * 查询手机助手用户
     *
     * @param id 手机助手用户主键
     * @return 手机助手用户
     */
    public CmsAssistantUser selectCmsAssistantUserById(Long id);

    /**
     * 根据手机号查询用户
     * @param phone 手机号
     * @return 手机助手用户
     */
    CmsAssistantUser selectCmsAssistantUserByPhone(String phone);

    /**
     * 查询手机助手用户列表
     *
     * @param cmsAssistantUser 手机助手用户
     * @return 手机助手用户集合
     */
    public List<CmsAssistantUser> selectCmsAssistantUserList(CmsAssistantUser cmsAssistantUser);
    /**
     * 检查手机助手用户用户名唯一性
     * @param:
     * @return:
     */
    public boolean checkUserNameUnique(CmsAssistantUser user);

    /**
     * 新增手机助手用户
     *
     * @param cmsAssistantUser 手机助手用户
     * @return 结果
     */
    public int insertCmsAssistantUser(CmsAssistantUser cmsAssistantUser);

    /**
     * 修改手机助手用户
     *
     * @param cmsAssistantUser 手机助手用户
     * @return 结果
     */
    public int updateCmsAssistantUser(CmsAssistantUser cmsAssistantUser);
    public int unbindCmsAssistantPhone(CmsAssistantUser cmsAssistantUser);
    public boolean updateUserAvatar(String userName, String avatar);

    /**
     * 批量删除手机助手用户
     *
     * @param ids 需要删除的手机助手用户主键集合
     * @return 结果
     */
    public int deleteCmsAssistantUserByIds(Long[] ids);

    /**
     * 删除手机助手用户信息
     *
     * @param id 手机助手用户主键
     * @return 结果
     */
    public int deleteCmsAssistantUserById(Long id);
    /**
     * 修改用户密码
     * @param:  userName 用户名
     * @return: password 密码
     */
    public int updateUserPwd(String userName, String password);
    /**
     * 登录逻辑处理
     * @param:
     * @return:
     */
    public AjaxResult login(String userName, String password,String...isPhoneLogin);
}
