package com.ruoyi.core.service;

import java.io.IOException;
import java.util.List;
import com.ruoyi.core.domain.CmsCoreVersion;

/**
 * core应用历史版本信息Service接口
 * 
 * @author zqc
 * @date 2023-11-23
 */
public interface ICmsCoreVersionService 
{
    /**
     * 查询core应用历史版本信息
     * 
     * @param id core应用历史版本信息主键
     * @return core应用历史版本信息
     */
    public CmsCoreVersion selectCmsCoreVersionById(Long id);

    /**
     * 查询core应用历史版本信息列表
     * 
     * @param cmsCoreVersion core应用历史版本信息
     * @return core应用历史版本信息集合
     */
    public List<CmsCoreVersion> selectCmsCoreVersionList(CmsCoreVersion cmsCoreVersion);

    /**
     * 新增core应用历史版本信息
     * 
     * @param cmsCoreVersion core应用历史版本信息
     * @return 结果
     */
    public int insertCmsCoreVersion(CmsCoreVersion cmsCoreVersion) throws IOException;

    /**
     * 修改core应用历史版本信息
     * 
     * @param cmsCoreVersion core应用历史版本信息
     * @return 结果
     */
    public int updateCmsCoreVersion(CmsCoreVersion cmsCoreVersion) throws IOException;

    /**
     * 批量删除core应用历史版本信息
     * 
     * @param ids 需要删除的core应用历史版本信息主键集合
     * @return 结果
     */
    public int deleteCmsCoreVersionByIds(Long[] ids);

    /**
     * 删除core应用历史版本信息信息
     * 
     * @param id core应用历史版本信息主键
     * @return 结果
     */
    public int deleteCmsCoreVersionById(Long id);
}
