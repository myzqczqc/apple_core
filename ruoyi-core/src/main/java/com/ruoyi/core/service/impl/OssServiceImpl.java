package com.ruoyi.core.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.*;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.core.service.IOssService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

@Service
public class OssServiceImpl implements IOssService {

    Logger logger = LoggerFactory.getLogger(OssServiceImpl.class);

    @Value("${oss.endpoint}")
    private   String endpoint;

    @Value("${oss.accessKeyId}")
    private  String accessKeyId;

    @Value("${oss.accessKeySecret}")
    private  String accessKeySecret;

    @Value("${oss.bucketName}")
    private  String bucketName;

    @Value("${oss.url}")
    private  String url;


    @Override
    public String uploadImageToOss(File file, String filePath) throws IOException{
        return uploaFileToOss(file,filePath,"image/jpeg") ;
    }


    @Override
    public String uploadFileToOss(File file, String filePath) throws IOException {
        return uploaFileToOss(file,filePath,"application/octet-stream") ;
    }


    private String uploaFileToOss(File file, String filePath,String contentType) throws IOException{
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
//        boolean b = ossClient.doesBucketExist("aitek-moyu");
// 创建PutObjectRequest对象。
// 填写Bucket名称、Object完整路径和本地文件的完整路径。Object完整路径中不能包含Bucket名称。
// 如果未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件。
        logger.info("文件地址:"+ RuoYiConfig.getOssGen() + "/" + filePath);
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, RuoYiConfig.getOssGen() + "/" + filePath,file);

// 如果需要上传时设置存储类型和访问权限，请参考以下示例代码。
        ObjectMetadata metadata = new ObjectMetadata();
//     metadata.setHeader(OSSHeaders.OSS_STORAGE_CLASS, StorageClass.Standard.toString());
        metadata.setObjectAcl(CannedAccessControlList.PublicRead);
//     设置 ContentType   https://help.aliyun.com/document_detail/39522.htm?spm=a2c4g.11186623.2.6.1a6c401cskCVSO#concept-5041
        metadata.setContentType(contentType);
        putObjectRequest.setMetadata(metadata);

        PutObjectResult putObjectResult = ossClient.putObject(putObjectRequest);
        String eTag = putObjectResult.getETag();
        logger.info(eTag);

        //设置权限 这里是公开读
        ossClient.setBucketAcl(bucketName, CannedAccessControlList.PublicRead);

// 关闭OSSClient。
        ossClient.shutdown();
        return url + RuoYiConfig.getOssGen() + "/" + filePath ;
    }


}
