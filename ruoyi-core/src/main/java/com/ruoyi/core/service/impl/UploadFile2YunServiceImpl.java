package com.ruoyi.core.service.impl;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.sign.Md5Utils;
import com.ruoyi.core.service.ICosService;
import com.ruoyi.core.service.IOssService;
import com.ruoyi.core.service.IUploadFile2YunService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Random;

@Service
public class UploadFile2YunServiceImpl implements IUploadFile2YunService {

    @Autowired
    private IOssService ossService;
    @Autowired
    private ICosService cosService;

    @Override
    public String uploadFile2Oss(String id, String url, String path) throws IOException {
        if(StringUtils.isEmpty(url)){
            return null;
        }
        path+="2";
        //获取加密后的名字
        id = getFileName(id);
        int i = url.indexOf("profile/upload");
        if(i>=0){
            String s = url.substring(i).replaceAll("profile/upload", "");
            String s1 = RuoYiConfig.getUploadPath() + s;
            File file = new File(s1);
            if(file.exists()){
                //文件扩展名
                int i1 = s1.lastIndexOf(".");
                String ext = s1.substring(i1);
                //apk上传到oss
//                return  ossService.uploadFileToOss(file, path + "/" + id + ext)+"?a="+new Random().nextInt(99999);
                return  cosService.uploadFile(file, path + "/" + id + ext)+"?a="+new Random().nextInt(99999);
            }
        }else{
            //本地文件的绝对地址
            File file = new File(url);
            if(file.exists()){
                //文件扩展名
                int i1 = url.lastIndexOf(".");
                String ext = url.substring(i1);
                //apk上传到oss
//                return  ossService.uploadFileToOss(file, path + "/" + id + ext)+"?a="+new Random().nextInt(99999);
                return  cosService.uploadFile(file, path + "/" + id + ext)+"?a="+new Random().nextInt(99999);
            }
        }
        return null;
    }

    /**
     * id 加密
     * @param id
     * @return
     */
    private String getFileName(String id){
        return Md5Utils.hash("yidao"+id+"2023@SJZ");
    }

    public static void main(String[] args) {
        String aaa = "yidao12023@SZJ";
        System.out.println(Md5Utils.hash(aaa));
    }

}
