package com.ruoyi.core.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.core.mapper.CmsLanguageMapper;
import com.ruoyi.core.domain.CmsLanguage;
import com.ruoyi.core.service.ICmsLanguageService;

/**
 * 语言字典信息Service业务层处理
 * 
 * @author zqc
 * @date 2023-11-21
 */
@Service
public class CmsLanguageServiceImpl implements ICmsLanguageService 
{
    @Autowired
    private CmsLanguageMapper cmsLanguageMapper;

    /**
     * 查询语言字典信息
     * 
     * @param id 语言字典信息主键
     * @return 语言字典信息
     */
    @Override
    public CmsLanguage selectCmsLanguageById(Long id)
    {
        return cmsLanguageMapper.selectCmsLanguageById(id);
    }

    /**
     * 查询语言字典信息列表
     * 
     * @param cmsLanguage 语言字典信息
     * @return 语言字典信息
     */
    @Override
    public List<CmsLanguage> selectCmsLanguageList(CmsLanguage cmsLanguage)
    {
        return cmsLanguageMapper.selectCmsLanguageList(cmsLanguage);
    }

    /**
     * 新增语言字典信息
     * 
     * @param cmsLanguage 语言字典信息
     * @return 结果
     */
    @Override
    public int insertCmsLanguage(CmsLanguage cmsLanguage)
    {
        return cmsLanguageMapper.insertCmsLanguage(cmsLanguage);
    }

    /**
     * 修改语言字典信息
     * 
     * @param cmsLanguage 语言字典信息
     * @return 结果
     */
    @Override
    public int updateCmsLanguage(CmsLanguage cmsLanguage)
    {
        return cmsLanguageMapper.updateCmsLanguage(cmsLanguage);
    }

    /**
     * 批量删除语言字典信息
     * 
     * @param ids 需要删除的语言字典信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsLanguageByIds(Long[] ids)
    {
        return cmsLanguageMapper.deleteCmsLanguageByIds(ids);
    }

    /**
     * 删除语言字典信息信息
     * 
     * @param id 语言字典信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsLanguageById(Long id)
    {
        return cmsLanguageMapper.deleteCmsLanguageById(id);
    }
}
