package com.ruoyi.core.service;

import java.util.List;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.core.domain.CmsApp;

/**
 * app配置Service接口
 *
 * @author zqc
 * @date 2023-11-17
 */
public interface ICmsAppService
{
    /**
     * 查询app配置
     *
     * @param id app配置主键
     * @return app配置
     */
    public CmsApp selectCmsAppById(Long id);

    /**
     * 查询app配置列表
     *
     * @param cmsApp app配置
     * @return app配置集合
     */
    public List<CmsApp> selectCmsAppList(CmsApp cmsApp);
    /**
     * 查询最新的序列号
     * @param:
     * @return:
     */
    public CmsApp selectMaxSeq(CmsApp cmsApp);

    /**
     * 新增app配置
     *
     * @param cmsApp app配置
     * @return 结果
     */
    public int insertCmsApp(CmsApp cmsApp);

    /**
     * 修改app配置
     *
     * @param cmsApp app配置
     * @return 结果
     */
    public int updateCmsApp(CmsApp cmsApp);
    /**
     * 批量更新app
     * @param:
     * @return:
     */
    public int updateCmsApps(List<CmsApp> cmsApps,LoginUser loginUser);

    /**
     * 批量删除app配置
     *
     * @param ids 需要删除的app配置主键集合
     * @return 结果
     */
    public int deleteCmsAppByIds(Long[] ids);

    /**
     * 删除app配置信息
     *
     * @param id app配置主键
     * @return 结果
     */
    public int deleteCmsAppById(Long id ,LoginUser loginUser);

    /**
     * 根据apkId删除app配置
     * @param apkId
     * @return
     */
    int deleteCmsAppByApkId(Long apkId);
}
