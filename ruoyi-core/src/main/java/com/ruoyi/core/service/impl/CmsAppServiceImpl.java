package com.ruoyi.core.service.impl;

import java.util.List;

import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.core.domain.CmsChannel;
import com.ruoyi.core.domain.CmsTab;
import com.ruoyi.core.mapper.CmsChannelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.core.mapper.CmsAppMapper;
import com.ruoyi.core.domain.CmsApp;
import com.ruoyi.core.service.ICmsAppService;
import org.springframework.transaction.annotation.Transactional;

/**
 * app配置Service业务层处理
 *
 * @author zqc
 * @date 2023-11-17
 */
@Service
@Transactional
public class CmsAppServiceImpl implements ICmsAppService
{
    @Autowired
    private CmsAppMapper cmsAppMapper;
    @Autowired
    private CmsChannelMapper cmsChannelMapper;
    @Autowired
    private RedisCache redisCache;

    /**
     * 查询app配置
     *
     * @param id app配置主键
     * @return app配置
     */
    @Override
    public CmsApp selectCmsAppById(Long id)
    {
        return cmsAppMapper.selectCmsAppById(id);
    }

    /**
     * 查询app配置列表
     *
     * @param cmsApp app配置
     * @return app配置
     */
    @Override
    public List<CmsApp> selectCmsAppList(CmsApp cmsApp)
    {
        return cmsAppMapper.selectCmsAppList(cmsApp);
    }

    @Override
    public CmsApp selectMaxSeq(CmsApp cmsApp) {
        return cmsAppMapper.selectMaxSeq(cmsApp);
    }

    /**
     * 新增app配置
     *
     * @param cmsApp app配置
     * @return 结果
     */
    @Override
    public int insertCmsApp(CmsApp cmsApp)
    {
        if( ! judgeUser(cmsApp.getChannelId(),cmsApp.getLoginUser())){
            return -1;
        }
        CmsApp lastApp = new CmsApp();
        lastApp.setAp(cmsApp.getAp());
        lastApp.setApType(cmsApp.getApType());
        lastApp.setChannelId(cmsApp.getChannelId());
        lastApp = cmsAppMapper.selectMaxSeq(lastApp);
        if (ObjectUtil.isNull(lastApp)) {
            cmsApp.setSeq(1L);
        } else {
            cmsApp.setSeq(lastApp.getSeq() + 1);
        }

        cmsAppMapper.insertCmsApp(cmsApp);
        //删除缓存
        removeRedis(cmsApp);

        return 1;
    }

    /**
     * 修改app配置
     *
     * @param cmsApp app配置
     * @return 结果
     */
    @Override
    public int updateCmsApp(CmsApp cmsApp)
    {
        //删除缓存
        removeRedis(cmsApp);
        return cmsAppMapper.updateCmsApp(cmsApp);
    }
    @Override
    public int updateCmsApps(List<CmsApp> cmsApps,LoginUser loginUser) {
        CmsApp cmsApp = cmsAppMapper.selectCmsAppById(cmsApps.get(0).getId());
        if( ! judgeUser(cmsApp.getChannelId(),loginUser)){
            return -1;
        }

        cmsApps.forEach(v -> {
            v.setUpdateTime(DateUtils.getNowDate());
            cmsAppMapper.updateCmsApp(v);
        });

        //删除缓存
        removeRedis(cmsApp);
        return 1;
    }
    /**
     * 批量删除app配置
     *
     * @param ids 需要删除的app配置主键
     * @return 结果
     */
    @Override
    public int deleteCmsAppByIds(Long[] ids)
    {
        return cmsAppMapper.deleteCmsAppByIds(ids);
    }

    /**
     * 删除app配置信息
     *
     * @param id app配置主键
     * @return 结果
     */
    @Override
    public int deleteCmsAppById(Long id,LoginUser loginUser)
    {
        CmsApp cmsApp = cmsAppMapper.selectCmsAppById(id);
        if( ! judgeUser(cmsApp.getChannelId(),loginUser)){
            return -1;
        }
        cmsAppMapper.deleteSeq(cmsApp);
        cmsAppMapper.deleteCmsAppById(id);

        //删除缓存
        removeRedis(cmsApp);
        return 1 ;
    }

    /**
     * 根据apkId删除app配置
     *
     * @param apkId
     * @return
     */
    @Override
    public int deleteCmsAppByApkId(Long apkId) {
        CmsApp cmsApp = new CmsApp();
        cmsApp.setApkId(apkId);
        List<CmsApp> appList = cmsAppMapper.selectCmsAppList(cmsApp);
        int a = cmsAppMapper.deleteCmsAppByApkId(apkId);
        // 删除缓存
        appList.forEach(this::removeRedis);
        return a;
    }

    /**
     * 判断当前数据是不是当前用户的
     * @param channelId
     * @param loginUser
     * @return
     */
    private boolean judgeUser(String channelId, LoginUser loginUser){
        CmsChannel cmsChannel = cmsChannelMapper.selectCmsChannelBySn(channelId);
        if(StringUtils.isNull(cmsChannel)){
            return false;
        }
        return cmsChannel.getPartnerId() == loginUser.getUserId().longValue();
    }

    /**
     * 删除缓存
     * @param cmsApp
     */
    private void removeRedis(CmsApp cmsApp){
        String appInfoKey = RedisKey.getAppListKey(cmsApp.getApType(), cmsApp.getChannelId());
        redisCache.deleteObject(appInfoKey);
    }
}
