package com.ruoyi.core.service.impl;

import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.core.domain.*;
import com.ruoyi.core.enums.ClickTypeEnums;
import com.ruoyi.core.mapper.*;
import com.ruoyi.core.service.ICmsRecommendService;
import com.ruoyi.core.service.IUploadFile2YunService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * tab的recommend信息Service业务层处理
 * 
 * @author zqc
 * @date 2023-11-08
 */
@Service
public class CmsRecommendServiceImpl implements ICmsRecommendService 
{
    @Autowired
    private CmsRecommendMapper cmsRecommendMapper;
    @Autowired
    private CmsBroadcastInfoMapper cmsBroadcastInfoMapper;
    @Autowired
    private CmsMoviesMapper cmsMoviesMapper;
    @Autowired
    private CmsApkInfoMapper cmsApkInfoMapper;
    @Autowired
    private CmsTabMapper cmsTabMapper;
    @Autowired
    private IUploadFile2YunService uploadFile2YunService;
    @Autowired
    private RedisCache redisCache;

    private static final String PATH = "recommend";
    /**
     * 查询tab的recommend信息
     * 
     * @param id tab的recommend信息主键
     * @return tab的recommend信息
     */
    @Override
    public CmsRecommend selectCmsRecommendById(Long id)
    {
        CmsRecommend cmsRecommend = cmsRecommendMapper.selectCmsRecommendById(id);
        //获取电台信息
        if(cmsRecommend.getClickType().equals(ClickTypeEnums.BROADCAST.getName())){
            CmsBroadcastInfo cmsBroadcastInfo = cmsBroadcastInfoMapper.selectCmsBroadcastInfoById(cmsRecommend.getClickId());
            cmsRecommend.setClickInfo(cmsBroadcastInfo);
        }else if(cmsRecommend.getClickType().equals(ClickTypeEnums.MOVIES.getName())){
            CmsMovies cmsMovies = cmsMoviesMapper.selectCmsMoviesById(cmsRecommend.getClickId());
            cmsRecommend.setClickInfo(cmsMovies);
        }else if(cmsRecommend.getClickType().equals(ClickTypeEnums.APK.getName())){
            CmsApkInfo cmsApkInfo = cmsApkInfoMapper.selectCmsApkInfoById(cmsRecommend.getClickId());
            cmsRecommend.setClickInfo(cmsApkInfo);
        }
        return cmsRecommend;
    }

    /**
     * 查询tab的recommend信息列表
     * 
     * @param cmsRecommend tab的recommend信息
     * @return tab的recommend信息
     */
    @Override
    public List<CmsRecommend> selectCmsRecommendList(CmsRecommend cmsRecommend)
    {
        List<CmsRecommend> cmsRecommends = cmsRecommendMapper.selectCmsRecommendList(cmsRecommend);
        if( ! cmsRecommends.isEmpty()){
            //直播流的id
            Long[] ids = cmsRecommends.stream().filter(a -> a.getClickType().equals(ClickTypeEnums.BROADCAST.getName())).map(a -> a.getClickId()).toArray(Long[]::new);
            if(ids.length>0){
                //直播流的数据信息
                List<CmsBroadcastInfo> cmsBroadcastInfos = cmsBroadcastInfoMapper.selectCmsBroadcastInfoByIds(ids);
                //直播流的数据传入到返回信息中
                for (CmsRecommend temp: cmsRecommends  ) {
                    if(temp.getClickType().equals(ClickTypeEnums.BROADCAST.getName())){
                        CmsBroadcastInfo cmsBroadcastInfo = cmsBroadcastInfos.stream().filter(a -> a.getId() == temp.getClickId().longValue()).findFirst().orElse(null);
                        temp.setClickInfo(cmsBroadcastInfo);
                    }
                }
            }
            //影视剧
            Long[] moviesId = cmsRecommends.stream().filter(a -> a.getClickType().equals(ClickTypeEnums.MOVIES.getName())).map(a -> a.getClickId()).toArray(Long[]::new);
            if(moviesId.length>0){
                List<CmsMovies> cmsMovies = cmsMoviesMapper.selectCmsMoviesByIds(moviesId);
                for (CmsRecommend temp: cmsRecommends  ) {
                    if(temp.getClickType().equals(ClickTypeEnums.MOVIES.getName())){
                        CmsMovies clickInfo = cmsMovies.stream().filter(a ->a.getId() == temp.getClickId().longValue()).findFirst().orElse(null);
                        temp.setClickInfo(clickInfo);
                    }
                }
            }
            //apk
            Long[] apkId = cmsRecommends.stream().filter(a -> a.getClickType().equals(ClickTypeEnums.APK.getName())).map(a -> a.getClickId()).toArray(Long[]::new);
            if(apkId.length>0){
                List<CmsApkInfo> cmsApks = cmsApkInfoMapper.selectCmsApkInfoByIds(apkId);
                for (CmsRecommend temp: cmsRecommends  ) {
                    if(temp.getClickType().equals(ClickTypeEnums.APK.getName())){
                        CmsApkInfo cmsApkInfo = cmsApks.stream().filter(a -> a.getId() == temp.getClickId().longValue()).findFirst().orElse(null);
                        temp.setClickInfo(cmsApkInfo);
                    }
                }

            }
        }
        return cmsRecommends ;
    }

    /**
     * 新增tab的recommend信息
     * 
     * @param cmsRecommend tab的recommend信息
     * @return 结果
     */
    @Override
    public int insertCmsRecommend(CmsRecommend cmsRecommend) throws IOException {
        if( ! judgeUserBanner(cmsRecommend.getTabId(),cmsRecommend.getCreateBy())){
            return -1;
        }
        //获取顺序号
        CmsRecommend para  = new CmsRecommend();
        para.setTabId(cmsRecommend.getTabId());
        List<CmsRecommend> recommendList = cmsRecommendMapper.selectCmsRecommendList(para);
        //顺序号
        Long seq = Long.valueOf(recommendList.size())+1;
        cmsRecommend.setSeq(seq);
        cmsRecommendMapper.insertCmsRecommend(cmsRecommend);
        //上传图片信息
        if (StringUtils.isNotEmpty(cmsRecommend.getImage())) {
            String imageUrl= uploadFile2YunService.uploadFile2Oss(String.valueOf(cmsRecommend.getId()), cmsRecommend.getImage(), PATH);
            if(StringUtils.isNotEmpty(imageUrl)){
                cmsRecommend.setImage(imageUrl);
                cmsRecommendMapper.updateCmsRecommend(cmsRecommend);
            }
        }
        //上传icon
        if (StringUtils.isNotEmpty(cmsRecommend.getIcon())) {
            String iconUrl= uploadFile2YunService.uploadFile2Oss(String.valueOf(cmsRecommend.getId()), cmsRecommend.getIcon(), PATH);
            if(StringUtils.isNotEmpty(iconUrl)){
                cmsRecommend.setIcon(iconUrl);
                cmsRecommendMapper.updateCmsRecommend(cmsRecommend);
            }
        }

        //删除缓存
        removeRedis(cmsRecommend.getTabId());
        return 1;
    }

    /**
     * 修改tab的recommend信息
     * 
     * @param cmsRecommend tab的recommend信息
     * @return 结果
     */
    @Override
    public int updateCmsRecommend(CmsRecommend cmsRecommend) throws IOException {
        if( ! judgeUserBanner(cmsRecommend.getTabId(),cmsRecommend.getUpdateBy())){
            return -1;
        }
        //上传图片信息
        if (StringUtils.isNotEmpty(cmsRecommend.getImage())) {
            String imageUrl= uploadFile2YunService.uploadFile2Oss(String.valueOf(cmsRecommend.getId()), cmsRecommend.getImage(), PATH);
            if(StringUtils.isNotEmpty(imageUrl)){
                cmsRecommend.setImage(imageUrl);
            }
        }
        //上传icon
        if (StringUtils.isNotEmpty(cmsRecommend.getIcon())) {
            String iconUrl= uploadFile2YunService.uploadFile2Oss(String.valueOf(cmsRecommend.getId()), cmsRecommend.getIcon(), PATH);
            if(StringUtils.isNotEmpty(iconUrl)){
                cmsRecommend.setIcon(iconUrl);
            }
        }

        //删除缓存
        removeRedis(cmsRecommend.getTabId());
        return cmsRecommendMapper.updateCmsRecommend(cmsRecommend);
    }

    /**
     * 批量删除tab的recommend信息
     * 
     * @param ids 需要删除的tab的recommend信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsRecommendByIds(Long[] ids)
    {
        return cmsRecommendMapper.deleteCmsRecommendByIds(ids);
    }

    /**
     * 删除tab的recommend信息信息
     * 
     * @param id tab的recommend信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsRecommendById(Long id,String userName)
    {

        CmsRecommend cmsRecommend = cmsRecommendMapper.selectCmsRecommendById(id);
        if( ! judgeUserBanner(cmsRecommend.getTabId(),userName)){
            return -1;
        }
        //删除
        cmsRecommendMapper.deleteCmsRecommendById(id);
        //其他数据seq 需要修改
        cmsRecommendMapper.deleteSeq(cmsRecommend);
        //删除缓存
        removeRedis(cmsRecommend.getTabId());

        return 1;
    }

    /**
     * 修改指定的数据到 顺序号seq上
     * @param id
     * @param seq
     * @return
     */
    public int updateSeq(Long id,Integer seq,String userName) {
        CmsRecommend cmsRecommend = cmsRecommendMapper.selectCmsRecommendById(id);
        if( ! judgeUserBanner(cmsRecommend.getTabId(),userName)){
            return -1;
        }
        if(cmsRecommend.getSeq() == seq.intValue()){
            return 1;
        }

        Map para = new HashMap<>();
        Integer count = 1 ;
        if(cmsRecommend.getSeq() < seq.intValue()){
            count = -1 ;
            para.put("start",cmsRecommend.getSeq()) ;
            para.put("end",seq) ;
        }else{
            para.put("start",seq) ;
            para.put("end",cmsRecommend.getSeq()) ;
        }
        para.put("count",count) ;
        para.put("tabId",cmsRecommend.getTabId()) ;
        //修改其他记录的顺序
        cmsRecommendMapper.updateSeq(para);

        //修改当前记录的顺序
        cmsRecommend.setSeq(seq.longValue());
        cmsRecommendMapper.updateCmsRecommend(cmsRecommend);
        //删除缓存
        removeRedis(cmsRecommend.getTabId());
        return 1 ;
    }

    /**
     * 根据apkId删除推荐配置
     *
     * @param apkId
     */
    @Override
    public int deleteCmsRecommendByApkId(Long apkId) {
        CmsRecommend recommend = new CmsRecommend();
        recommend.setClickId(apkId);
        List<CmsRecommend> recommendList = cmsRecommendMapper.selectCmsRecommendList(recommend);
        int a = cmsRecommendMapper.deleteCmsRecommendByApkId(apkId);
        // 删除缓存
        recommendList.forEach(re -> removeRedis(re.getTabId()));
        return a;
    }

    /**
     * 判断当前数据是不是当前用户的
     * @param tabId
     * @param userName
     * @return
     */
    private boolean judgeUserBanner(Long tabId,String userName){
        CmsTab cmsTab = cmsTabMapper.selectCmsTabById(tabId);
        return cmsTab.getCreateBy().equals(userName);
    }

    /**
     * 删除缓存
     * @param tabId
     */
    private void removeRedis(Long tabId){
        String recommendKey = RedisKey.getRecommendKey(tabId);
        redisCache.deleteObject(recommendKey);
    }
}
