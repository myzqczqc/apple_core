package com.ruoyi.core.service.impl;

import java.util.Comparator;
import java.util.List;

import com.ruoyi.core.domain.CmsCoreVersion;
import com.ruoyi.core.mapper.CmsCoreVersionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.core.mapper.CmsCoreMapper;
import com.ruoyi.core.domain.CmsCore;
import com.ruoyi.core.service.ICmsCoreService;

/**
 * 核心应用信息Service业务层处理
 * 
 * @author zqc
 * @date 2023-11-23
 */
@Service
public class CmsCoreServiceImpl implements ICmsCoreService 
{
    @Autowired
    private CmsCoreMapper cmsCoreMapper;
    @Autowired
    private CmsCoreVersionMapper cmsCoreVersionMapper;

    /**
     * 查询核心应用信息
     * 
     * @param id 核心应用信息主键
     * @return 核心应用信息
     */
    @Override
    public CmsCore selectCmsCoreById(Long id)
    {
        return cmsCoreMapper.selectCmsCoreById(id);
    }

    /**
     * 查询核心应用信息列表
     * 
     * @param cmsCore 核心应用信息
     * @return 核心应用信息
     */
    @Override
    public List<CmsCore> selectCmsCoreList(CmsCore cmsCore)
    {
        List<CmsCore> cmsCores = cmsCoreMapper.selectCmsCoreList(cmsCore);
        List<CmsCoreVersion> cmsCoreVersions = cmsCoreVersionMapper.selectCmsCoreVersionList(null);
        for (int i = 0; i < cmsCores.size(); i++) {
            CmsCore temp = cmsCores.get(i);
            CmsCoreVersion cmsCoreVersion = cmsCoreVersions.stream().filter(a -> a.getTypeId() == temp.getId().longValue())
                    .max(Comparator.comparing(CmsCoreVersion::getVersionCode)).orElse(null);
            temp.setCmsCoreVersion(cmsCoreVersion);
        }
        return cmsCores ;
    }

    /**
     * 新增核心应用信息
     * 
     * @param cmsCore 核心应用信息
     * @return 结果
     */
    @Override
    public int insertCmsCore(CmsCore cmsCore)
    {
        return cmsCoreMapper.insertCmsCore(cmsCore);
    }

    /**
     * 修改核心应用信息
     * 
     * @param cmsCore 核心应用信息
     * @return 结果
     */
    @Override
    public int updateCmsCore(CmsCore cmsCore)
    {
        return cmsCoreMapper.updateCmsCore(cmsCore);
    }

    /**
     * 批量删除核心应用信息
     * 
     * @param ids 需要删除的核心应用信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsCoreByIds(Long[] ids)
    {
        return cmsCoreMapper.deleteCmsCoreByIds(ids);
    }

    /**
     * 删除核心应用信息信息
     * 
     * @param id 核心应用信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsCoreById(Long id)
    {
        return cmsCoreMapper.deleteCmsCoreById(id);
    }
}
