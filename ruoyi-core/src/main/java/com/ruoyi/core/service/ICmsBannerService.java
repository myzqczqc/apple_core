package com.ruoyi.core.service;

import java.io.IOException;
import java.util.List;
import com.ruoyi.core.domain.CmsBanner;

/**
 * tab的banner信息Service接口
 * 
 * @author zqc
 * @date 2023-11-08
 */
public interface ICmsBannerService 
{
    /**
     * 查询tab的banner信息
     * 
     * @param id tab的banner信息主键
     * @return tab的banner信息
     */
    public CmsBanner selectCmsBannerById(Long id);

    /**
     * 查询tab的banner信息列表
     * 
     * @param cmsBanner tab的banner信息
     * @return tab的banner信息集合
     */
    public List<CmsBanner> selectCmsBannerList(CmsBanner cmsBanner);

    /**
     * 新增tab的banner信息
     * 
     * @param cmsBanner tab的banner信息
     * @return 结果
     */
    public int insertCmsBanner(CmsBanner cmsBanner) throws IOException;

    /**
     * 修改tab的banner信息
     * 
     * @param cmsBanner tab的banner信息
     * @return 结果
     */
    public int updateCmsBanner(CmsBanner cmsBanner) throws IOException;

    /**
     * 批量删除tab的banner信息
     * 
     * @param ids 需要删除的tab的banner信息主键集合
     * @return 结果
     */
    public int deleteCmsBannerByIds(Long[] ids);

    /**
     * 删除tab的banner信息信息
     * 
     * @param id tab的banner信息主键
     * @return 结果
     */
    public int deleteCmsBannerById(Long id,String userName);

    /**
     * 修改指定的数据到 顺序号seq上
     * @param id
     * @param seq
     * @return
     */
    public int updateSeq(Long id,Integer seq,String userName);

    /**
     * 根据apkId删除banner配置
     * @param apkId
     * @return
     */
    int deleteCmsBannerByApkId(Long apkId);
}
