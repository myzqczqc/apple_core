package com.ruoyi.core.service.impl;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.ruoyi.core.mapper.CmsChannelMapper;
import com.ruoyi.core.domain.CmsChannel;
import com.ruoyi.core.service.ICmsChannelService;

/**
 * 渠道商管理Service业务层处理
 *
 * @author zqc
 * @date 2023-11-08
 */
@Service
public class CmsChannelServiceImpl implements ICmsChannelService
{
    @Autowired
    private CmsChannelMapper cmsChannelMapper;

    /**
     * 查询渠道商管理
     *
     * @param id 渠道商管理主键
     * @return 渠道商管理
     */
    @Override
    public CmsChannel selectCmsChannelById(Long id)
    {
        return cmsChannelMapper.selectCmsChannelById(id);
    }
    @Override
    public CmsChannel selectLastChannelSn(CmsChannel cmsChannel)
    {
        return cmsChannelMapper.selectLastChannelSn(cmsChannel);
    }

    /**
     * 查询渠道商管理列表
     *
     * @param cmsChannel 渠道商管理
     * @return 渠道商管理
     */
    @Override
    public List<CmsChannel> selectCmsChannelList(CmsChannel cmsChannel)
    {
        return cmsChannelMapper.selectCmsChannelList(cmsChannel);
    }

    /**
     * 新增渠道商管理
     *
     * @param cmsChannel 渠道商管理
     * @return 结果
     */
    @Override
    public int insertCmsChannel(CmsChannel cmsChannel)
    {
        cmsChannel.setCreateTime(DateUtils.getNowDate());
        return cmsChannelMapper.insertCmsChannel(cmsChannel);
    }

    /**
     * 修改渠道商管理
     *
     * @param cmsChannel 渠道商管理
     * @return 结果
     */
    @Override
    public int updateCmsChannel(CmsChannel cmsChannel)
    {
        cmsChannel.setUpdateTime(DateUtils.getNowDate());
        return cmsChannelMapper.updateCmsChannel(cmsChannel);
    }

    /**
     * 批量删除渠道商管理
     *
     * @param ids 需要删除的渠道商管理主键
     * @return 结果
     */
    @Override
    public int deleteCmsChannelByIds(Long[] ids)
    {
        return cmsChannelMapper.deleteCmsChannelByIds(ids);
    }

    /**
     * 删除渠道商管理信息
     *
     * @param id 渠道商管理主键
     * @return 结果
     */
    @Override
    public int deleteCmsChannelById(Long id)
    {
        return cmsChannelMapper.deleteCmsChannelById(id);
    }

    /**
     * 根据渠道号查询渠道
     *
     * @param channelSn
     * @return
     */
    @Override
    public CmsChannel selectCmsChannelBySn(String channelSn) {
        return cmsChannelMapper.selectCmsChannelBySn(channelSn);
    }
}
