package com.ruoyi.core.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.core.domain.*;
import com.ruoyi.core.enums.ClickTypeEnums;
import com.ruoyi.core.mapper.*;
import com.ruoyi.core.service.IUploadFile2YunService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.core.service.ICmsBannerService;

/**
 * tab的banner信息Service业务层处理
 * 
 * @author zqc
 * @date 2023-11-08
 */
@Service
public class CmsBannerServiceImpl implements ICmsBannerService 
{
    @Autowired
    private CmsBannerMapper cmsBannerMapper;
    @Autowired
    private CmsBroadcastInfoMapper cmsBroadcastInfoMapper;
    @Autowired
    private CmsMoviesMapper cmsMoviesMapper;
    @Autowired
    private CmsApkInfoMapper cmsApkInfoMapper;
    @Autowired
    private CmsTabMapper cmsTabMapper;

    @Autowired
    private RedisCache redisCache;
    @Autowired
    private IUploadFile2YunService uploadFile2YunService;
    private static final String PATH = "banner";
    /**
     * 查询tab的banner信息
     * 
     * @param id tab的banner信息主键
     * @return tab的banner信息
     */
    @Override
    public CmsBanner selectCmsBannerById(Long id)
    {
        CmsBanner cmsBanner = cmsBannerMapper.selectCmsBannerById(id);

        if(cmsBanner.getClickType().equals(ClickTypeEnums.BROADCAST.getName())){
            CmsBroadcastInfo cmsBroadcastInfo = cmsBroadcastInfoMapper.selectCmsBroadcastInfoById(cmsBanner.getClickId());
            cmsBanner.setClickInfo(cmsBroadcastInfo);
        }else if(cmsBanner.getClickType().equals(ClickTypeEnums.MOVIES.getName())){
            CmsMovies cmsMovies = cmsMoviesMapper.selectCmsMoviesById(cmsBanner.getClickId());
            cmsBanner.setClickInfo(cmsMovies);
        }else if(cmsBanner.getClickType().equals(ClickTypeEnums.APK.getName())){
            CmsApkInfo cmsApkInfo = cmsApkInfoMapper.selectCmsApkInfoById(cmsBanner.getClickId());
            cmsBanner.setClickInfo(cmsApkInfo);
        }
        return cmsBanner;
    }

    /**
     * 查询tab的banner信息列表
     * 
     * @param cmsBanner tab的banner信息
     * @return tab的banner信息
     */
    @Override
    public List<CmsBanner> selectCmsBannerList(CmsBanner cmsBanner)
    {
        List<CmsBanner> cmsBanners = cmsBannerMapper.selectCmsBannerList(cmsBanner);
        if( ! cmsBanners.isEmpty()){
            //直播流的id
            Long[] ids = cmsBanners.stream().filter(a -> a.getClickType().equals(ClickTypeEnums.BROADCAST.getName())).map(a -> a.getClickId()).toArray(Long[]::new);
            if(ids.length>0){
                //直播流的数据信息
                List<CmsBroadcastInfo> cmsBroadcastInfos = cmsBroadcastInfoMapper.selectCmsBroadcastInfoByIds(ids);
                //直播流的数据传入到返回信息中
                for (CmsBanner temp: cmsBanners  ) {
                    if(temp.getClickType().equals(ClickTypeEnums.BROADCAST.getName())){
                        CmsBroadcastInfo cmsBroadcastInfo = cmsBroadcastInfos.stream().filter(a -> a.getId() == temp.getClickId().longValue()).findFirst().orElse(null);
                        temp.setClickInfo(cmsBroadcastInfo);
                    }
                }
            }
            //影视剧
            Long[] moviesId = cmsBanners.stream().filter(a -> a.getClickType().equals(ClickTypeEnums.MOVIES.getName())).map(a -> a.getClickId()).toArray(Long[]::new);
            if(moviesId.length>0){
                List<CmsMovies> cmsMovies = cmsMoviesMapper.selectCmsMoviesByIds(moviesId);
                for (CmsBanner temp: cmsBanners  ) {
                    if(temp.getClickType().equals(ClickTypeEnums.MOVIES.getName())){
                        CmsMovies clickInfo = cmsMovies.stream().filter(a ->a.getId() == temp.getClickId().longValue()).findFirst().orElse(null);
                        temp.setClickInfo(clickInfo);
                    }
                }
            }
            //apk
            Long[] apkId = cmsBanners.stream().filter(a -> a.getClickType().equals(ClickTypeEnums.APK.getName())).map(a -> a.getClickId()).toArray(Long[]::new);
            if(apkId.length>0){
                List<CmsApkInfo> cmsApks = cmsApkInfoMapper.selectCmsApkInfoByIds(apkId);
                for (CmsBanner temp: cmsBanners  ) {
                    if(temp.getClickType().equals(ClickTypeEnums.APK.getName())){
                        CmsApkInfo cmsApkInfo = cmsApks.stream().filter(a -> a.getId() == temp.getClickId().longValue()).findFirst().orElse(null);
                        temp.setClickInfo(cmsApkInfo);
                    }
                }

            }
        }
        return cmsBanners;
    }

    /**
     * 新增tab的banner信息
     * 
     * @param cmsBanner tab的banner信息
     * @return 结果
     */
    @Override
    public int insertCmsBanner(CmsBanner cmsBanner) throws IOException {
        if( ! judgeUserBanner(cmsBanner.getTabId(),cmsBanner.getCreateBy())){
            return -1;
        }
        // 校验是否已经添加了这个banner信息
        CmsBanner para = new CmsBanner();
        para.setTabId(cmsBanner.getTabId());
        para.setClickId(cmsBanner.getClickId());
        para.setClickType(cmsBanner.getClickType());
        para.setCreateBy(cmsBanner.getCreateBy());
        List<CmsBanner> cmsBanners = cmsBannerMapper.selectCmsBannerList(para);
        if (!cmsBanners.isEmpty()) {
            return -2;
        }

        //获取顺序号
//        CmsBanner para = new CmsBanner();
//        para.setTabId(cmsBanner.getTabId());
//        List<CmsBanner> cmsBanners = cmsBannerMapper.selectCmsBannerList(para);
        //顺序号
        Long maxSeq = cmsBannerMapper.selectBannerMaxSeq(cmsBanner.getTabId());
        cmsBanner.setSeq(Objects.isNull(maxSeq) ? 1 : maxSeq + 1);
        cmsBannerMapper.insertCmsBanner(cmsBanner);

        //上传图片信息
        String imageUrl= uploadFile2YunService.uploadFile2Oss(String.valueOf(cmsBanner.getId()), cmsBanner.getImage(), PATH);
        if(StringUtils.isNotEmpty(imageUrl)){
            cmsBanner.setImage(imageUrl);
        }
//        //上传logo信息
//        String iconUrl= uploadFile2YunService.uploadFile2Oss(getIconId(cmsBanner.getId()), cmsBanner.getIcon(), PATH);
//        if(StringUtils.isNotEmpty(iconUrl)){
//            cmsBanner.setIcon(iconUrl);
//        }

        cmsBannerMapper.updateCmsBanner(cmsBanner);
        return 1;
    }

    /**
     * 修改tab的banner信息
     * 
     * @param cmsBanner tab的banner信息
     * @return 结果
     */
    @Override
    public int updateCmsBanner(CmsBanner cmsBanner) throws IOException {

        if( ! judgeUserBanner(cmsBanner.getTabId(),cmsBanner.getUpdateBy())){
            return -1;
        }

        String imageUrl = uploadFile2YunService.uploadFile2Oss(String.valueOf(cmsBanner.getId()), cmsBanner.getImage(), PATH);
        if(StringUtils.isNotEmpty(imageUrl)){
            cmsBanner.setImage(imageUrl);
        }
//        String iconUrl = uploadFile2YunService.uploadFile2Oss(getIconId(cmsBanner.getId()), cmsBanner.getImage(), PATH);
//        if(StringUtils.isNotEmpty(iconUrl)){
//            cmsBanner.setIcon(iconUrl);
//        }
        //删除缓存
        removeReids(cmsBanner.getTabId());
        return cmsBannerMapper.updateCmsBanner(cmsBanner);
    }

    /**
     * 批量删除tab的banner信息
     * 
     * @param ids 需要删除的tab的banner信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsBannerByIds(Long[] ids)
    {
        return cmsBannerMapper.deleteCmsBannerByIds(ids);
    }

    /**
     * 删除tab的banner信息信息
     * 
     * @param id tab的banner信息主键
     * @return 结果
     */
    @Override
    public int deleteCmsBannerById(Long id,String userName)
    {

        CmsBanner cmsBanner = cmsBannerMapper.selectCmsBannerById(id);
        if( ! judgeUserBanner(cmsBanner.getTabId(),userName)){
            return -1;
        }
        //删除
        cmsBannerMapper.deleteCmsBannerById(id);
        //其他数据seq 需要修改
        cmsBannerMapper.deleteSeq(cmsBanner);
        //删除缓存
        removeReids(cmsBanner.getTabId());
        return 1;
    }
    /**
     * 修改指定的数据到 顺序号seq上
     * @param id
     * @param seq
     * @return
     */
    public int updateSeq(Long id,Integer seq,String userName) {
        CmsBanner cmsBanner = cmsBannerMapper.selectCmsBannerById(id);
        if( ! judgeUserBanner(cmsBanner.getTabId(),userName)){
            return -1;
        }
        if(cmsBanner.getSeq() == seq.intValue()){
            return 1;
        }


        Map para = new HashMap<>();
        Integer count = 1 ;
        if(cmsBanner.getSeq() < seq.intValue()){
            count = -1 ;
            para.put("start",cmsBanner.getSeq()) ;
            para.put("end",seq) ;
        }else{
            para.put("start",seq) ;
            para.put("end",cmsBanner.getSeq()) ;
        }
        para.put("count",count) ;
        para.put("tabId",cmsBanner.getTabId()) ;
        cmsBannerMapper.updateSeq(para);


        cmsBanner.setSeq(seq.longValue());
        cmsBannerMapper.updateCmsBanner(cmsBanner);

        //删除缓存
        removeReids(cmsBanner.getTabId());
        return 1;
    }

    /**
     * 根据apkId删除banner配置
     * @param apkId
     * @return
     */
    @Override
    public int deleteCmsBannerByApkId(Long apkId) {
        CmsBanner cmsBanner = new CmsBanner();
        cmsBanner.setClickId(apkId);
        List<CmsBanner> bannerList = cmsBannerMapper.selectCmsBannerList(cmsBanner);
        int a = cmsBannerMapper.deleteCmsBannerByApkId(apkId);
        // 删除缓存
        bannerList.forEach(banner -> removeReids(banner.getTabId()));
        return a;
    }

    /**
     * 判断当前数据是不是当前用户的
     * @param tabId
     * @param userName
     * @return
     */
    private boolean judgeUserBanner(Long tabId,String userName){
        CmsTab cmsTab = cmsTabMapper.selectCmsTabById(tabId);
        return cmsTab.getCreateBy().equals(userName);
    }

    /**
     * 删除缓存
     * @param tabId
     */
    private void removeReids(Long tabId){
        String bannerKey = RedisKey.getBannerKey(tabId);
        redisCache.deleteObject(bannerKey);
    }

}
