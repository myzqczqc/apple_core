package com.ruoyi.core.service;

import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * 文件上传到云
 */
public interface IUploadFile2YunService {

    /**
     * 上传文件到oss
     * @param id
     * @param url
     * @param path
     * @return
     * @throws IOException
     */
    public String uploadFile2Oss(String id,String url,String path) throws IOException;

}
