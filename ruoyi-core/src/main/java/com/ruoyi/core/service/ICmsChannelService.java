package com.ruoyi.core.service;

import java.util.List;
import com.ruoyi.core.domain.CmsChannel;

/**
 * 渠道商管理Service接口
 *
 * @author zqc
 * @date 2023-11-08
 */
public interface ICmsChannelService
{
    /**
     * 查询渠道商管理
     *
     * @param id 渠道商管理主键
     * @return 渠道商管理
     */
    public CmsChannel selectCmsChannelById(Long id);
    /**
     * 查询合作商下的最新渠道SN
     * @param:
     * @return:
     */
    public CmsChannel selectLastChannelSn(CmsChannel cmsChannel);

    /**
     * 查询渠道商管理列表
     *
     * @param cmsChannel 渠道商管理
     * @return 渠道商管理集合
     */
    public List<CmsChannel> selectCmsChannelList(CmsChannel cmsChannel);

    /**
     * 新增渠道商管理
     *
     * @param cmsChannel 渠道商管理
     * @return 结果
     */
    public int insertCmsChannel(CmsChannel cmsChannel);

    /**
     * 修改渠道商管理
     *
     * @param cmsChannel 渠道商管理
     * @return 结果
     */
    public int updateCmsChannel(CmsChannel cmsChannel);

    /**
     * 批量删除渠道商管理
     *
     * @param ids 需要删除的渠道商管理主键集合
     * @return 结果
     */
    public int deleteCmsChannelByIds(Long[] ids);

    /**
     * 删除渠道商管理信息
     *
     * @param id 渠道商管理主键
     * @return 结果
     */
    public int deleteCmsChannelById(Long id);

    /**
     * 根据渠道号查询渠道
     * @param channelSn
     * @return
     */
    CmsChannel selectCmsChannelBySn(String channelSn);
}
