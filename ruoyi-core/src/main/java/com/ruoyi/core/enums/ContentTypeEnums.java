package com.ruoyi.core.enums;

/**
 * 相对资源类型，
 * app点击对象的数据类型
 */
public enum ContentTypeEnums {
    SHARE("共享",1),
    PRIVATE("私有",0),
    SEARCH("可搜索",2),
    ;
    private String name;
    private Integer type;
    // 构造方法
    private ContentTypeEnums(String name,Integer type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
