package com.ruoyi.core.enums;

/**
 * 相对资源类型，
 * app点击对象的数据类型
 */
public enum ClickTypeEnums {
    BROADCAST("broadcast"),
    MOVIES("movies"),
    APK("apk"),
    ;
    private String name;
    // 构造方法
    private ClickTypeEnums(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
