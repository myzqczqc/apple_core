package com.ruoyi.core.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.core.domain.CmsBanner;

/**
 * tab的banner信息Mapper接口
 * 
 * @author zqc
 * @date 2023-11-08
 */
public interface CmsBannerMapper 
{
    /**
     * 查询tab的banner信息
     * 
     * @param id tab的banner信息主键
     * @return tab的banner信息
     */
    public CmsBanner selectCmsBannerById(Long id);

    /**
     * 查询tab的banner信息列表
     * 
     * @param cmsBanner tab的banner信息
     * @return tab的banner信息集合
     */
    public List<CmsBanner> selectCmsBannerList(CmsBanner cmsBanner);

    /**
     * 新增tab的banner信息
     * 
     * @param cmsBanner tab的banner信息
     * @return 结果
     */
    public int insertCmsBanner(CmsBanner cmsBanner);

    /**
     * 修改tab的banner信息
     * 
     * @param cmsBanner tab的banner信息
     * @return 结果
     */
    public int updateCmsBanner(CmsBanner cmsBanner);

    /**
     * 删除tab的banner信息
     * 
     * @param id tab的banner信息主键
     * @return 结果
     */
    public int deleteCmsBannerById(Long id);

    /**
     * 批量删除tab的banner信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCmsBannerByIds(Long[] ids);


    /**
     * 删除数据的时候修改顺序号
     * @param cmsBanner
     * @return
     */
    public int deleteSeq(CmsBanner cmsBanner);
    /**
     * 修改顺序号到
     * @param map
     * @return
     */
    public int updateSeq(Map map);

    /**
     * 查询最大序列号
     * @param tabId
     * @return
     */
    Long selectBannerMaxSeq(Long tabId);

    /**
     * 根据apkId删除banner配置
     * @param apkId
     * @return
     */
    int deleteCmsBannerByApkId(Long apkId);
}
