package com.ruoyi.core.mapper;

import java.util.List;
import com.ruoyi.core.domain.CmsMovies;

/**
 * 影视数据信息Mapper接口
 * 
 * @author zqc
 * @date 2023-11-06
 */
public interface CmsMoviesMapper 
{
    /**
     * 查询影视数据信息
     * 
     * @param id 影视数据信息主键
     * @return 影视数据信息
     */
    public CmsMovies selectCmsMoviesById(Long id);
    /**
     * 查询影视数据信息 多个id
     *
     * @param ids 影视数据信息主键
     * @return 影视数据信息
     */
    public List<CmsMovies>  selectCmsMoviesByIds(Long[] ids);
    /**
     * 查询影视数据信息列表
     * 
     * @param cmsMovies 影视数据信息
     * @return 影视数据信息集合
     */
    public List<CmsMovies> selectCmsMoviesList(CmsMovies cmsMovies);

    /**
     * 新增影视数据信息
     * 
     * @param cmsMovies 影视数据信息
     * @return 结果
     */
    public int insertCmsMovies(CmsMovies cmsMovies);

    /**
     * 修改影视数据信息
     * 
     * @param cmsMovies 影视数据信息
     * @return 结果
     */
    public int updateCmsMovies(CmsMovies cmsMovies);

    /**
     * 删除影视数据信息
     * 
     * @param id 影视数据信息主键
     * @return 结果
     */
    public int deleteCmsMoviesById(Long id);

    /**
     * 批量删除影视数据信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCmsMoviesByIds(Long[] ids);
}
