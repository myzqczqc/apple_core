package com.ruoyi.core.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.core.domain.CmsApkInfo;
import com.ruoyi.core.domain.CmsMovies;

/**
 * apk信息Mapper接口
 * 
 * @author zqc
 * @date 2023-11-06
 */
public interface CmsApkInfoMapper 
{
    /**
     * 查询apk信息
     * 
     * @param id apk信息主键
     * @return apk信息
     */
    public CmsApkInfo selectCmsApkInfoById(Long id);
    /**
     * 查询影视数据信息 多个id
     *
     * @param ids 影视数据信息主键
     * @return 影视数据信息
     */
    public List<CmsApkInfo>  selectCmsApkInfoByIds(Long[] ids);
    /**
     * 根据 packageName获取apk
     * @param packageName
     * @return
     */
    public CmsApkInfo selectCmsApkInfoByPackageName(String packageName);

    /**
     * 查询apk信息列表
     * 
     * @param cmsApkInfo apk信息
     * @return apk信息集合
     */
    public List<CmsApkInfo> selectCmsApkInfoList(CmsApkInfo cmsApkInfo);

    /**
     * 接口根据子分类查询app
     * @param map
     * @return
     */
    public List<CmsApkInfo> selectCmsApkInfoByApi(Map map);

    /**
     * 新增apk信息
     * 
     * @param cmsApkInfo apk信息
     * @return 结果
     */
    public int insertCmsApkInfo(CmsApkInfo cmsApkInfo);

    /**
     * 修改apk信息
     * 
     * @param cmsApkInfo apk信息
     * @return 结果
     */
    public int updateCmsApkInfo(CmsApkInfo cmsApkInfo);

    /**
     * 删除apk信息
     * 
     * @param id apk信息主键
     * @return 结果
     */
    public int deleteCmsApkInfoById(Long id);

    /**
     * 批量删除apk信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCmsApkInfoByIds(Long[] ids);
}
