package com.ruoyi.core.mapper;

import java.util.List;
import com.ruoyi.core.domain.CmsLanguage;

/**
 * 语言字典信息Mapper接口
 * 
 * @author zqc
 * @date 2023-11-21
 */
public interface CmsLanguageMapper 
{
    /**
     * 查询语言字典信息
     * 
     * @param id 语言字典信息主键
     * @return 语言字典信息
     */
    public CmsLanguage selectCmsLanguageById(Long id);

    /**
     * 查询语言字典信息列表
     * 
     * @param cmsLanguage 语言字典信息
     * @return 语言字典信息集合
     */
    public List<CmsLanguage> selectCmsLanguageList(CmsLanguage cmsLanguage);

    /**
     * 新增语言字典信息
     * 
     * @param cmsLanguage 语言字典信息
     * @return 结果
     */
    public int insertCmsLanguage(CmsLanguage cmsLanguage);

    /**
     * 修改语言字典信息
     * 
     * @param cmsLanguage 语言字典信息
     * @return 结果
     */
    public int updateCmsLanguage(CmsLanguage cmsLanguage);

    /**
     * 删除语言字典信息
     * 
     * @param id 语言字典信息主键
     * @return 结果
     */
    public int deleteCmsLanguageById(Long id);

    /**
     * 批量删除语言字典信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCmsLanguageByIds(Long[] ids);
}
