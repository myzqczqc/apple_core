package com.ruoyi.core.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.core.domain.CmsAppType;

/**
 * app子分类Mapper接口
 * 
 * @author zqc
 * @date 2023-11-25
 */
public interface CmsAppTypeMapper 
{
    /**
     * 查询app子分类
     * 
     * @param id app子分类主键
     * @return app子分类
     */
    public CmsAppType selectCmsAppTypeById(Long id);

    /**
     * 查询app子分类列表
     * 
     * @param cmsAppType app子分类
     * @return app子分类集合
     */
    public List<CmsAppType> selectCmsAppTypeList(CmsAppType cmsAppType);

    /**
     * 新增app子分类
     * 
     * @param cmsAppType app子分类
     * @return 结果
     */
    public int insertCmsAppType(CmsAppType cmsAppType);

    /**
     * 修改app子分类
     * 
     * @param cmsAppType app子分类
     * @return 结果
     */
    public int updateCmsAppType(CmsAppType cmsAppType);

    /**
     * 删除app子分类
     * 
     * @param id app子分类主键
     * @return 结果
     */
    public int deleteCmsAppTypeById(Long id);

    /**
     * 批量删除app子分类
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCmsAppTypeByIds(Long[] ids);

    /**
     * 更新子分类排序
     * @param para
     */
    int updateAppTypeSeq(Map<String, Object> para);

    Integer selectAppTypeMaxSeq(Integer ap);
}
