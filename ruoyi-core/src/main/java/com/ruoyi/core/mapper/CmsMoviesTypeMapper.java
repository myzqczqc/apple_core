package com.ruoyi.core.mapper;

import java.util.List;
import com.ruoyi.core.domain.CmsMoviesType;

/**
 * 影视类型，关系Mapper接口
 * 
 * @author zqc
 * @date 2023-11-07
 */
public interface CmsMoviesTypeMapper 
{

    /**
     * 查询影视类型，关系列表
     * 
     * @param cmsMoviesType 影视类型，关系
     * @return 影视类型，关系集合
     */
    public List<CmsMoviesType> selectCmsMoviesTypeList(CmsMoviesType cmsMoviesType);

    /**
     * 根据moviesId查询
     * @param moviesId
     * @return
     */
    public List<CmsMoviesType> selectCmsMoviesTypeByMoviesId(Long moviesId);


    public List<CmsMoviesType> selectCmsMoviesTypeByMoviesIds(Long[] moviesId);
    /**
     * 新增影视类型，关系
     * 
     * @param list 影视类型，关系
     * @return 结果
     */
    public int insertCmsMoviesType(List<CmsMoviesType> list);
    /**
     * 删除影视类型，关系
     * 
     * @param id 影视类型，关系主键
     * @return 结果
     */
    public int deleteCmsMoviesTypeByMoviesId(Long id);
}
