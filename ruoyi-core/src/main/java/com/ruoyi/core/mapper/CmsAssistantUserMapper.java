package com.ruoyi.core.mapper;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.core.domain.CmsAssistantUser;
import org.apache.ibatis.annotations.Param;

/**
 * 手机助手用户Mapper接口
 *
 * @author zqc
 * @date 2023-11-21
 */
public interface CmsAssistantUserMapper
{
    /**
     * 查询手机助手用户
     *
     * @param id 手机助手用户主键
     * @return 手机助手用户
     */
    public CmsAssistantUser selectCmsAssistantUserById(Long id);

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    public CmsAssistantUser selectUserByUserName(String userName);
    /**
     * 检查用户名是否唯一
     * @param: user 手机助手对象
     * @return: 手机助手用户
     */
    public CmsAssistantUser checkUserNameUnique(CmsAssistantUser user);

    /**
     * 查询手机助手用户列表
     *
     * @param cmsAssistantUser 手机助手用户
     * @return 手机助手用户集合
     */
    public List<CmsAssistantUser> selectCmsAssistantUserList(CmsAssistantUser cmsAssistantUser);

    /**
     * 新增手机助手用户
     *
     * @param cmsAssistantUser 手机助手用户
     * @return 结果
     */
    public int insertCmsAssistantUser(CmsAssistantUser cmsAssistantUser);

    /**
     * 修改手机助手用户
     *
     * @param cmsAssistantUser 手机助手用户
     * @return 结果
     */
    public int updateCmsAssistantUser(CmsAssistantUser cmsAssistantUser);

    /**
     *  解绑手机号
     * @param cmsAssistantUser
     * @return
     */
    public int unbindCmsAssistantPhone(CmsAssistantUser cmsAssistantUser);
    /**
     * 修改用户密码
     * @param:  userName 用户名
     * @return: password 新密码
     */
    public int updateUserPwd(@Param("userName") String userName, @Param("password") String password);

    public int updateUserAvatar(@Param("userName") String userName, @Param("avatar") String avatar);

    /**
     * 删除手机助手用户
     *
     * @param id 手机助手用户主键
     * @return 结果
     */
    public int deleteCmsAssistantUserById(Long id);

    /**
     * 批量删除手机助手用户
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCmsAssistantUserByIds(Long[] ids);

    /**
     * 根据手机号查询用户
     * @param phone 手机号
     * @return 手机助手用户
     */
    CmsAssistantUser selectCmsAssistantUserByPhone(String phone);
}
