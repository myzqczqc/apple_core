package com.ruoyi.core.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.core.domain.CmsRecommend;

/**
 * tab的recommend信息Mapper接口
 * 
 * @author zqc
 * @date 2023-11-08
 */
public interface CmsRecommendMapper 
{
    /**
     * 查询tab的recommend信息
     * 
     * @param id tab的recommend信息主键
     * @return tab的recommend信息
     */
    public CmsRecommend selectCmsRecommendById(Long id);

    /**
     * 查询tab的recommend信息列表
     * 
     * @param cmsRecommend tab的recommend信息
     * @return tab的recommend信息集合
     */
    public List<CmsRecommend> selectCmsRecommendList(CmsRecommend cmsRecommend);

    /**
     * 新增tab的recommend信息
     * 
     * @param cmsRecommend tab的recommend信息
     * @return 结果
     */
    public int insertCmsRecommend(CmsRecommend cmsRecommend);

    /**
     * 修改tab的recommend信息
     * 
     * @param cmsRecommend tab的recommend信息
     * @return 结果
     */
    public int updateCmsRecommend(CmsRecommend cmsRecommend);

    /**
     * 删除tab的recommend信息
     * 
     * @param id tab的recommend信息主键
     * @return 结果
     */
    public int deleteCmsRecommendById(Long id);

    /**
     * 批量删除tab的recommend信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCmsRecommendByIds(Long[] ids);

    /**
     * 删除数据，修改顺序号
     * @param cmsRecommend
     * @return
     */
    public int deleteSeq(CmsRecommend cmsRecommend);
    /**
     * 修改顺序号到
     * @param map
     * @return
     */
    public int updateSeq(Map map);

    /**
     * 根据apkId删除recommend推荐配置
     * @param apkId
     * @return
     */
    int deleteCmsRecommendByApkId(Long apkId);
}
