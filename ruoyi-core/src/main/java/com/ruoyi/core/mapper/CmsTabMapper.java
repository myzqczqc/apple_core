package com.ruoyi.core.mapper;

import java.util.List;
import com.ruoyi.core.domain.CmsTab;

/**
 * tab信息Mapper接口
 * 
 * @author zqc
 * @date 2023-11-08
 */
public interface CmsTabMapper 
{
    /**
     * 查询tab信息
     * 
     * @param id tab信息主键
     * @return tab信息
     */
    public CmsTab selectCmsTabById(Long id);

    /**
     * 查询tab信息列表
     * 
     * @param cmsTab tab信息
     * @return tab信息集合
     */
    public List<CmsTab> selectCmsTabList(CmsTab cmsTab);

    /**
     * 根据channelId获取数据
     * @param channelId
     * @return
     */
    public List<CmsTab> selectCmsTabByChannelId(String channelId);

    /**
     * 新增tab信息
     * 
     * @param cmsTab tab信息
     * @return 结果
     */
    public int insertCmsTab(CmsTab cmsTab);

    /**
     * 修改tab信息
     * 
     * @param cmsTab tab信息
     * @return 结果
     */
    public int updateCmsTab(CmsTab cmsTab);

    /**
     * 删除tab信息
     * 
     * @param id tab信息主键
     * @return 结果
     */
    public int deleteCmsTabById(Long id);

    /**
     * 批量删除tab信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCmsTabByIds(Long[] ids);
}
