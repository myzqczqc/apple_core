package com.ruoyi.core.mapper;

import java.util.List;
import com.ruoyi.core.domain.CmsIssueFeedback;

/**
 * 手机助手问题反馈Mapper接口
 * 
 * @author zqc
 * @date 2023-11-27
 */
public interface CmsIssueFeedbackMapper 
{
    /**
     * 查询手机助手问题反馈
     * 
     * @param id 手机助手问题反馈主键
     * @return 手机助手问题反馈
     */
    public CmsIssueFeedback selectCmsIssueFeedbackById(Long id);

    /**
     * 查询手机助手问题反馈列表
     * 
     * @param cmsIssueFeedback 手机助手问题反馈
     * @return 手机助手问题反馈集合
     */
    public List<CmsIssueFeedback> selectCmsIssueFeedbackList(CmsIssueFeedback cmsIssueFeedback);

    /**
     * 新增手机助手问题反馈
     * 
     * @param cmsIssueFeedback 手机助手问题反馈
     * @return 结果
     */
    public int insertCmsIssueFeedback(CmsIssueFeedback cmsIssueFeedback);

    /**
     * 修改手机助手问题反馈
     * 
     * @param cmsIssueFeedback 手机助手问题反馈
     * @return 结果
     */
    public int updateCmsIssueFeedback(CmsIssueFeedback cmsIssueFeedback);

    /**
     * 删除手机助手问题反馈
     * 
     * @param id 手机助手问题反馈主键
     * @return 结果
     */
    public int deleteCmsIssueFeedbackById(Long id);

    /**
     * 批量删除手机助手问题反馈
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCmsIssueFeedbackByIds(Long[] ids);
}
