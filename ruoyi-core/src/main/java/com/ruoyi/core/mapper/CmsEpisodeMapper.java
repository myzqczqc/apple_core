package com.ruoyi.core.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.core.domain.CmsEpisode;

/**
 * 剧集信息Mapper接口
 * 
 * @author zqc
 * @date 2023-11-06
 */
public interface CmsEpisodeMapper 
{
    /**
     * 查询剧集信息
     * 
     * @param id 剧集信息主键
     * @return 剧集信息
     */
    public CmsEpisode selectCmsEpisodeById(Long id);

    /**
     * 查询剧集信息列表
     * 
     * @param cmsEpisode 剧集信息
     * @return 剧集信息集合
     */
    public List<CmsEpisode> selectCmsEpisodeList(CmsEpisode cmsEpisode);

    /**
     * 新增剧集信息
     * 
     * @param cmsEpisode 剧集信息
     * @return 结果
     */
    public int insertCmsEpisode(CmsEpisode cmsEpisode);

    /**
     * 修改剧集信息
     * 
     * @param cmsEpisode 剧集信息
     * @return 结果
     */
    public int updateCmsEpisode(CmsEpisode cmsEpisode);

    /**
     * 删除剧集信息
     * 
     * @param id 剧集信息主键
     * @return 结果
     */
    public int deleteCmsEpisodeById(Long id);

    /**
     * 批量删除剧集信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCmsEpisodeByIds(Long[] ids);

    /**
     * 删除顺序号
     * @param cmsEpisode
     * @return
     */
    public int deleteSeq(CmsEpisode cmsEpisode);

    /**
     * 修改顺序号
     * @param map
     * @return
     */
    public int updateSeq(Map map);
}
