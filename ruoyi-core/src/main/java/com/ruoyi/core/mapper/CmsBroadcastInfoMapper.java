package com.ruoyi.core.mapper;

import java.util.List;
import com.ruoyi.core.domain.CmsBroadcastInfo;

/**
 * 直播流信息Mapper接口
 * 
 * @author zqc
 * @date 2023-11-06
 */
public interface CmsBroadcastInfoMapper 
{
    /**
     * 查询直播流信息
     * 
     * @param id 直播流信息主键
     * @return 直播流信息
     */
    public CmsBroadcastInfo selectCmsBroadcastInfoById(Long id);
    /**
     * 查询直播流信息列表  根据id 查询多个
     * @param ids
     * @return
     */
    public List<CmsBroadcastInfo> selectCmsBroadcastInfoByIds(Long[] ids);

    /**
     * 查询直播流信息列表
     * 
     * @param cmsBroadcastInfo 直播流信息
     * @return 直播流信息集合
     */
    public List<CmsBroadcastInfo> selectCmsBroadcastInfoList(CmsBroadcastInfo cmsBroadcastInfo);

    /**
     * 新增直播流信息
     * 
     * @param cmsBroadcastInfo 直播流信息
     * @return 结果
     */
    public int insertCmsBroadcastInfo(CmsBroadcastInfo cmsBroadcastInfo);

    /**
     * 修改直播流信息
     * 
     * @param cmsBroadcastInfo 直播流信息
     * @return 结果
     */
    public int updateCmsBroadcastInfo(CmsBroadcastInfo cmsBroadcastInfo);

    /**
     * 删除直播流信息
     * 
     * @param id 直播流信息主键
     * @return 结果
     */
    public int deleteCmsBroadcastInfoById(Long id);

    /**
     * 批量删除直播流信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCmsBroadcastInfoByIds(Long[] ids);
}
