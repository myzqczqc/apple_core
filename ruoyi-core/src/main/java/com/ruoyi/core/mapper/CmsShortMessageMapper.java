package com.ruoyi.core.mapper;

import com.ruoyi.core.domain.CmsShortMessage;

public interface CmsShortMessageMapper {

    CmsShortMessage selectCmsShortMessageById(Integer id);
}
