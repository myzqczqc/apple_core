package com.ruoyi.core.mapper;

import java.util.List;
import com.ruoyi.core.domain.CmsChannel;

/**
 * 渠道商管理Mapper接口
 *
 * @author zqc
 * @date 2023-11-08
 */
public interface CmsChannelMapper
{
    /**
     * 查询渠道商管理
     *
     * @param id 渠道商管理主键
     * @return 渠道商管理
     */
    public CmsChannel selectCmsChannelById(Long id);
    /**
     * 查询当前合作商下的最新渠道SN
     * @param:
     * @return:
     */
    public CmsChannel selectLastChannelSn(CmsChannel cmsChannel);

    /**
     * 根据sn获取数据
     * @param channelSn
     * @return
     */
    public CmsChannel selectCmsChannelBySn(String channelSn);

    /**
     * 查询渠道商管理列表
     *
     * @param cmsChannel 渠道商管理
     * @return 渠道商管理集合
     */
    public List<CmsChannel> selectCmsChannelList(CmsChannel cmsChannel);

    /**
     * 新增渠道商管理
     *
     * @param cmsChannel 渠道商管理
     * @return 结果
     */
    public int insertCmsChannel(CmsChannel cmsChannel);

    /**
     * 修改渠道商管理
     *
     * @param cmsChannel 渠道商管理
     * @return 结果
     */
    public int updateCmsChannel(CmsChannel cmsChannel);

    /**
     * 删除渠道商管理
     *
     * @param id 渠道商管理主键
     * @return 结果
     */
    public int deleteCmsChannelById(Long id);

    /**
     * 批量删除渠道商管理
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCmsChannelByIds(Long[] ids);
}
