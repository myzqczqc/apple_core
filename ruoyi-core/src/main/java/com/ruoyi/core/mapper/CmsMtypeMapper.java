package com.ruoyi.core.mapper;

import java.util.List;
import com.ruoyi.core.domain.CmsMtype;

/**
 * 影视类型Mapper接口
 * 
 * @author zqc
 * @date 2023-11-06
 */
public interface CmsMtypeMapper 
{
    /**
     * 查询影视类型
     * 
     * @param id 影视类型主键
     * @return 影视类型
     */
    public CmsMtype selectCmsMtypeById(Long id);

    /**
     * 查询影视类型列表
     * 
     * @param cmsMtype 影视类型
     * @return 影视类型集合
     */
    public List<CmsMtype> selectCmsMtypeList(CmsMtype cmsMtype);

    /**
     * 新增影视类型
     * 
     * @param cmsMtype 影视类型
     * @return 结果
     */
    public int insertCmsMtype(CmsMtype cmsMtype);

    /**
     * 修改影视类型
     * 
     * @param cmsMtype 影视类型
     * @return 结果
     */
    public int updateCmsMtype(CmsMtype cmsMtype);

    /**
     * 删除影视类型
     * 
     * @param id 影视类型主键
     * @return 结果
     */
    public int deleteCmsMtypeById(Long id);

    /**
     * 批量删除影视类型
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCmsMtypeByIds(Long[] ids);
}
