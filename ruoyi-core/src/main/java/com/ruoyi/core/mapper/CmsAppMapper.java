package com.ruoyi.core.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.core.domain.CmsApp;

/**
 * app配置Mapper接口
 *
 * @author zqc
 * @date 2023-11-17
 */
public interface CmsAppMapper
{
    /**
     * 查询app配置
     *
     * @param id app配置主键
     * @return app配置
     */
    public CmsApp selectCmsAppById(Long id);

    /**
     * 查询app配置列表
     *
     * @param cmsApp app配置
     * @return app配置集合
     */
    public List<CmsApp> selectCmsAppList(CmsApp cmsApp);
    /**
     * 查询当前条件下最新的排序号
     * @param:
     * @return:
     */
    public CmsApp selectMaxSeq(CmsApp cmsApp);

    /**
     * 新增app配置
     *
     * @param cmsApp app配置
     * @return 结果
     */
    public int insertCmsApp(CmsApp cmsApp);

    /**
     * 修改app配置
     *
     * @param cmsApp app配置
     * @return 结果
     */
    public int updateCmsApp(CmsApp cmsApp);

    /**
     * 删除app配置
     *
     * @param id app配置主键
     * @return 结果
     */
    public int deleteCmsAppById(Long id);

    /**
     * 批量删除app配置
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCmsAppByIds(Long[] ids);

    /**
     * 根据apkId删除app配置
     * @param apkId
     * @return
     */
    int deleteCmsAppByApkId(Long apkId);

    /**
     * 删除一个app推荐
     * @param cmsApp
     * @return
     */
    public int deleteSeq(CmsApp cmsApp);

    List<CmsApp> selectCmsAppPage(Map<String, Object> map);
}
