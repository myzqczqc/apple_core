package com.ruoyi.core.mapper;

import java.util.List;
import com.ruoyi.core.domain.CmsCore;

/**
 * 核心应用信息Mapper接口
 * 
 * @author zqc
 * @date 2023-11-23
 */
public interface CmsCoreMapper 
{
    /**
     * 查询核心应用信息
     * 
     * @param id 核心应用信息主键
     * @return 核心应用信息
     */
    public CmsCore selectCmsCoreById(Long id);

    /**
     * 查询核心应用信息列表
     * 
     * @param cmsCore 核心应用信息
     * @return 核心应用信息集合
     */
    public List<CmsCore> selectCmsCoreList(CmsCore cmsCore);

    /**
     * 新增核心应用信息
     * 
     * @param cmsCore 核心应用信息
     * @return 结果
     */
    public int insertCmsCore(CmsCore cmsCore);

    /**
     * 修改核心应用信息
     * 
     * @param cmsCore 核心应用信息
     * @return 结果
     */
    public int updateCmsCore(CmsCore cmsCore);

    /**
     * 删除核心应用信息
     * 
     * @param id 核心应用信息主键
     * @return 结果
     */
    public int deleteCmsCoreById(Long id);

    /**
     * 批量删除核心应用信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCmsCoreByIds(Long[] ids);
}
