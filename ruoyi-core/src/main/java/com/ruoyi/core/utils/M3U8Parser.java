package com.ruoyi.core.utils;

public class M3U8Parser {

    public static void main(String[] args) {
//        String m3u8Content = "#EXTINF:-1 tvg-id=\"1\" tvg-name=\"CCTV1\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV1.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV-1综合\n" +
//                "https://live.goodiptv.club/api/bestv.php?id=cctv1hd8m/8000000#http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221225804/index.m3u8?#http://223.95.111.98:5555/newlive/live/hls/1/live.m3u8#http://1.195.111.251:11190/tsfile/live/0001_1.m3u8#https://cntv.sbs/tv?auth=230813&id=cctv1\n" +
//                "#EXTINF:-1 tvg-id=\"2\" tvg-name=\"CCTV2\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV2.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV-2财经\n" +
//                "http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221226195/index.m3u8?#http://1.195.111.251:11190/tsfile/live/0002_1.m3u8#https://live.goodiptv.club/api/cqyx.php?id=cctv2HD#https://tv.iill.top/bestv/cctv2hd8m/8000000\n" +
//                "#EXTINF:-1 tvg-id=\"3\" tvg-name=\"CCTV3\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV3.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV-3综艺\n" +
//                "https://cntv.sbs/tv?auth=230813&id=cctv3#https://live.goodiptv.club/api/bestv.php?id=cctv38m/8000000#https://tv.iill.top/bestv/cctv38m/8000000#http://1.195.111.251:11190/tsfile/live/0003_1.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"4\" tvg-name=\"CCTV4\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV4.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV-4中文国际\n" +
//                "http://39.134.24.161/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221226191/index.m3u8?#http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221226191/index.m3u8?#https://tv.iill.top/bestv/cctv4hd8m/8000000#https://live.goodiptv.club/api/cqyx.php?id=cctv4HD#https://cntv.sbs/tv?auth=230813&id=cctv4\n" +
//                "#EXTINF:-1 tvg-id=\"5\" tvg-name=\"CCTV5\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV5.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV-5体育\n" +
//                "http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221226395/index.m3u8?#http://39.134.24.161/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221226395/index.m3u8?#https://live.goodiptv.club/api/bestv.php?id=cctv58m/8000000#https://tv.iill.top/bestv/cctv58m/8000000#http://1.195.111.251:11190/tsfile/live/0005_1.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"6\" tvg-name=\"CCTV5+\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV5+.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV-5+体育赛事\n" +
//                "https://live.goodiptv.club/api/bestv.php?id=cctv5phd8m/8000000#http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221225761/index.m3u8?#https://cntv.sbs/tv?auth=230813&id=cctv5p#https://tv.iill.top/bestv/cctv5phd8m/8000000\n" +
//                "#EXTINF:-1 tvg-id=\"7\" tvg-name=\"CCTV6\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV6.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV-6电影\n" +
//                "https://live.goodiptv.club/api/bestv.php?id=cctv6hd8m/8000000#https://cntv.sbs/tv?auth=230813&id=cctv6#http://1.195.111.251:11190/tsfile/live/0006_1.m3u8#https://tv.iill.top/bestv/cctv6hd8m/8000000\n" +
//                "#EXTINF:-1 tvg-id=\"8\" tvg-name=\"CCTV7\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV7.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV-7国防军事\n" +
//                "https://live.goodiptv.club/api/bestv.php?id=cctv7hd8m/8000000#https://cntv.sbs/tv?auth=230813&id=cctv7#http://1.195.111.251:11190/tsfile/live/0007_1.m3u8#https://tv.iill.top/bestv/cctv7hd8m/8000000\n" +
//                "#EXTINF:-1 tvg-id=\"9\" tvg-name=\"CCTV8\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV8.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV-8电视剧\n" +
//                "https://live.goodiptv.club/api/bestv.php?id=cctv8hd8m/8000000#https://cntv.sbs/tv?auth=230813&id=cctv8#http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221226012/index.m3u8?#http://1.195.111.251:11190/tsfile/live/0008_1.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"10\" tvg-name=\"CCTV9\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV9.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV-9纪录\n" +
//                "http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221226197/index.m3u8?#https://tv.iill.top/bestv/cctv9hd8m/8000000#http://1.195.111.251:11190/tsfile/live/0009_1.m3u8#https://cntv.sbs/tv?auth=230813&id=cctv9#https://live.goodiptv.club/api/cqyx.php?id=cctv9HD\n" +
//                "#EXTINF:-1 tvg-id=\"11\" tvg-name=\"CCTV10\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV10.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV-10科教\n" +
//                "https://live.goodiptv.club/api/bestv.php?id=cctv10hd8m/8000000#http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221226189/index.m3u8?#https://cntv.sbs/tv?auth=230813&id=cctv10#https://tv.iill.top/bestv/cctv10hd8m/8000000#http://1.195.111.251:11190/tsfile/live/0010_1.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"12\" tvg-name=\"CCTV11\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV11.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV-11戏曲\n" +
//                "https://cntv.sbs/tv?auth=230813&id=cctv11#http://1.195.111.251:11190/tsfile/live/0011_1.m3u8#https://tv.iill.top/bestv/cctv11hd8m/8000000#https://live.goodiptv.club/api/cqyx.php?id=cctv11HD\n" +
//                "#EXTINF:-1 tvg-id=\"13\" tvg-name=\"CCTV12\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV12.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV-12社会与法\n" +
//                "https://live.goodiptv.club/api/bestv.php?id=cctv12hd8m/8000000#http://1.195.111.251:11190/tsfile/live/0012_1.m3u8#https://tv.iill.top/bestv/cctv12hd8m/8000000\n" +
//                "#EXTINF:-1 tvg-id=\"14\" tvg-name=\"CCTV13\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV13.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV-13新闻\n" +
//                "https://cntv.sbs/tv?auth=230813&id=cctv13#https://live-play.cctvnews.cctv.com/cctv/merge_cctv13.m3u8#http://223.95.111.98:5555/newlive/live/hls/13/live.m3u8#http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221226233/index.m3u8?#https://tv.iill.top/bestv/cctv13xwhd8m/8000000\n" +
//                "#EXTINF:-1 tvg-id=\"15\" tvg-name=\"CCTV14\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV14.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV-14少儿\n" +
//                "https://live.goodiptv.club/api/bestv.php?id=cctvsehd8m/8000000#https://cntv.sbs/tv?auth=230813&id=cctv14#https://tv.iill.top/bestv/cctvsehd8m/8000000#http://1.195.111.251:11190/tsfile/live/0014_1.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"16\" tvg-name=\"CCTV15\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV15.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV-15音乐\n" +
//                "https://cntv.sbs/tv?auth=230813&id=cctv15#https://live.goodiptv.club/api/bestv.php?id=cctv15hd8m/8000000#http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221225785/index.m3u8?#http://1.195.111.251:11190/tsfile/live/0015_1.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"17\" tvg-name=\"CCTV17\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV17.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV-17农村农业\n" +
//                "https://live.goodiptv.club/api/bestv.php?id=cctv17hd8m/8000000#https://tv.iill.top/bestv/cctv17hd8m/8000000#https://cntv.sbs/tv?auth=230813&id=cctv17\n" +
//                "#EXTINF:-1 tvg-id=\"20\" tvg-name=\"CGTN\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/cgtn.png\" tvg-rez=\"0x0\" group-title=\"央视\",CGTN\n" +
//                "http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888888/224/3221225745/1.m3u8#https://livedoc.cgtn.com/500d/prog_index.m3u8#https://live.cgtn.com/1000/prog_index.m3u8#http://livees.cgtn.com/1000e/prog_index.m3u8#http://liveru.cgtn.com/1000r/prog_index.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"106\" tvg-name=\"CCTV4K\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV4k.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV4K\n" +
//                "http://159.75.85.63:5679/cctv4k.php#https://cntv.sbs/tv?auth=230813&id=cctv4k#https://live.goodiptv.club/api/cqyx.php?id=CCTV4K#http://liveop.cctv.cn/hls/4KHD/playlist.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"7249\" tvg-name=\"CCTV16\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/CCTV/CCTV16.png\" tvg-rez=\"0x0\" group-title=\"央视\",CCTV奥林匹克频道\n" +
//                "https://cntv.sbs/tv?auth=230813&id=cctv164k#https://live.goodiptv.club/api/bestv.php?id=cctv16hd8m/8000000#http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221226921/index.m3u8?#https://tv.iill.top/bestv/cctv16hd8m/8000000#http://39.134.24.166/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221226921/index.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"27\" tvg-name=\"湖南卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/hunan.png\" tvg-rez=\"0x0\" group-title=\"卫视\",湖南卫视\n" +
//                "https://live.goodiptv.club/api/bestv.php?id=hunanwshd8m/8000000#https://tv.iill.top/bestv/hunanwshd8m/8000000#https://cntv.sbs/tv?auth=230813&id=hunws#http://221.2.36.34:8888/newlive/live/hls/23/live.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"28\" tvg-name=\"浙江卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/zhejiang.png\" tvg-rez=\"0x0\" group-title=\"卫视\",浙江卫视\n" +
//                "http://hw-m-l.cztv.com/channels/lantian/channel001/1080p.m3u8#https://live.goodiptv.club/api/bestv.php?id=zjwshd8m/8000000#http://39.134.24.166/dbiptv.sn.chinamobile.com/PLTV/88888888/224/3221225798/1.m3u8#http://221.2.36.34:8888/newlive/live/hls/24/live.m3u8#https://cntv.sbs/tv?auth=230813&id=zjws\n" +
//                "#EXTINF:-1 tvg-id=\"29\" tvg-name=\"江苏卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/jiangsu.png\" tvg-rez=\"0x0\" group-title=\"卫视\",江苏卫视\n" +
//                "https://cntv.sbs/tv?auth=230813&id=jsws#http://39.134.24.166/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221226200/index.m3u8?#https://tv.iill.top/bestv/jswshd8m/8000000#http://221.2.36.34:8888/newlive/live/hls/22/live.m3u8#https://live.goodiptv.club/api/cqyx.php?id=jiangsuHD\n" +
//                "#EXTINF:-1 tvg-id=\"30\" tvg-name=\"北京卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/beijing.png\" tvg-rez=\"0x0\" group-title=\"卫视\",北京卫视\n" +
//                "https://live.goodiptv.club/api/bestv.php?id=bjwshd8m/8000000#https://tv.iill.top/bestv/bjwshd8m/8000000#https://cntv.sbs/tv?auth=230813&id=bjws#http://39.134.24.166/dbiptv.sn.chinamobile.com/PLTV/88888888/224/3221226222/1.m3u8#http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221225796/index.m3u8?\n" +
//                "#EXTINF:-1 tvg-id=\"31\" tvg-name=\"东方卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/dongfang.png\" tvg-rez=\"0x0\" group-title=\"卫视\",东方卫视\n" +
//                "http://221.2.36.34:8888/newlive/live/hls/20/live.m3u8#https://tv.iill.top/bestv/dfwshd8m/8000000#https://live.goodiptv.club/api/cqyx.php?id=shanghaiHD#https://cntv.sbs/tv?auth=230813&id=dfws\n" +
//                "#EXTINF:-1 tvg-id=\"32\" tvg-name=\"安徽卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/anhui.png\" tvg-rez=\"0x0\" group-title=\"卫视\",安徽卫视\n" +
//                "https://cntv.sbs/tv?auth=230813&id=ahws#http://221.2.36.34:8888/newlive/live/hls/25/live.m3u8#https://live.goodiptv.club/api/cqyx.php?id=anhuiSD\n" +
//                "#EXTINF:-1 tvg-id=\"33\" tvg-name=\"广东卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/guangdong.png\" tvg-rez=\"0x0\" group-title=\"卫视\",广东卫视\n" +
//                "https://cntv.sbs/tv?auth=230813&id=gdws#http://39.134.24.166/dbiptv.sn.chinamobile.com/PLTV/88888888/224/3221226216/1.m3u8#https://tv.iill.top/bestv/gdwshd8m/8000000#http://221.2.36.34:8888/newlive/live/hls/21/live.m3u8#https://live.goodiptv.club/api/cqyx.php?id=guangdongHD\n" +
//                "#EXTINF:-1 tvg-id=\"34\" tvg-name=\"深圳卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/shenzhen.png\" tvg-rez=\"0x0\" group-title=\"卫视\",深圳卫视\n" +
//                "http://39.134.24.166/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221226205/index.m3u8?#https://tv.iill.top/bestv/szwshd8m/8000000#http://221.2.36.34:8888/newlive/live/hls/28/live.m3u8#https://cntv.sbs/tv?auth=230813&id=szws#https://live.goodiptv.club/api/cqyx.php?id=shenzhenHD\n" +
//                "#EXTINF:-1 tvg-id=\"36\" tvg-name=\"辽宁卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/liaoning.png\" tvg-rez=\"0x0\" group-title=\"卫视\",辽宁卫视\n" +
//                "https://live.goodiptv.club/api/bestv.php?id=lnwshd8m/8000000#http://221.2.36.34:8888/newlive/live/hls/43/live.m3u8#https://cntv.sbs/tv?auth=230813&id=lnws#https://tv.iill.top/bestv/lnwshd8m/8000000\n" +
//                "#EXTINF:-1 tvg-id=\"37\" tvg-name=\"旅游卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/lvyou.png\" tvg-rez=\"0x0\" group-title=\"卫视\",海南卫视\n" +
//                "https://cntv.sbs/tv?auth=230813&id=hinws#https://tv.iill.top/bestv/hainanwshd8m/8000000#http://221.2.36.34:8888/newlive/live/hls/47/live.m3u8#https://live.goodiptv.club/api/bestv.php?id=hainanwshd8m/8000000#http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221225769/index.m3u8?\n" +
//                "#EXTINF:-1 tvg-id=\"38\" tvg-name=\"山东卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/shandong.png\" tvg-rez=\"0x0\" group-title=\"卫视\",山东卫视\n" +
//                "https://live.goodiptv.club/api/bestv.php?id=sdws8m/8000000#https://tv.iill.top/bestv/sdws8m/8000000#http://221.2.36.34:8888/newlive/live/hls/30/live.m3u8#https://cntv.sbs/tv?auth=230813&id=sdws\n" +
//                "#EXTINF:-1 tvg-id=\"39\" tvg-name=\"天津卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/tianjin.png\" tvg-rez=\"0x0\" group-title=\"卫视\",天津卫视\n" +
//                "https://live.goodiptv.club/api/bestv.php?id=tjwshd8m/8000000#https://cntv.sbs/tv?auth=230813&id=tjws#http://221.2.36.34:8888/newlive/live/hls/42/live.m3u8#https://tv.iill.top/bestv/tjwshd8m/8000000\n" +
//                "#EXTINF:-1 tvg-id=\"40\" tvg-name=\"重庆卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/chongqing.png\" tvg-rez=\"0x0\" group-title=\"卫视\",重庆卫视\n" +
//                "https://cntv.sbs/tv?auth=230813&id=cqws#https://live.goodiptv.club/api/bestv.php?id=cqws8m/8000000#http://221.2.36.34:8888/newlive/live/hls/31/live.m3u8#https://tv.iill.top/bestv/cqws8m/8000000\n" +
//                "#EXTINF:-1 tvg-id=\"41\" tvg-name=\"东南卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/dongnan.png\" tvg-rez=\"0x0\" group-title=\"卫视\",东南卫视\n" +
//                "https://tv.iill.top/bestv/dnwshd8m/8000000#http://221.2.36.34:8888/newlive/live/hls/44/live.m3u8#https://live.goodiptv.club/api/cqyx.php?id=dongnanHD#https://cntv.sbs/tv?auth=230813&id=dnws\n" +
//                "#EXTINF:-1 tvg-id=\"42\" tvg-name=\"甘肃卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/gansu.png\" tvg-rez=\"0x0\" group-title=\"卫视\",甘肃卫视\n" +
//                "https://live.goodiptv.club/api/bestv.php?id=gswshd8m/8000000#https://tv.iill.top/bestv/gswshd8m/8000000#https://cntv.sbs/tv?auth=230813&id=gsws\n" +
//                "#EXTINF:-1 tvg-id=\"43\" tvg-name=\"广西卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/guangxi.png\" tvg-rez=\"0x0\" group-title=\"卫视\",广西卫视\n" +
//                "http://221.2.36.34:8888/newlive/live/hls/26/live.m3u8#https://tv.iill.top/bestv/gxwshd8m/8000000#https://cntv.sbs/tv?auth=230813&id=gxws#https://live.goodiptv.club/api/cqyx.php?id=guangxiHD\n" +
//                "#EXTINF:-1 tvg-id=\"44\" tvg-name=\"贵州卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/guizhou.png\" tvg-rez=\"0x0\" group-title=\"卫视\",贵州卫视\n" +
//                "https://tv.iill.top/bestv/gzwshd8m/8000000#https://cntv.sbs/tv?auth=230813&id=gzws#http://221.2.36.34:8888/newlive/live/hls/29/live.m3u8#https://live.goodiptv.club/api/cqyx.php?id=guizhouHD\n" +
//                "#EXTINF:-1 tvg-id=\"45\" tvg-name=\"河北卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/hebei.png\" tvg-rez=\"0x0\" group-title=\"卫视\",河北卫视\n" +
//                "https://tv.iill.top/bestv/hbwshd8m/8000000#https://live.goodiptv.club/api/bestv.php?id=hbwshd8m/8000000#https://cntv.sbs/tv?auth=230813&id=hbws#http://221.2.36.34:8888/newlive/live/hls/46/live.m3u8#http://event.pull.hebtv.com/jishi/weishipindao.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"46\" tvg-name=\"黑龙江卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/heilongjiang.png\" tvg-rez=\"0x0\" group-title=\"卫视\",黑龙江卫视\n" +
//                "https://cntv.sbs/tv?auth=230813&id=hljws#http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888888/224/3221225802/1.m3u8#https://tv.iill.top/bestv/hljwshd8m/8000000#https://live.goodiptv.club/api/bestv.php?id=hljwshd8m/8000000#http://221.2.36.34:8888/newlive/live/hls/45/live.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"47\" tvg-name=\"河南卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/henan.png\" tvg-rez=\"0x0\" group-title=\"卫视\",河南卫视\n" +
//                "https://live.goodiptv.club/api/bestv.php?id=hnwshd8m/8000000#http://221.2.36.34:8888/newlive/live/hls/33/live.m3u8#http://media.hndyjyfw.gov.cn/live/jz-hnweishi/live.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"48\" tvg-name=\"湖北卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/hubei.png\" tvg-rez=\"0x0\" group-title=\"卫视\",湖北卫视\n" +
//                "http://221.2.36.34:8888/newlive/live/hls/34/live.m3u8#https://tv.iill.top/bestv/hubeiws8m/8000000#https://cntv.sbs/tv?auth=230813&id=hubws#https://live.goodiptv.club/api/cqyx.php?id=hubeiSD\n" +
//                "#EXTINF:-1 tvg-id=\"50\" tvg-name=\"江西卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/jiangxi.png\" tvg-rez=\"0x0\" group-title=\"卫视\",江西卫视\n" +
//                "https://cntv.sbs/tv?auth=230813&id=jxws#https://tv.iill.top/bestv/jxws8m/8000000#http://221.2.36.34:8888/newlive/live/hls/48/live.m3u8#https://live.goodiptv.club/api/cqyx.php?id=jiangxiHD\n" +
//                "#EXTINF:-1 tvg-id=\"51\" tvg-name=\"吉林卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/jilin.png\" tvg-rez=\"0x0\" group-title=\"卫视\",吉林卫视\n" +
//                "https://cntv.sbs/tv?auth=230813&id=jlws#https://tv.iill.top/bestv/jlwshd8m/8000000#http://221.2.36.34:8888/newlive/live/hls/35/live.m3u8#https://live.goodiptv.club/api/bestv.php?id=jlwshd8m/8000000\n" +
//                "#EXTINF:-1 tvg-id=\"52\" tvg-name=\"内蒙古卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/neimeng.png\" tvg-rez=\"0x0\" group-title=\"卫视\",内蒙古卫视\n" +
//                "http://lu1.cc/c/tv/nmtv/nmxl.php?id=nmws#https://cntv.sbs/tv?auth=230813&id=nmws#https://live.goodiptv.club/api/cqyx.php?id=neimengkuSD\n" +
//                "#EXTINF:-1 tvg-id=\"53\" tvg-name=\"宁夏卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/ningxia.png\" tvg-rez=\"0x0\" group-title=\"卫视\",宁夏卫视\n" +
//                "https://cntv.sbs/tv?auth=230813&id=nxws\n" +
//                "#EXTINF:-1 tvg-id=\"54\" tvg-name=\"山西卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/shanxi_.png\" tvg-rez=\"0x0\" group-title=\"卫视\",山西卫视\n" +
//                "https://cntv.sbs/tv?auth=230813&id=sxiws\n" +
//                "#EXTINF:-1 tvg-id=\"55\" tvg-name=\"陕西卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/shanxi.png\" tvg-rez=\"0x0\" group-title=\"卫视\",陕西卫视\n" +
//                "https://cntv.sbs/tv?auth=230813&id=sxws\n" +
//                "#EXTINF:-1 tvg-id=\"56\" tvg-name=\"四川卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/sichuan.png\" tvg-rez=\"0x0\" group-title=\"卫视\",四川卫视\n" +
//                "https://cntv.sbs/tv?auth=230813&id=scws#https://live.goodiptv.club/api/bestv.php?id=scwshd/8000000#http://221.2.36.34:8888/newlive/live/hls/32/live.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"57\" tvg-name=\"新疆卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/xinjiang.png\" tvg-rez=\"0x0\" group-title=\"卫视\",新疆卫视\n" +
//                "https://cntv.sbs/tv?auth=230813&id=xjws#http://221.2.36.34:8888/newlive/live/hls/38/live.m3u8#http://39.134.24.166/dbiptv.sn.chinamobile.com/PLTV/88888888/224/3221225747/1.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"58\" tvg-name=\"云南卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/yunnan.png\" tvg-rez=\"0x0\" group-title=\"卫视\",云南卫视\n" +
//                "https://cntv.sbs/tv?auth=230813&id=ynws#https://tv.iill.top/bestv/ynwshd8m/8000000#https://live.goodiptv.club/api/bestv.php?id=ynwshd8m/8000000#http://221.2.36.34:8888/newlive/live/hls/27/live.m3u8#http://tvlive.ynradio.com/live/yunnanweishi/chunks.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"59\" tvg-name=\"青海卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/qinghai.png\" tvg-rez=\"0x0\" group-title=\"卫视\",青海卫视\n" +
//                "http://221.2.36.34:8888/newlive/live/hls/39/live.m3u8#http://stream.qhbtv.com/qhws/sd/live.m3u8#https://cntv.sbs/tv?auth=230813&id=qhws\n" +
//                "#EXTINF:-1 tvg-id=\"60\" tvg-name=\"南方卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/nanfang.png\" tvg-rez=\"0x0\" group-title=\"卫视\",南方卫视\n" +
//                "http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221226218/index.m3u8?\n" +
//                "#EXTINF:-1 tvg-id=\"61\" tvg-name=\"兵团卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/bingtuan.png\" tvg-rez=\"0x0\" group-title=\"卫视\",兵团卫视\n" +
//                "https://cntv.sbs/tv?auth=230813&id=btws#http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221226214/index.m3u8#https://live.goodiptv.club/api/cqyx.php?id=bingtuanSD\n" +
//                "#EXTINF:-1 tvg-id=\"63\" tvg-name=\"延边卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/YANBIAN1.png\" tvg-rez=\"0x0\" group-title=\"卫视\",延边卫视\n" +
//                "http://live.ybtvyun.com/video/s10016-6f0dfd97912f/index.m3u8#http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221226220/index.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"68\" tvg-name=\"厦门卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/xiamen.png\" tvg-rez=\"0x0\" group-title=\"卫视\",厦门卫视\n" +
//                "https://cntv.sbs/tv?auth=230813&id=xmws\n" +
//                "#EXTINF:-1 tvg-id=\"69\" tvg-name=\"金鹰卡通\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/qt/jinyingkatong.png\" tvg-rez=\"0x0\" group-title=\"卫视\",金鹰卡通\n" +
//                "http://223.151.51.27:85/tsfile/live/1016_1.m3u8?key=txiptv&playlive=1&authid=0#https://live.goodiptv.club/api/cqyx.php?id=jinyingSD#https://cntv.sbs/tv?auth=230813&id=jykt\n" +
//                "#EXTINF:-1 tvg-id=\"70\" tvg-name=\"康巴卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/kangba.png\" tvg-rez=\"0x0\" group-title=\"卫视\",康巴卫视\n" +
//                "https://cntv.sbs/tv?auth=230813&id=kbws\n" +
//                "#EXTINF:-1 tvg-id=\"71\" tvg-name=\"西藏卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/xizang.png\" tvg-rez=\"0x0\" group-title=\"卫视\",西藏卫视\n" +
//                "http://lu1.cc/c/tv/xztv/xzxl.php?id=xzws#https://cntv.sbs/tv?auth=230813&id=xzws#http://221.2.36.34:8888/newlive/live/hls/37/live.m3u8#http://39.134.24.162/dbiptv.sn.chinamobile.com/PLTV/88888890/224/3221226212/index.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"72\" tvg-name=\"三沙卫视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/ws/sansha.png\" tvg-rez=\"0x0\" group-title=\"卫视\",三沙卫视\n" +
//                "https://cntv.sbs/tv?auth=230813&id=ssws\n" +
//                "#EXTINF:-1 tvg-id=\"1987\" tvg-name=\"五星体育频道\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/qt/wuxingtiyu.png\" tvg-rez=\"0x0\" group-title=\"上海\",五星体育频道\n" +
//                "https://cntv.sbs/tv?auth=230813&id=wxty#https://tv.iill.top/bestv/wxtyhd8m/8000000#https://live.goodiptv.club/api/bestv.php?id=wxtyhd8m/8000000\n" +
//                "#EXTINF:-1 tvg-id=\"126\" tvg-name=\"翡翠台\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/gt/TVB翡翠台.png\" tvg-rez=\"0x0\" group-title=\"香港\",翡翠台\n" +
//                "http://r.jdshipin.com/qrfbg#http://198.16.100.90:8278/tvbxinhe_twn/playlist.m3u8?tid=ME2E8352893183528931&ct=19225&tsum=5f852c878e02000708a055c6b08a9405\n" +
//                "#EXTINF:-1 tvg-id=\"142\" tvg-name=\"凤凰资讯\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/gt/fenghuangzixun.png\" tvg-rez=\"0x0\" group-title=\"香港\",凤凰卫视资讯台\n" +
//                "http://lu1.cc/c/tv/gatv/fhtv.php?id=fhzx#https://raw.githubusercontent.com/ChiSheng9/iptv/master/TV22.m3u8#http://162.19.247.76:22222/live/fenghuangzhixun/index.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"143\" tvg-name=\"凤凰香港\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/gt/fenghuangxianggang.png\" tvg-rez=\"0x0\" group-title=\"香港\",凤凰卫视香港台\n" +
//                "http://162.19.247.76:22222/live/fenghuangxianggang/index.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"412\" tvg-name=\"HKS\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/gt/hks.png\" tvg-rez=\"0x0\" group-title=\"香港\",香港卫视\n" +
//                "http://zhibo.hkstv.tv/livestream/mutfysrq/playlist.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"7117\" tvg-name=\"香港国际财经台\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/gt/hkguojicaijing.png\" tvg-rez=\"0x0\" group-title=\"香港\",香港国际财经台\n" +
//                "http://uc6.i-cable.com/live_freedirect/opentvhd002_h.live/playlist.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"7118\" tvg-name=\"香港开电视\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/gt/hongkongkai.png\" tvg-rez=\"0x0\" group-title=\"香港\",香港开电视\n" +
//                "https://hoytv-live-stream.hoy.tv/ch77/index-fhd.m3u8\n" +
//                "#EXTINF:-1 tvg-id=\"7122\" tvg-name=\"有线新闻台\" tvg-logo=\"http://epg.51zmt.top:8000/tb1/gt/youxianxinwen.png\" tvg-rez=\"0x0\" group-title=\"香港\",有线新闻台\n" +
//                "https://api.leonardpark.dev/live/icable/109#http://61.10.2.141/live_freedirect/freehd209_h.live/playlist.m3u8#http://61.10.2.140:80/live_freedirect/freehd209_h.live/chunklist_w135209556.m3u8";
//
//        Pattern extinfPattern = Pattern.compile("#EXTINF:-1 tvg-id=\"(.*?)\" tvg-name=\"(.*?)\" tvg-logo=\"(.*?)\" tvg-rez=\"(.*?)\" group-title=\"(.*?)\",(.*?)\n(.*?)\n");
//        Matcher matcher = extinfPattern.matcher(m3u8Content);
//
//        while (matcher.find()) {
//            String tvgId = matcher.group(1);
//            String tvgName = matcher.group(2);
//            String tvgLogo = matcher.group(3);
//            String tvgRez = matcher.group(4);
//            String groupTitle = matcher.group(5);
//            String title = matcher.group(6);
//            String playUrl = matcher.group(7);
//
//            List<StreamVo> list = new ArrayList<>();
//            String[] split = playUrl.split("#");
//            for (int i = 0; i < split.length; i++) {
//                StreamVo streamVo =new StreamVo();
//                streamVo.setUrl(split[i]);
//
//                if(split[i].contains("m3u8"))
//                list.add(streamVo);
//            }
//
//
//            System.out.println(tvgName + "\t" + title + "\t" + tvgLogo + "\t" + JSON.toJSONString(list));
//        }
    }
}