package com.ruoyi.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileUtil {


    private static final Logger logger = LoggerFactory.getLogger(FileUtil.class);

    public static void mkdirPath(String filePath){
        File file = new File(filePath);
        if( ! file.exists()){
            file.mkdirs();
        }
    }


    /**
     * 删除文件
     * @param fileFullPath
     * @return
     */
    public static boolean deleteFile(String fileFullPath){
        File file = new File(fileFullPath);
        return deleteFile(file);
    }
    /**
     * 删除文件
     * @param file
     * @return
     */
    public static boolean deleteFile(File file){
        if(file.exists()){
            file.delete();
            return true;
        }
        return false;
    }




    /**
     * 获取网络地址，的文件名字
     * @param urlPath
     * @param suffixes
     * @return
     */
    public  static String getUrlFileName(String urlPath,String suffixes){
        Pattern pat=Pattern.compile("[\\w]+[\\.]("+suffixes+")");//正则判断
        Matcher mc=pat.matcher(urlPath);//条件匹配
        while(mc.find()) {
            return  mc.group();//截取文件名后缀名
        }
        return null ;
    }

    /**
     * 下载网络文件
     * @param urlStr
     * @param fileName
     * @param savePath
     * @return
     * @throws IOException
     */
    public static boolean downloadFromIntNet(String urlStr, String fileName, String savePath)  {
        try{

            long l = System.currentTimeMillis();
            URL url = new URL(urlStr);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            //设置超时间为30秒
            conn.setConnectTimeout(30*1000);
            //防止屏蔽程序抓取而返回403错误
            conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
            //得到输入流
            InputStream inputStream = conn.getInputStream();
            //创建保存路径
            File saveDir = new File(savePath);
            if(!saveDir.exists()) {
                saveDir.mkdirs();
            }
            //创建文件
            File file = new File(savePath+File.separator+fileName);
            //文件输出流
            FileOutputStream fout = new FileOutputStream(file);
            //输出文件
            byte[] buff = new byte[4096];
            int len = -1;
            while ((len = inputStream.read(buff)) != -1) {
                fout.write(buff,0,len);
            }
            //关闭输出输入流
            fout.close();
            inputStream.close();
            logger.info("download-url success :" + urlStr + "====下载时长："+(System.currentTimeMillis()-l));
            return true ;
        }catch (Exception e){
            logger.info("download-url failed :" + urlStr);
            e.printStackTrace();
            return false;
        }
    }

    public static boolean isExists(String path) {

        return new File(path).exists();
    }

    public static boolean mkdir(String path) {

        return new File(path).mkdirs();
    }

    public static String getSuffix(String path) {

        int lastIndexOf = path.lastIndexOf(".");
        if(lastIndexOf!=-1)
        {
            String suffix = path.substring(lastIndexOf);
            return suffix;
        }
        return path;
    }

//    public static void main(String[] args)
//    {
//        MyLog.debug(getSuffix("E:\\tme\\album\\279085261\\5.wav"));
//    }

    public static String getNameWithoutSuffix(String path) {

        String name = new File(path).getName();
        int lastIndexOf = name.lastIndexOf(".");
        if(lastIndexOf!=-1)
        {
            name = name.substring(0,lastIndexOf);
            return name;
        }
        else
        {
            return name;
        }
    }




}
