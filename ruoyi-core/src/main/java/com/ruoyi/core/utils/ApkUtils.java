package com.ruoyi.core.utils;

import com.intelliset.apk.parser.ApkFile;
import com.intelliset.apk.parser.ApkParser;
import com.intelliset.apk.parser.FileUtil;
import com.intelliset.apk.parser.bean.ApkMeta;
import com.ruoyi.core.domain.ApkInfo;

import java.io.File;
import java.io.IOException;

public class ApkUtils {


    public static ApkInfo getApkInfo(String apkFilePath){
        ApkInfo apkInfo = new ApkInfo();
        // APK文件路径
        File file = new File(apkFilePath);
        try {
            ApkParser apkParser = new ApkParser(file);
            ApkMeta apkMeta = apkParser.getApkMeta();

            apkInfo.setPackageName(apkMeta.getPackageName());
            apkInfo.setVersionName(apkMeta.getVersionName());
            apkInfo.setName(apkMeta.getName());
            apkInfo.setVersionCode(apkMeta.getVersionCode());

            //保存icon
            if (apkMeta.getIcon() != null) {
                ApkFile apkFile = new ApkFile(new File(apkFilePath));
                byte[] iconData = apkFile.getFileData(apkMeta.getIcon());
                FileUtil.writeFileData(apkFilePath+".png",iconData);
                File iconFile = new File(apkFilePath + ".png");
            }

            apkInfo.setLength(file.length());

            apkParser.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return apkInfo;
    }

}
