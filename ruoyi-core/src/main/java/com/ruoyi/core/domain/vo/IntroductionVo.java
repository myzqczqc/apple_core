package com.ruoyi.core.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 直播流的结构
 */
@ApiModel(value = "IntroductionVo", description = "影视剧描述的数据信息")
public class IntroductionVo {

    @ApiModelProperty("描述名字")
    private String title ;
    @ApiModelProperty("描述内容")
    private String desc ;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
