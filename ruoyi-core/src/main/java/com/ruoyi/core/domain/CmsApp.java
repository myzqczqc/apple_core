package com.ruoyi.core.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * app配置对象 cms_app
 *
 * @author zqc
 * @date 2023-11-17
 */
public class CmsApp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 1：游戏，2：应用 */
    @Excel(name = "1：游戏，2：应用")
    private Long ap;
    private Long apType;

    /** apkid */
    @Excel(name = "apkid")
    private Long apkId;

    /** 顺序号 */
    @Excel(name = "顺序号")
    private Long seq;

    /** 渠道号 */
    @Excel(name = "渠道号")
    private String channelId;
    // 渠道名称
    private String channelName;
    // apk名称
    private String apkName;
    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setAp(Long ap)
    {
        this.ap = ap;
    }

    public Long getAp()
    {
        return ap;
    }
    public void setApkId(Long apkId)
    {
        this.apkId = apkId;
    }

    public Long getApType() {
        return apType;
    }

    public void setApType(Long apType) {
        this.apType = apType;
    }

    public Long getApkId()
    {
        return apkId;
    }
    public void setSeq(Long seq)
    {
        this.seq = seq;
    }

    public Long getSeq()
    {
        return seq;
    }
    public void setChannelId(String channelId)
    {
        this.channelId = channelId;
    }

    public String getChannelId()
    {
        return channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getApkName() {
        return apkName;
    }

    public void setApkName(String apkName) {
        this.apkName = apkName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ap", getAp())
            .append("apkId", getApkId())
            .append("seq", getSeq())
            .append("channelId", getChannelId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
