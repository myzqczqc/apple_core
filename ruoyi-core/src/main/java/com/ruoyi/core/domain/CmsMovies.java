package com.ruoyi.core.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 影视数据信息对象 cms_movies
 * 
 * @author zqc
 * @date 2023-11-06
 */
@ApiModel(value = "CmsMovies", description = "影视剧信息")
public class CmsMovies extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 影视名字 */
    @Excel(name = "影视名字")
    @ApiModelProperty("影视名字")
    private String title;

    /** 副标题 */
    @Excel(name = "副标题")
    @ApiModelProperty("副标题")
    private String subTitle;


    /** 封面图 */
    @Excel(name = "封面图")
    @ApiModelProperty("封面图")
    private String image;

    /** 剧集的集数 */
    @Excel(name = "剧集的集数")
    @ApiModelProperty("剧集的集数")
    private Long episodeCount;
    private Integer typeId;
    private Integer languageId ;

    /** 简介 */
    @Excel(name = "简介")
    @ApiModelProperty("简介")
    private String introduction;

    @ApiModelProperty("影视剧标签")
    private List<CmsMtype> mtypeList;
    @ApiModelProperty("剧集列表")
    private List<CmsEpisode> episodeList;

    public List<CmsEpisode> getEpisodeList() {
        return episodeList;
    }

    public void setEpisodeList(List<CmsEpisode> episodeList) {
        this.episodeList = episodeList;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public List<CmsMtype> getMtypeList() {
        return mtypeList;
    }

    public void setMtypeList(List<CmsMtype> mtypeList) {
        this.mtypeList = mtypeList;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setSubTitle(String subTitle) 
    {
        this.subTitle = subTitle;
    }

    public String getSubTitle() 
    {
        return subTitle;
    }
    public void setImage(String image)
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setEpisodeCount(Long episodeCount) 
    {
        this.episodeCount = episodeCount;
    }

    public Long getEpisodeCount() 
    {
        return episodeCount;
    }
    public void setIntroduction(String introduction) 
    {
        this.introduction = introduction;
    }

    public String getIntroduction() 
    {
        return introduction;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("subTitle", getSubTitle())
            .append("image", getImage())
            .append("episodeCount", getEpisodeCount())
            .append("introduction", getIntroduction())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
