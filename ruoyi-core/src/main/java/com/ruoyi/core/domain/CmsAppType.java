package com.ruoyi.core.domain;

import com.ruoyi.core.domain.vo.CmsApkInfoVo;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * app子分类对象 cms_app_type
 * 
 * @author zqc
 * @date 2023-11-25
 */
public class CmsAppType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 1：游戏，2：应用 */
    @Excel(name = "1：游戏，2：应用")
    private Integer ap;

    /** 子分类名字 */
    @Excel(name = "子分类名字")
    private String name;

    @Excel(name = "顺序号")
    private Integer seq;

    private List<CmsApkInfoVo> apkList ;

    public List<CmsApkInfoVo> getApkList() {
        return apkList;
    }

    public void setApkList(List<CmsApkInfoVo> apkList) {
        this.apkList = apkList;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAp(Integer ap) 
    {
        this.ap = ap;
    }

    public Integer getAp() 
    {
        return ap;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ap", getAp())
            .append("name", getName())
            .toString();
    }
}
