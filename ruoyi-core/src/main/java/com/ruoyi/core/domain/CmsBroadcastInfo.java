package com.ruoyi.core.domain;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.core.domain.vo.CmsBroadcastInfoVo;
import com.ruoyi.core.domain.vo.StreamVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 直播流信息对象 cms_broadcast_info
 * 
 * @author zqc
 * @date 2023-11-06
 */
@ApiModel(value = "CmsBroadcastInfo", description = "直播流信息")
public class CmsBroadcastInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 直播流名字 */
    @Excel(name = "直播流名字")
    @ApiModelProperty("直播流名字")
    private String title;

    /** 副标题 */
    @Excel(name = "副标题")
    @ApiModelProperty("副标题")
    private String subTitle;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long status;
    private Integer typeId ;
    private Integer languageId ;

    /** 图片 */
    @Excel(name = "图片")
    @ApiModelProperty("图片")
    private String image;

    /** 直播流地址，多个，用json保存 */
    @Excel(name = "直播流地址，多个，用json保存")
    @ApiModelProperty("直播流地址，多个")
    private String streaming;

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setSubTitle(String subTitle) 
    {
        this.subTitle = subTitle;
    }

    public String getSubTitle() 
    {
        return subTitle;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setStreaming(String streaming) 
    {
        this.streaming = streaming;
    }

    public String getStreaming() 
    {
        return streaming;
    }

    /**
     * 数据库bean  转 vo
     * @return
     */
    public CmsBroadcastInfoVo broadcast2Vo(){
        CmsBroadcastInfoVo cmsBroadcastInfoVo = new CmsBroadcastInfoVo();
        BeanUtils.copyBeanProp(cmsBroadcastInfoVo,this);
        cmsBroadcastInfoVo.setStreaming(JSON.parseArray(this.streaming, StreamVo.class));
        return cmsBroadcastInfoVo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("subTitle", getSubTitle())
            .append("status", getStatus())
            .append("image", getImage())
            .append("streaming", getStreaming())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
