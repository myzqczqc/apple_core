package com.ruoyi.core.domain.vo;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.core.domain.CmsBroadcastInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * 直播流信息对象 cms_glasses_broadcast
 * 
 * @author ruoyi
 * @date 2023-08-16
 */
@ApiModel(value = "CmsGlassesBroadcastVo", description = "直播流详细信息")
public class CmsBroadcastInfoVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 直播流名字 */
    @Excel(name = "直播流名字")
    @ApiModelProperty("直播流名字")
    private String title;

    /** 副标题 */
    @Excel(name = "副标题")
    @ApiModelProperty("副标题")
    private String subTitle;

    private Integer status ;
    private Integer typeId ;
    private Integer languageId ;
    /** 图片 */
    @Excel(name = "图片")
    @ApiModelProperty("图片")
    private String image;

    /** 直播流地址，多个，用json保存 */
    @Excel(name = "直播流地址，多个，用json保存")
    @ApiModelProperty("直播流地址")
    private List<StreamVo> streaming;

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setSubTitle(String subTitle) 
    {
        this.subTitle = subTitle;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getSubTitle()
    {
        return subTitle;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }

    public List<StreamVo> getStreaming() {
        return streaming;
    }

    public void setStreaming(List<StreamVo> streaming) {
        this.streaming = streaming;
    }

    /**
     * vo  专辑数据库bean类
     * @return
     */
    public CmsBroadcastInfo vo2broadcast(){
        CmsBroadcastInfo cmsBroadcastInfo = new CmsBroadcastInfo();
        BeanUtils.copyBeanProp(cmsBroadcastInfo,this);
        cmsBroadcastInfo.setStreaming(JSON.toJSONString(this.streaming));
        return cmsBroadcastInfo;
    }



    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("subTitle", getSubTitle())
            .append("image", getImage())
            .append("streaming", getStreaming())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
