package com.ruoyi.core.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 影视类型，关系对象 cms_movies_type
 * 
 * @author zqc
 * @date 2023-11-07
 */
public class CmsMoviesType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 影视id */
    @Excel(name = "影视id")
    private Long moviesId;

    /** 影视类型id */
    @Excel(name = "影视类型id")
    private Long typeId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMoviesId(Long moviesId) 
    {
        this.moviesId = moviesId;
    }

    public Long getMoviesId() 
    {
        return moviesId;
    }
    public void setTypeId(Long typeId) 
    {
        this.typeId = typeId;
    }

    public Long getTypeId() 
    {
        return typeId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("moviesId", getMoviesId())
            .append("typeId", getTypeId())
            .toString();
    }
}
