package com.ruoyi.core.domain.vo;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.core.domain.CmsApkInfo;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * apk信息对象 cms_apk_info
 * 
 * @author zqc
 * @date 2023-11-06
 */
public class CmsApkInfoVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 名字 */
    @Excel(name = "名字")
    private String name;

    /** 唯一标识 */
    @Excel(name = "唯一标识")
    private String packageName;

    /** icon */
    @Excel(name = "icon")
    private String icon;
    private List<String> image;

    /** 文件大小 */
    @Excel(name = "文件大小")
    private Long length;

    /** 文件地址 */
    @Excel(name = "文件地址")
    private String url;

    /** version_code */
    @Excel(name = "version_code")
    private String versionCode;

    /** version_name */
    @Excel(name = "version_name")
    private String versionName;

    /** 1：共有，0：私有 */
    @Excel(name = "1：共有，0：私有")
    private Integer typeId;
    @Excel(name = "1：游戏，2：应用")
    private Integer ap;
    @Excel(name = "1：游戏，2：应用的子分类")
    private Integer apType;
    private Integer languageId;

    @Excel(name = "apk简介")
    private String introduction;

    @Excel(name = "slogan")
    private String slogan;

    public Integer getApType() {
        return apType;
    }

    public void setApType(Integer apType) {
        this.apType = apType;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public Integer getAp() {
        return ap;
    }

    public void setAp(Integer ap) {
        this.ap = ap;
    }

    public List<String> getImage() {
        return image;
    }

    public void setImage(List<String> image) {
        this.image = image;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPackageName(String packageName) 
    {
        this.packageName = packageName;
    }

    public String getPackageName() 
    {
        return packageName;
    }
    public void setIcon(String icon) 
    {
        this.icon = icon;
    }

    public String getIcon() 
    {
        return icon;
    }
    public void setLength(Long length) 
    {
        this.length = length;
    }

    public Long getLength() 
    {
        return length;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }
    public void setVersionCode(String versionCode) 
    {
        this.versionCode = versionCode;
    }

    public String getVersionCode() 
    {
        return versionCode;
    }
    public void setVersionName(String versionName) 
    {
        this.versionName = versionName;
    }

    public String getVersionName() 
    {
        return versionName;
    }
    public void setTypeId(Integer typeId) 
    {
        this.typeId = typeId;
    }

    public Integer getTypeId() 
    {
        return typeId;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public CmsApkInfo vo2CmsApkInfo(){
        CmsApkInfo cmsApkInfo = new CmsApkInfo();
        BeanUtils.copyBeanProp(cmsApkInfo,this);
        if(StringUtils.isNotEmpty(this.image)){
            cmsApkInfo.setImage(JSON.toJSONString(this.image));
        }
        return cmsApkInfo;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("packageName", getPackageName())
            .append("icon", getIcon())
            .append("length", getLength())
            .append("url", getUrl())
            .append("versionCode", getVersionCode())
            .append("versionName", getVersionName())
            .append("typeId", getTypeId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("remark", getRemark())
            .append("introduction", getIntroduction())
            .append("slogan", getSlogan())
            .toString();
    }
}
