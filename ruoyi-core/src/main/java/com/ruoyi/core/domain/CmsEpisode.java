package com.ruoyi.core.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 剧集信息对象 cms_episode
 * 
 * @author zqc
 * @date 2023-11-06
 */
@ApiModel(value = "CmsEpisode", description = "剧集信息")
public class CmsEpisode extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 剧集名字 */
    @Excel(name = "剧集名字")
    @ApiModelProperty("剧集名字")
    private String title;

    /** 副标题 */
    @Excel(name = "副标题")
    @ApiModelProperty("副标题")
    private String subTitle;

    /** 影视名字 */
    @Excel(name = "影视名字")
    private Long moviesId;

    /** 顺序号 */
    @Excel(name = "顺序号")
    @ApiModelProperty("顺序号")
    private Integer seq;

    /** 播放地址 */
    @Excel(name = "播放地址")
    @ApiModelProperty("播放地址")
    private String url;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setSubTitle(String subTitle) 
    {
        this.subTitle = subTitle;
    }

    public String getSubTitle() 
    {
        return subTitle;
    }
    public void setMoviesId(Long moviesId) 
    {
        this.moviesId = moviesId;
    }

    public Long getMoviesId() 
    {
        return moviesId;
    }
    public void setSeq(Integer seq)
    {
        this.seq = seq;
    }

    public Integer getSeq()
    {
        return seq;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("subTitle", getSubTitle())
            .append("moviesId", getMoviesId())
            .append("seq", getSeq())
            .append("url", getUrl())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
