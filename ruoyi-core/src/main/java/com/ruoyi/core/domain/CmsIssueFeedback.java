package com.ruoyi.core.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 手机助手问题反馈对象 cms_issue_feedback
 *
 * @author zqc
 * @date 2023-11-27
 */
public class CmsIssueFeedback extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 问题反馈表流水ID */
    private Long id;

    /** 手机助手用户ID */
    @Excel(name = "手机助手用户ID")
    private Long uid;

    /** 问题反馈内容 */
    @Excel(name = "问题反馈内容")
    private String issueContent;
    @Excel(name = "联系方式")
    private String contactInfo;

    /** 问题反馈图片 */
    @Excel(name = "问题反馈图片")
    private String issueImage;

    /** 审核状态 0 待审批 1 审核通过 2审核不通过  */
    @Excel(name = "审核状态 0 待审批 1 审核通过 2审核不通过 ")
    private String approvalStatus;

    /** 显示标识 0显示2删除 */
    private String delFlag;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUid(Long uid)
    {
        this.uid = uid;
    }

    public Long getUid()
    {
        return uid;
    }
    public void setIssueContent(String issueContent)
    {
        this.issueContent = issueContent;
    }

    public String getIssueContent()
    {
        return issueContent;
    }
    public void setIssueImage(String issueImage)
    {
        this.issueImage = issueImage;
    }

    public String getIssueImage()
    {
        return issueImage;
    }
    public void setApprovalStatus(String approvalStatus)
    {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovalStatus()
    {
        return approvalStatus;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("uid", getUid())
            .append("issueContent", getIssueContent())
            .append("issueImage", getIssueImage())
            .append("approvalStatus", getApprovalStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
