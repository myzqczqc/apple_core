package com.ruoyi.core.domain.vo;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.core.domain.CmsEpisode;
import com.ruoyi.core.domain.CmsMovies;
import com.ruoyi.core.domain.CmsMoviesType;
import com.ruoyi.core.domain.CmsMtype;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Arrays;
import java.util.List;

/**
 * 影视数据信息对象 cms_glasses_movies
 * 
 * @author ruoyi
 * @date 2023-08-25
 */
@ApiModel(value = "CmsMoviesVo", description = "影视剧信息")
public class CmsMoviesVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 影视名字 */
    @Excel(name = "影视名字")
    @ApiModelProperty("影视名字")
    private String title;

    /** 副标题 */
    @Excel(name = "副标题")
    @ApiModelProperty("副标题")
    private String subTitle;

    /** 类型 */
    @Excel(name = "类型")
    private Integer[] type;

    /** 封面图 */
    @Excel(name = "封面图")
    @ApiModelProperty("封面图")
    private String image;

    /** 剧集的集数 */
    @Excel(name = "剧集的集数")
    private Integer episodeCount;
    private Integer typeId;
    private Integer languageId ;

    /** 简介 */
    @Excel(name = "简介")
    @ApiModelProperty("简介")
    private List<IntroductionVo> introduction;

    @ApiModelProperty("单剧集信息")
    private List<CmsEpisode> episodeList;
    @ApiModelProperty("影视剧标签")
    private List<CmsMtype> mtypeList;

    public List<CmsMtype> getMtypeList() {
        return mtypeList;
    }

    public void setMtypeList(List<CmsMtype> mtypeList) {
        this.mtypeList = mtypeList;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public List<CmsEpisode> getEpisodeList() {
        return episodeList;
    }

    public void setEpisodeList(List<CmsEpisode> episodeList) {
        this.episodeList = episodeList;
    }
    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setSubTitle(String subTitle) 
    {
        this.subTitle = subTitle;
    }

    public String getSubTitle() 
    {
        return subTitle;
    }
    public void setImage(String image)
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }
    public void setEpisodeCount(Integer episodeCount) 
    {
        this.episodeCount = episodeCount;
    }

    public Integer getEpisodeCount() 
    {
        return episodeCount;
    }

    public Integer[] getType() {
        return type;
    }

    public void setType(Integer[] type) {
        this.type = type;
    }


    public List<IntroductionVo> getIntroduction() {
        return introduction;
    }

    public void setIntroduction(List<IntroductionVo> introduction) {
        this.introduction = introduction;
    }

//    public CmsMovies vo2Movies(){
//        CmsMovies cmsMovies = new CmsMovies();
//        BeanUtils.copyBeanProp(cmsMovies,this);
//        //影视剧描述
//        cmsMovies.setIntroduction(JSON.toJSONString(this.introduction));
//        //类型
//        Arrays.stream(this.type).map(a->{
//            CmsMoviesType temp = new CmsMoviesType();
//            temp.setMoviesId(t);
//        })
//        cmsMovies.setType(JSON.toJSONString(this.type));
//
//        return cmsMovies;
//    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("subTitle", getSubTitle())
            .append("type", getType())
            .append("image", getImage())
            .append("episodeCount", getEpisodeCount())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
