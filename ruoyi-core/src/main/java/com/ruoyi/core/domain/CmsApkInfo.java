package com.ruoyi.core.domain;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.core.domain.vo.CmsApkInfoVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * apk信息对象 cms_apk_info
 * 
 * @author zqc
 * @date 2023-11-06
 */
@ApiModel(value = "CmsApkInfo", description = "apkInfo信息")
public class CmsApkInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 名字 */
    @Excel(name = "名字")
    @ApiModelProperty("名字")
    private String name;

    /** 唯一标识 */
    @Excel(name = "唯一标识")
    @ApiModelProperty("packageName")
    private String packageName;

    /** icon */
    @Excel(name = "icon")
    @ApiModelProperty("icon")
    private String icon;
    @ApiModelProperty("3张背景图")
    private String image;

    /** 文件大小 */
    @Excel(name = "文件大小")
    @ApiModelProperty("文件大小")
    private Long length;

    /** 文件地址 */
    @Excel(name = "文件地址")
    @ApiModelProperty("文件地址")
    private String url;

    /** version_code */
    @Excel(name = "version_code")
    @ApiModelProperty("version_code")
    private String versionCode;

    /** version_name */
    @Excel(name = "version_name")
    @ApiModelProperty("version_name")
    private String versionName;

    /** 1：共有，0：私有 */
    @Excel(name = "1：共有，0：私有")
    private Integer typeId;
    @Excel(name = "1：游戏，2：应用")
    private Integer ap;
    @Excel(name = "1：游戏，2：应用的子分类")
    private Integer apType;
    private Integer languageId;

    @Excel(name = "apk简介")
    @ApiModelProperty("apk简介")
    private String introduction;

    @Excel(name = "slogan")
    @ApiModelProperty("apk slogan")
    private String slogan;

    public Integer getApType() {
        return apType;
    }

    public void setApType(Integer apType) {
        this.apType = apType;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public Integer getAp() {
        return ap;
    }

    public void setAp(Integer ap) {
        this.ap = ap;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPackageName(String packageName) 
    {
        this.packageName = packageName;
    }

    public String getPackageName() 
    {
        return packageName;
    }
    public void setIcon(String icon) 
    {
        this.icon = icon;
    }

    public String getIcon() 
    {
        return icon;
    }
    public void setLength(Long length) 
    {
        this.length = length;
    }

    public Long getLength() 
    {
        return length;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }
    public void setVersionCode(String versionCode) 
    {
        this.versionCode = versionCode;
    }

    public String getVersionCode() 
    {
        return versionCode;
    }
    public void setVersionName(String versionName) 
    {
        this.versionName = versionName;
    }

    public String getVersionName() 
    {
        return versionName;
    }
    public void setTypeId(Integer typeId) 
    {
        this.typeId = typeId;
    }

    public Integer getTypeId() 
    {
        return typeId;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public CmsApkInfoVo vo2CmsApkInfo(){
        CmsApkInfoVo cmsApkInfoVo = new CmsApkInfoVo();
        BeanUtils.copyBeanProp(cmsApkInfoVo,this);
        if(StringUtils.isNotEmpty(this.image)){
            cmsApkInfoVo.setImage(JSON.parseArray(this.image,String.class));
        }
        return cmsApkInfoVo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("packageName", getPackageName())
            .append("icon", getIcon())
            .append("length", getLength())
            .append("url", getUrl())
            .append("versionCode", getVersionCode())
            .append("versionName", getVersionName())
            .append("typeId", getTypeId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("remark", getRemark())
            .append("introduction", getIntroduction())
            .append("slogan", getSlogan())
            .toString();
    }
}
