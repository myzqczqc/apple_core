package com.ruoyi.core.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 渠道商管理对象 cms_channel
 *
 * @author zqc
 * @date 2023-11-08
 */
public class CmsChannel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 渠道ID */
    private Long id;

    /** 所属合作商ID */

    private Long partnerId;
    /** 渠道名称 */
    @Excel(name = "渠道名称")
    private String channelName;
    /** 合作商名称  **/
    @Excel(name = "所属合作商")
    private String partnerName;

    @Excel(name = "合作商SN")
    private String partnerSn;
    /** 渠道唯一码 SN */
    @Excel(name = "渠道SN")
    private String channelSn;
    private String userName;
    @Excel(name = "是否是主绑定对象")
    private String isBind;

    /** 渠道信息概况 */
    @Excel(name = "渠道信息概况")
    private String channelInfo;

    /** 显示标识  0显示 2删除 默认0显示 */
    private String delFlag;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setPartnerId(Long partnerId)
    {
        this.partnerId = partnerId;
    }

    public Long getPartnerId()
    {
        return partnerId;
    }
    public void setChannelSn(String channelSn)
    {
        this.channelSn = channelSn;
    }

    public String getChannelSn()
    {
        return channelSn;
    }
    public void setChannelName(String channelName)
    {
        this.channelName = channelName;
    }

    public String getChannelName()
    {
        return channelName;
    }
    public void setChannelInfo(String channelInfo)
    {
        this.channelInfo = channelInfo;
    }

    public String getChannelInfo()
    {
        return channelInfo;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    public String getPartnerSn() {
        return partnerSn;
    }

    public void setPartnerSn(String partnerSn) {
        this.partnerSn = partnerSn;
    }

    public String getIsBind() {
        return isBind;
    }

    public void setIsBind(String isBind) {
        this.isBind = isBind;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("partnerId", getPartnerId())
            .append("channelSn", getChannelSn())
            .append("channelName", getChannelName())
            .append("channelInfo", getChannelInfo())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("delFlag", getDelFlag())
            .append("remark", getRemark())
            .toString();
    }
}
