package com.ruoyi.core.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * tab的recommend信息对象 cms_recommend
 * 
 * @author zqc
 * @date 2023-11-08
 */
@ApiModel(value = "CmsRecommend", description = "推荐信息")
public class CmsRecommend extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** tab id */
    @Excel(name = "tab id")
    private Long tabId;

    /** 标题 */
    @Excel(name = "标题")
    @ApiModelProperty("标题")
    private String title;

    /** 副标题 */
    @Excel(name = "副标题")
    @ApiModelProperty("副标题")
    private String subTitle;

    /** 图片，背景图 */
    @Excel(name = "图片，背景图")
    @ApiModelProperty("图片，背景图")
    private String image;

    @Excel(name = "icon图标")
    @ApiModelProperty("icon图标")
    private String icon;

    /** 顺序号 */
    @Excel(name = "顺序号")
    private Long seq;

    /** 点击类型，直播:broadcast */
    @Excel(name = "点击类型，直播:broadcast")
    @ApiModelProperty("点击类型，直播:broadcast,影视剧：movies，apk:apk,")
    private String clickType;

    /** 点击对象id */
    @Excel(name = "点击对象id")
    private Long clickId;

    private Object clickInfo ;

    public Object getClickInfo() {
        return clickInfo;
    }

    public void setClickInfo(Object clickInfo) {
        this.clickInfo = clickInfo;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTabId(Long tabId) 
    {
        this.tabId = tabId;
    }

    public Long getTabId() 
    {
        return tabId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setSubTitle(String subTitle) 
    {
        this.subTitle = subTitle;
    }

    public String getSubTitle() 
    {
        return subTitle;
    }
    public void setImage(String image) 
    {
        this.image = image;
    }

    public String getImage() 
    {
        return image;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setSeq(Long seq)
    {
        this.seq = seq;
    }

    public Long getSeq() 
    {
        return seq;
    }
    public void setClickType(String clickType) 
    {
        this.clickType = clickType;
    }

    public String getClickType() 
    {
        return clickType;
    }
    public void setClickId(Long clickId) 
    {
        this.clickId = clickId;
    }

    public Long getClickId() 
    {
        return clickId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("tabId", getTabId())
            .append("title", getTitle())
            .append("subTitle", getSubTitle())
            .append("image", getImage())
            .append("icon", getIcon())
            .append("seq", getSeq())
            .append("clickType", getClickType())
            .append("clickId", getClickId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
