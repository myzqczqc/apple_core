package com.ruoyi.core.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 核心应用信息对象 cms_core
 * 
 * @author zqc
 * @date 2023-11-23
 */
public class CmsCore extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 副标题 */
    @Excel(name = "副标题")
    private String subtitle;

    /** 代码标志 */
    @Excel(name = "代码标志")
    private String core;

    private CmsCoreVersion cmsCoreVersion ;


    public CmsCoreVersion getCmsCoreVersion() {
        return cmsCoreVersion;
    }

    public void setCmsCoreVersion(CmsCoreVersion cmsCoreVersion) {
        this.cmsCoreVersion = cmsCoreVersion;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setSubtitle(String subtitle) 
    {
        this.subtitle = subtitle;
    }

    public String getSubtitle() 
    {
        return subtitle;
    }
    public void setCore(String core) 
    {
        this.core = core;
    }

    public String getCore() 
    {
        return core;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("subtitle", getSubtitle())
            .append("core", getCore())
            .toString();
    }
}
