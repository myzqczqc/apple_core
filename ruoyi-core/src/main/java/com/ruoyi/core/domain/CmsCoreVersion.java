package com.ruoyi.core.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * core应用历史版本信息对象 cms_core_version
 * 
 * @author zqc
 * @date 2023-11-23
 */
public class CmsCoreVersion extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** core应用类型 */
    @Excel(name = "core应用类型")
    private Long typeId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String installType;

    /** 名字 */
    @Excel(name = "名字")
    private String name;

    /** 唯一标识 */
    @Excel(name = "唯一标识")
    private String packageName;

    /** icon */
    @Excel(name = "icon")
    private String icon;

    /** 文件大小 */
    @Excel(name = "文件大小")
    private Long length;

    /** 文件地址 */
    @Excel(name = "文件地址")
    private String url;

    /** version_code */
    @Excel(name = "version_code")
    private Long versionCode;

    /** version_name */
    @Excel(name = "version_name")
    private String versionName;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTypeId(Long typeId) 
    {
        this.typeId = typeId;
    }

    public Long getTypeId() 
    {
        return typeId;
    }
    public void setInstallType(String installType) 
    {
        this.installType = installType;
    }

    public String getInstallType() 
    {
        return installType;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPackageName(String packageName) 
    {
        this.packageName = packageName;
    }

    public String getPackageName() 
    {
        return packageName;
    }
    public void setIcon(String icon) 
    {
        this.icon = icon;
    }

    public String getIcon() 
    {
        return icon;
    }
    public void setLength(Long length) 
    {
        this.length = length;
    }

    public Long getLength() 
    {
        return length;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }
    public void setVersionCode(Long versionCode) 
    {
        this.versionCode = versionCode;
    }

    public Long getVersionCode() 
    {
        return versionCode;
    }
    public void setVersionName(String versionName) 
    {
        this.versionName = versionName;
    }

    public String getVersionName() 
    {
        return versionName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("typeId", getTypeId())
            .append("installType", getInstallType())
            .append("name", getName())
            .append("packageName", getPackageName())
            .append("icon", getIcon())
            .append("length", getLength())
            .append("url", getUrl())
            .append("versionCode", getVersionCode())
            .append("versionName", getVersionName())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("remark", getRemark())
            .toString();
    }
}
