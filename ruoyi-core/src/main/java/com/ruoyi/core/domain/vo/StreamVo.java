package com.ruoyi.core.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 直播流的结构
 */
@ApiModel(value = "StreamVo", description = "直播流的数据信息")
public class StreamVo {

    @ApiModelProperty("直播流地址")
    private String url ;
    @ApiModelProperty("直播流名字")
    private String name ;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
