package com.ruoyi.api.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

/**
 * @author java_liuzhibo
 * @version 1.0
 * @description: 换绑手机号接收对象
 * @date 2023-11-23 0023 10:31:56
 */
public class AssistantUserUpdatePhoneBody {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long uid;
    private String oldPhone;
    private String oldValicode;
    private String newPhone;
    private String newValicode;


    public AssistantUserUpdatePhoneBody() {
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getOldPhone() {
        return oldPhone;
    }

    public void setOldPhone(String oldPhone) {
        this.oldPhone = oldPhone;
    }

    public String getOldValicode() {
        return oldValicode;
    }

    public void setOldValicode(String oldValicode) {
        this.oldValicode = oldValicode;
    }

    public String getNewPhone() {
        return newPhone;
    }

    public void setNewPhone(String newPhone) {
        this.newPhone = newPhone;
    }

    public String getNewValicode() {
        return newValicode;
    }

    public void setNewValicode(String newValicode) {
        this.newValicode = newValicode;
    }

    public AssistantUserUpdatePhoneBody(Long uid, String oldPhone, String oldValicode, String newPhone, String newValicode) {
        this.uid = uid;
        this.oldPhone = oldPhone;
        this.oldValicode = oldValicode;
        this.newPhone = newPhone;
        this.newValicode = newValicode;
    }
}
