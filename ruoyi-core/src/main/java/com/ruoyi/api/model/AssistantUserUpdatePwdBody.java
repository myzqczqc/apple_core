package com.ruoyi.api.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

/**
 * @author java_liuzhibo
 * @version 1.0
 * @description: TODO
 * @date 2023-11-23 0023 10:33:57
 */
public class AssistantUserUpdatePwdBody {
    private String phone;
    private String valicode;
    private String password;
    private String retryPassword;

    public AssistantUserUpdatePwdBody() {

    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getValicode() {
        return valicode;
    }

    public void setValicode(String valicode) {
        this.valicode = valicode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRetryPassword() {
        return retryPassword;
    }

    public void setRetryPassword(String retryPassword) {
        this.retryPassword = retryPassword;
    }

    public AssistantUserUpdatePwdBody(String phone, String valicode, String password, String retryPassword) {
        this.phone = phone;
        this.valicode = valicode;
        this.password = password;
        this.retryPassword = retryPassword;
    }
}
