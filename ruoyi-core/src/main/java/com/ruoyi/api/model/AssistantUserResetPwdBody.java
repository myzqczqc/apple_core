package com.ruoyi.api.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

/**
 * @author java_liuzhibo
 * @version 1.0
 * @description: TODO
 * @date 2023-11-23 0023 10:33:57
 */
public class AssistantUserResetPwdBody {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long uid;
    private String userName;
    private String oldPassword;
    private String newPassword;

    public AssistantUserResetPwdBody() {

    }

    public AssistantUserResetPwdBody(Long uid, String userName, String oldPassword, String newPassword) {
        this.uid = uid;
        this.userName = userName;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }
}
