package com.ruoyi.api.model;

/**
 * @author java_liuzhibo
 * @version 1.0
 * @description: TODO
 * @date 2023-11-23 0023 10:30:09
 */
public class AssistantUserLoginBody {
    private String userName;
    private String password;
    public AssistantUserLoginBody() {

    }

    public AssistantUserLoginBody(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
