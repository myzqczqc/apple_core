package com.ruoyi.api.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

/**
 * @author java_liuzhibo
 * @version 1.0
 * @description: TODO
 * @date 2023-11-23 0023 10:31:56
 */
public class AssistantUserIssueFeedbackBody {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long uid;
    private String issueContent;

    public AssistantUserIssueFeedbackBody() {
    }

    public AssistantUserIssueFeedbackBody(Long uid, String issueContent) {
        this.uid = uid;
        this.issueContent = issueContent;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getIssueContent() {
        return issueContent;
    }

    public void setIssueContent(String issueContent) {
        this.issueContent = issueContent;
    }
}
