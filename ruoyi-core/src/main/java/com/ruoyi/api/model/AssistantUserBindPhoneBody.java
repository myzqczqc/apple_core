package com.ruoyi.api.model;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

/**
 * @author java_liuzhibo
 * @version 1.0
 * @description: TODO
 * @date 2023-11-23 0023 10:31:56
 */
public class AssistantUserBindPhoneBody {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long uid;
    private String phone;
    private String valicode;

    private String isBind;

    public AssistantUserBindPhoneBody() {
    }

    public AssistantUserBindPhoneBody(Long uid, String phone, String valicode, String isBind) {
        this.uid = uid;
        this.phone = phone;
        this.valicode = valicode;
        this.isBind = isBind;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getValicode() {
        return valicode;
    }

    public void setValicode(String valicode) {
        this.valicode = valicode;
    }

    public String getIsBind() {
        return isBind;
    }

    public void setIsBind(String isBind) {
        this.isBind = isBind;
    }
}
