package com.ruoyi.api.constant;

public class RedisKey {

    //配置信息======================================================================================================
    //redis的存储时间， 分钟
    public static final Integer TIMEOUT = 10;

    //页签key
    public static String getTabKey(String type, String channelId) {
        return "api:tab:channel:" + type + ":" + channelId;
    }

    //banner  key
    public static String getBannerKey(Long tabId) {
        return "api:banner:id:" + tabId;
    }

    //推荐页 key
    public static String getRecommendKey(Long tabId) {
        return "api:recommend:id:" + tabId;
    }

    //app 子分类信息
    public static String getApListKey(Integer ap) {
        return "api:appInfo:ap:" + ap;
    }

    //app 子分类推荐信息
    public static String getAppListKey(Long apType) {
        return "api:appInfo:apType:" + apType;
    }

    //app 子分类推荐信息
    public static String getAppListKey(Long apType, String channelId) {
        return getAppListKey(apType) + ":channelId:" + channelId;
    }

    //app  子分类列表
    public static String getAppListKey(Long apType, String channelId, Integer start, Integer pageSize) {
        return getAppListKey(apType, channelId) + ":start" + start + ":pageSize" + pageSize;
    }

    //内容资源信息===================================================================================================

    //影视剧的key
    public static String getMoviesKey(Long id) {
        return "api:movies:id:" + id;
    }

    //某个影视剧的类型信息
    public static String getMoviesTypeKey(Long moviesId) {
        return "api:movies:type:id:" + moviesId;
    }

    //影视剧类型字典信息
    public static String getMTypeKey() {
        return "api:movies:type:list:id";
    }

    //直播流的key
    public static String getBroadcastKey(Long id) {
        return "api:broadcast:id:" + id;
    }

    //apk的key
    public static String getApkKey(Long id) {
        return "api:apk:id:" + id;
    }

    //核心应用配置
    public static String getCoreVersionKey(Integer typeId) {
        return "api:coreversion:id:" + typeId;
    }


    //cms用户的key=================================================================================================

    public static String getCmsChannelKey(String channelId) {
        return "api:cms:channel:id:" + channelId;
    }

    public static String getAssistantUserCodeKey(String phone) {
        return "assistantUser-" + phone;
    }
}
