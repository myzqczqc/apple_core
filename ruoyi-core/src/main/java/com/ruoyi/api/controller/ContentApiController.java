package com.ruoyi.api.controller;

import com.ruoyi.api.service.IContentApiService;
import com.ruoyi.api.service.ITabApiService;
import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.ApiResult;
import com.ruoyi.core.domain.CmsApkInfo;
import com.ruoyi.core.domain.CmsBroadcastInfo;
import com.ruoyi.core.domain.CmsMovies;
import com.ruoyi.core.domain.CmsTab;
import com.ruoyi.core.domain.vo.CmsApkInfoVo;
import com.ruoyi.core.domain.vo.CmsMoviesVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * apk信息Controller
 * 
 * @author ruoyi
 * @date 2023-08-11
 */
@Api(tags = "内容资源-接口")
@RestController
@RequestMapping("/content")
public class ContentApiController extends BaseController
{

    @Autowired
    private IContentApiService contentApiService ;


    @GetMapping("/movies")
    @ApiOperation("影视剧信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "影视剧id",   required = true,dataType = "Long", dataTypeClass = Long.class),
    })
    @Anonymous
    public ApiResult<CmsMoviesVo> getMovies(Long id){
        CmsMoviesVo moviesInfo = contentApiService.getMoviesInfo(id);
        return ApiResult.success(moviesInfo);
    }


    @GetMapping("/broadcast")
    @ApiOperation("直播流信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "直播流id",   required = true,dataType = "Long", dataTypeClass = Long.class),
    })
    @Anonymous
    public ApiResult<CmsBroadcastInfo> getBroadcast(Long id){
        CmsBroadcastInfo cmsBroadcastInfo = contentApiService.getCmsBroadcastInfo(id);
        return ApiResult.success(cmsBroadcastInfo);
    }


    @GetMapping("/apk")
    @ApiOperation("apk信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "apkId",   required = true,dataType = "Long", dataTypeClass = Long.class),
    })
    @Anonymous
    public ApiResult<CmsBroadcastInfo> getApk(Long id){
        CmsApkInfoVo cmsApkInfo = contentApiService.getCmsApkInfo(id);
        return ApiResult.success(cmsApkInfo);
    }

}
