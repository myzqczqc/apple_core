package com.ruoyi.api.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.PhoneUtil;
import cn.hutool.core.util.RandomUtil;
import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.api.model.*;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.MimeTypeUtils;
import com.ruoyi.core.domain.CmsAssistantUser;
import com.ruoyi.core.domain.CmsIssueFeedback;
import com.ruoyi.core.service.ICmsAssistantUserService;
import com.ruoyi.core.service.ICmsIssueFeedbackService;
import com.ruoyi.core.service.ICmsShortMessageService;
import io.swagger.annotations.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 手机助手用户接口Controller
 *
 * @author java_liuzhibo@163.com
 * @date 2023-11-21
 */
@Api(tags = "APP用户-接口")
@RestController
public class AssistantApiController extends BaseController {
    @Autowired
    private ICmsAssistantUserService cmsAssistantUserService;
    @Autowired
    private ICmsIssueFeedbackService issueFeedbackService;
    @Autowired
    private RedisCache redisCache;
    @Autowired
    private ICmsShortMessageService messageService;

    /**
     * 手机助手用户注册
     *
     * @param: userName 用户名/账号
     * @param: password 密码
     * @param: retryPassword 再次密码 （这个也可以放在客户端去处理）
     * @return:
     */
    @ApiOperation("用户注册")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "用户名称", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "password", value = "用户密码", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "retryPassword", value = "再次输入密码", dataType = "String", dataTypeClass = String.class),
    })
    @PostMapping("/assistantUserRegister")
    public AjaxResult register(@RequestBody AssistantUserEntity userEntity) {
        // 这一步可以放在客户端处理
        if (!userEntity.getPassword().equals(userEntity.getRetryPassword())) {
            return error("两次输入的密码不一致，请重新输入");
        }
        CmsAssistantUser user = new CmsAssistantUser();
        BeanUtils.copyProperties(userEntity, user);

        if (!cmsAssistantUserService.checkUserNameUnique(user)) {
            return error("用户'" + user.getUserName() + "'失败，账号已存在");
        }
        user.setCreateBy(user.getUserName());
        user.setPassword(SecurityUtils.encryptPassword(user.getPassword()));
        user.setId(IdUtil.getSnowflakeNextId());
        return toAjax(cmsAssistantUserService.insertCmsAssistantUser(user));
    }

    /**
     * 手机助手用户登录接口
     *
     * @return 结果
     * @param: userName 用户名/账号
     * @param: password 密码
     */
    @ApiOperation("用户登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "用户名称", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "password", value = "用户密码", dataType = "String", dataTypeClass = String.class),
    })
    @PostMapping("/assistantUserLogin")
    public AjaxResult login(@RequestBody AssistantUserLoginBody user) {
        return cmsAssistantUserService.login(user.getUserName(), user.getPassword());
    }

    /**
     * 手机助手用户手机号验证码登录接口
     *
     * @return 结果
     * @param: userName 用户名/账号
     * @param: password 密码
     */
    @ApiOperation("手机号+短信验证码登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "手机号码", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "valicode", value = "短信验证码", dataType = "String", dataTypeClass = String.class),
    })
    @PostMapping("/assistantUserPhoneLogin")
    public AjaxResult phoneLogin(@RequestBody AssistantUserBindPhoneBody user) {
        String redisKey = RedisKey.getAssistantUserCodeKey(user.getPhone());
        if (!redisCache.hasKey(redisKey)) {
            return error("请输入正确的验证码");
        } else {
            String existsValicode = redisCache.getCacheObject(redisKey);
            if (!existsValicode.equals(user.getValicode())) {
                return error("请输入正确的验证码");
            } else {
                CmsAssistantUser cmsAssistantUser = new CmsAssistantUser();
                cmsAssistantUser.setPhone(user.getPhone());
                cmsAssistantUser.setDelFlag(Constants.DELFLAG_VISIABLE);
                List<CmsAssistantUser> cmsAssistantUsers = cmsAssistantUserService.selectCmsAssistantUserList(cmsAssistantUser);
                if (CollUtil.isEmpty(cmsAssistantUsers)) {// 若系统中不存在此用户，则自动生成一个用户，用户名为“vr_8位随机数”
                    CmsAssistantUser insertUser = new CmsAssistantUser();
                    String randomUseNameAfter = RandomUtil.randomNumbers(8);
                    if (!redisCache.hasKey(StringUtils.join("vs_", randomUseNameAfter))) {
                        redisCache.setCacheObject(StringUtils.join("vs_", randomUseNameAfter), "1");
                        insertUser.setUserName(StringUtils.join("vr_", randomUseNameAfter));
                    } else {
                        String vv = RandomUtil.randomNumbers(8);
                        redisCache.setCacheObject(StringUtils.join("vs_", vv), "1");
                        insertUser.setUserName(StringUtils.join("vr_", vv));
                    }
                    insertUser.setPhone(user.getPhone());
                    insertUser.setCreateBy("admin");
                    insertUser.setId(IdUtil.getSnowflakeNextId());
                    cmsAssistantUserService.insertCmsAssistantUser(insertUser);
                    return cmsAssistantUserService.login(insertUser.getUserName(), "", "1");
                } else {
                    return cmsAssistantUserService.login(cmsAssistantUsers.get(0).getUserName(), cmsAssistantUsers.get(0).getPassword(), "1");
                }
            }
        }
    }

    /**
     * 发送短信验证码接口
     *
     * @param: userName 用户名/账号
     * @param: oldPassword 旧密码
     * @param: newPassword 新密码
     * @return:
     */
    @ApiOperation("发送短信验证码接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "手机号", dataType = "String", paramType = "query")
    })
    @PostMapping("/assistantUserSendCode")
    public AjaxResult sendCode(@RequestBody AssistantUserBindPhoneBody phoneBody) {
        String code = RandomUtil.randomNumbers(6);
        boolean result = messageService.sendShortMessage(phoneBody.getPhone(), code);
        if (result) {
            String redisKey = RedisKey.getAssistantUserCodeKey(phoneBody.getPhone());
            redisCache.setCacheObject(StringUtils.join(redisKey), code, 1000 * 60 * 5, TimeUnit.MILLISECONDS);
        }
        return AjaxResult.success();
    }

    /**
     * 修改密码接口
     *
     * @param: userName 用户名/账号
     * @param: oldPassword 旧密码
     * @param: newPassword 新密码
     * @return:
     */
    @ApiOperation("绑定手机号/解绑手机号")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid", value = "用户ID", dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "valicode", value = "短信验证码", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "isBind", value = "绑定(1)/解绑(0)", dataType = "String", paramType = "query")
    })
    @PostMapping("/assistantUserBindPhone")
    public AjaxResult bindPhone(@RequestBody AssistantUserBindPhoneBody bindPhoneBody) {
        if (!PhoneUtil.isPhone(bindPhoneBody.getPhone())) {
            return error("请输入有效手机号码");
        }
        CmsAssistantUser cmsAssistantUser = cmsAssistantUserService.selectCmsAssistantUserById(bindPhoneBody.getUid());
        if (ObjectUtil.isNotNull(cmsAssistantUser)) {
            String redisKey = RedisKey.getAssistantUserCodeKey(bindPhoneBody.getPhone());
            if (redisCache.hasKey(redisKey)) {
                String storeValiCode = redisCache.getCacheObject(redisKey);
                if (!storeValiCode.equals(bindPhoneBody.getValicode())) {
                    return error("请输入正确的短信验证码");
                }
                // 解绑
                if ("0".equals(bindPhoneBody.getIsBind())) {
                    if (!cmsAssistantUser.getPhone().equals(bindPhoneBody.getPhone())) {
                        return error("无法解绑！此手机号不是已绑定手机号码");
                    }
                    cmsAssistantUserService.unbindCmsAssistantPhone(cmsAssistantUser);
                } else {// 绑定
                    // 1. 本账号还未绑定手机
                    if (StringUtils.isBlank(cmsAssistantUser.getPhone())) {
                        // 2. 检查手机号是否已经被其他账号绑定
                        CmsAssistantUser userByPhone = cmsAssistantUserService.selectCmsAssistantUserByPhone(bindPhoneBody.getPhone());
                        if (ObjectUtil.isNotNull(userByPhone)) {
                            return error("此手机号已被绑定，请更换手机号");
                        }
                    } else {
                        // 本账号已经绑定手机，判断已绑定手机，是否与要绑定的手机号 是同一个
                        // 如果是同一个，则提示“您已绑定此手机号，无需重复绑定”
                        if (cmsAssistantUser.getPhone().equals(bindPhoneBody.getPhone())) {
                            return error("此账号已绑定此手机号，无需重复绑定");
                        } else {
                            return error("此账号已绑定其他手机号，请先解绑再重新绑定");
                        }
                    }
                    cmsAssistantUser.setPhone(bindPhoneBody.getPhone());
                    cmsAssistantUserService.updateCmsAssistantUser(cmsAssistantUser);
                }
                return success();
            } else {
                return error("请输入正确的短信验证码");
            }
        } else {
            return error("用户不存在");
        }
    }

    @ApiOperation("换绑手机号")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "oldPhone", value = "旧手机号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "oldValicode", value = "旧手机号短信验证码", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "newPhone", value = "新手机号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "newValicode", value = "新手机号短信验证码", dataType = "String", paramType = "query"),
    })
    @PostMapping("/assistantUserUpdatePhone")
    public AjaxResult updatePhone(@RequestBody AssistantUserUpdatePhoneBody bindPhoneBody) {
        String redisKeyOld = RedisKey.getAssistantUserCodeKey(bindPhoneBody.getOldPhone());
        if (!redisCache.hasKey(redisKeyOld)) {
            return error("原手机号码错误/原手机号码验证码输入不正确");
        }
        String cacheOldValicode = (String) redisCache.getCacheObject(redisKeyOld);
        if (!cacheOldValicode.equals(bindPhoneBody.getOldValicode())) {
            return error("原手机号码验证码输入不正确");
        }
        String redisKeyNew = RedisKey.getAssistantUserCodeKey(bindPhoneBody.getNewPhone());
        if (!redisCache.hasKey(redisKeyNew)) {
            return error("新手机号码错误/新手机号码验证码输入不正确");
        }
        String cacheNewValicode = (String) redisCache.getCacheObject(redisKeyNew);
        if (!cacheNewValicode.equals(bindPhoneBody.getNewValicode())) {
            return error("新手机号码验证码输入不正确");
        }
        CmsAssistantUser phoneUser = new CmsAssistantUser();
        phoneUser.setPhone(bindPhoneBody.getOldPhone());
        phoneUser.setDelFlag(Constants.DELFLAG_VISIABLE);
        List<CmsAssistantUser> users = cmsAssistantUserService.selectCmsAssistantUserList(phoneUser);
        // 查询原手机号码用户信息，判断用户是否存在，不存在，则说明原手机号码不正确
        if (CollUtil.isEmpty(users)) {
            return error("原手机号码输入不正确");
        } else {
            // 原手机号码查询到用户，
            // 然后查询新手机号是否已绑定用户，若已绑定，返回错误信息
            CmsAssistantUser queryUser = new CmsAssistantUser();
            queryUser.setPhone(bindPhoneBody.getNewPhone());
            queryUser.setDelFlag(Constants.DELFLAG_VISIABLE);
            List<CmsAssistantUser> queryUsers = cmsAssistantUserService.selectCmsAssistantUserList(queryUser);
            if (CollUtil.isEmpty(queryUsers)) {
                CmsAssistantUser user = users.get(0);
                user.setPhone(bindPhoneBody.getNewPhone());
                cmsAssistantUserService.updateCmsAssistantUser(user);
                return success(user);
            } else {
                return error("新手机号码已绑定其他用户");
            }
        }
    }

    /**
     * 获取手机助手用户详细信息
     */
    @ApiOperation("获取用户详情")
    @ApiImplicitParam(name = "uid", value = "用户ID", required = true, dataType = "Long")
    @GetMapping("/assistantUserInfo")
    public AjaxResult getInfo(Long uid) {
        return success(cmsAssistantUserService.selectCmsAssistantUserById(uid));
    }

    /**
     * 修改密码接口
     *
     * @param: userName 用户名/账号
     * @param: oldPassword 旧密码
     * @param: newPassword 新密码
     * @return:
     */
    @ApiOperation("修改密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "账号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "oldPassword", value = "旧密码", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "newPassword", value = "新密码", dataType = "String", paramType = "query")
    })
    @PostMapping("/updatePassword")
    public AjaxResult updatePassword(@RequestBody AssistantUserResetPwdBody resetPwdBody) {
        String password = "";
        CmsAssistantUser assistantUser = new CmsAssistantUser();
        assistantUser.setUserName(resetPwdBody.getUserName());
        List<CmsAssistantUser> cmsAssistantUsers = cmsAssistantUserService.selectCmsAssistantUserList(assistantUser);
        if (CollUtil.isNotEmpty(cmsAssistantUsers)) {
            password = cmsAssistantUsers.get(0).getPassword();
        }
        // TODO: 2023-11-21 暂时未处理token问题
        if (!SecurityUtils.matchesPassword(resetPwdBody.getOldPassword(), password)) {
            return error("修改密码失败，旧密码错误");
        }
        if (SecurityUtils.matchesPassword(resetPwdBody.getNewPassword(), password)) {
            return error("新密码不能与旧密码相同");
        }
        if (cmsAssistantUserService.updateUserPwd(resetPwdBody.getUserName(), SecurityUtils.encryptPassword(resetPwdBody.getNewPassword())) > 0) {
            return success();
        }
        return error("修改密码异常，请联系管理员");
    }

    /**
     * 修改密码接口
     *
     * @param: userName 用户名/账号
     * @param: oldPassword 旧密码
     * @param: newPassword 新密码
     * @return:
     */
    @ApiOperation("通过手机号码和短信验证码修改密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "手机号码", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "valicode", value = "手机短信验证码", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "password", value = "密码", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "retryPassword", value = "再次输入密码", dataType = "String", paramType = "query")
    })
    @PostMapping("/updatePasswordByPhone")
    public AjaxResult updatePasswordByPhone(@RequestBody AssistantUserUpdatePwdBody resetPwdBody) {
        String redisKey = RedisKey.getAssistantUserCodeKey(resetPwdBody.getPhone());
        if (!redisCache.hasKey(redisKey)) {
            return error("手机号码验证码输入不正确");
        }
        String cacheValicode = redisCache.getCacheObject(redisKey);
        if (!cacheValicode.equals(resetPwdBody.getValicode())) {
            return error("手机号码验证码输入不正确");
        }
        if (!resetPwdBody.getPassword().equals(resetPwdBody.getRetryPassword())) {
            return error("两次输入的密码不一致");
        }
        CmsAssistantUser queryUser = new CmsAssistantUser();
        queryUser.setPhone(resetPwdBody.getPhone());
        queryUser.setDelFlag(Constants.DELFLAG_VISIABLE);
        List<CmsAssistantUser> users = cmsAssistantUserService.selectCmsAssistantUserList(queryUser);
        if (CollUtil.isEmpty(users)) {
            return error("手机号码不正确");
        } else {
            CmsAssistantUser user = users.get(0);
            String existsPassword = user.getPassword();
            // TODO: 2023-11-21 暂时未处理token问题
            if (SecurityUtils.matchesPassword(resetPwdBody.getPassword(), existsPassword)) {
                return error("新密码不能与旧密码相同");
            }
            if (cmsAssistantUserService.updateUserPwd(user.getUserName(), SecurityUtils.encryptPassword(resetPwdBody.getPassword())) > 0) {
                user.setPassword(SecurityUtils.encryptPassword(resetPwdBody.getPassword()));
                return success(user);
            } else {
                return error("修改密码异常，请联系管理员");
            }
        }
    }

    /**
     * 修改密码接口
     *
     * @param: userName 用户名/账号
     * @param: oldPassword 旧密码
     * @param: newPassword 新密码
     * @return:
     */
    @ApiOperation("设置密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid", value = "用户id", dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "newPassword", value = "密码", dataType = "String", paramType = "query")
    })
    @PostMapping("/setPassword")
    public AjaxResult setPassword(@RequestBody AssistantUserResetPwdBody resetPwdBody) {
        CmsAssistantUser assistantUser = cmsAssistantUserService.selectCmsAssistantUserById(resetPwdBody.getUid());
        if (cmsAssistantUserService.updateUserPwd(assistantUser.getUserName(), SecurityUtils.encryptPassword(resetPwdBody.getNewPassword())) > 0) {
            return success();
        }
        return error("设置密码异常，请联系管理员");
    }

    /**
     * 头像上传
     */
    @ApiOperation("上传头像")
/*    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName",value = "账号" , dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "file", paramType="query", value = "头像文件", dataType="file", required = true),
    })*/
    @PostMapping(value = "/uploadAvatar")
    public AjaxResult avatar(String userName, @RequestParam("file") MultipartFile file) throws Exception {
        if (!file.isEmpty()) {
            CmsAssistantUser cmsAssistantUser = new CmsAssistantUser();
            cmsAssistantUser.setUserName(userName);
            List<CmsAssistantUser> cmsAssistantUsers = cmsAssistantUserService.selectCmsAssistantUserList(cmsAssistantUser);
            String avatar = FileUploadUtils.upload(RuoYiConfig.getAvatarPath(), file, MimeTypeUtils.IMAGE_EXTENSION);
            if (cmsAssistantUserService.updateUserAvatar(cmsAssistantUsers.get(0).getUserName(), avatar)) {
                AjaxResult ajax = AjaxResult.success();
                ajax.put("imgUrl", avatar);
                return ajax;
            }
        }
        return error("上传图片异常，请联系管理员");
    }

    @ApiOperation("问题反馈")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid", value = "用户id", dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "issueContent", value = "反馈内容", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "contactInfo", value = "联系方式", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "file", paramType = "query", value = "反馈图片", dataType = "file", required = true),
    })
    @PostMapping(value = "/issuesFeedback")
    public AjaxResult issuesFeedback(Long uid, String issueContent, String contactInfo, @RequestParam("file") MultipartFile file) throws Exception {
        if (!file.isEmpty()) {
            String issueImage = FileUploadUtils.upload(RuoYiConfig.getAvatarPath(), file, MimeTypeUtils.IMAGE_EXTENSION);
            CmsIssueFeedback issueFeedback = new CmsIssueFeedback();
            issueFeedback.setUid(uid);
            issueFeedback.setIssueImage(issueImage);
            issueFeedback.setIssueContent(issueContent);
            issueFeedback.setContactInfo(contactInfo);
            issueFeedback.setDelFlag(Constants.DELFLAG_VISIABLE);
            issueFeedback.setApprovalStatus(Constants.DELFLAG_VISIABLE);
            issueFeedbackService.insertCmsIssueFeedback(issueFeedback);
            AjaxResult ajax = AjaxResult.success();
            ajax.put("issueFeedback", issueFeedback);
            return ajax;
        }
        return error("问题反馈图片异常，请联系管理员");
    }
}

@ApiModel(value = "AssistantUserEntity", description = "用户实体")
class AssistantUserEntity {
    @ApiModelProperty("用户名")
    private String userName;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("再次输入密码")
    private String retryPassword;

    public AssistantUserEntity() {

    }

    public AssistantUserEntity(String userName, String password, String retryPassword) {
        this.userName = userName;
        this.password = password;
        this.retryPassword = retryPassword;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRetryPassword() {
        return retryPassword;
    }

    public void setRetryPassword(String retryPassword) {
        this.retryPassword = retryPassword;
    }

}
