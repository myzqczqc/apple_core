package com.ruoyi.api.controller;

import com.ruoyi.api.service.IBannerApiService;
import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.ApiResult;
import com.ruoyi.core.domain.CmsBanner;
import com.ruoyi.core.domain.CmsTab;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * apk信息Controller
 * 
 * @author ruoyi
 * @date 2023-08-11
 */
@Api(tags = "页签-banner接口")
@RestController
@RequestMapping("/banner")
public class BannerApiController extends BaseController
{

    @Autowired
    private IBannerApiService bannerApiService ;


    @GetMapping("/list")
    @ApiOperation("banner的信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tabId", value = "页签id",   required = true,dataType = "Long", dataTypeClass = Long.class),
    })
    @Anonymous
    public ApiResult<List<CmsTab>> getTabList(Long tabId){
        List<CmsBanner> bannerList = bannerApiService.getBannerList(tabId);
        return ApiResult.success(bannerList);
    }
}
