package com.ruoyi.api.controller;

import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.api.service.IAppApiService;
import com.ruoyi.api.service.IAppCoreApiService;
import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.ApiResult;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.core.domain.CmsAppType;
import com.ruoyi.core.domain.CmsCoreVersion;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * apk信息Controller
 * 
 * @author ruoyi
 * @date 2023-08-11
 */
@Api(tags = "核心应用-接口")
@RestController
@RequestMapping("/core")
public class AppCoreApiController extends BaseController
{

    @Autowired
    private IAppCoreApiService appCoreApiService ;

    @Autowired
    private RedisCache redisCache;

    @GetMapping("/appleCoreUpdate")
    @ApiOperation("核心应用")
    @Anonymous
    public ApiResult<List<CmsCoreVersion>> appleCoreUpdate(){
        List<CmsCoreVersion> result = new ArrayList<>();
        for (int i = 1; i <=3 ; i++) {
            CmsCoreVersion coreVersion = appCoreApiService.getCoreVersion(i);
            result.add(coreVersion);
        }
        // VR助手服务
        CmsCoreVersion coreVersion = appCoreApiService.getCoreVersion(5);
        result.add(coreVersion);
        return ApiResult.success(result);
    }


    @GetMapping("/mobileUpdate")
    @ApiOperation("手机助手升级")
    @Anonymous
    public ApiResult<CmsCoreVersion> mobileUpdate(){
        CmsCoreVersion coreVersion = appCoreApiService.getCoreVersion(4);
        return ApiResult.success(coreVersion);
    }

    /**
     * 手动清除核心应用缓存
     * @return
     */
    /*@Anonymous
    @GetMapping("/removeRedis")
    public ApiResult removeRedis(){
        for (int i = 1; i <= 5 ; i++) {
            String coreVersion = RedisKey.getCoreVersionKey(i);
            redisCache.deleteObject(coreVersion);
        }
        return ApiResult.success();
    }*/

}
