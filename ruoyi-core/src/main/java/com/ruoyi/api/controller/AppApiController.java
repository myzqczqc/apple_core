package com.ruoyi.api.controller;

import com.ruoyi.api.service.IAppApiService;
import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.ApiResult;
import com.ruoyi.core.domain.CmsApkInfo;
import com.ruoyi.core.domain.CmsAppType;
import com.ruoyi.core.domain.CmsChannel;
import com.ruoyi.core.domain.vo.CmsApkInfoVo;
import com.ruoyi.core.service.ICmsChannelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * apk信息Controller
 *
 * @author ruoyi
 * @date 2023-08-11
 */
@Api(tags = "应用、游戏-接口")
@RestController
@RequestMapping("/app")
public class AppApiController extends BaseController {

    @Autowired
    private IAppApiService appApiService;

    @Autowired
    private ICmsChannelService channelService;


    @GetMapping("/recommed")
    @ApiOperation("推荐信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ap", value = "类型id:(1:游戏，2：应用)", required = true, dataType = "Integer", dataTypeClass = Integer.class),
            @ApiImplicitParam(name = "channel", value = "渠道号", dataType = "String", paramType = "header"),
    })
    @Anonymous
    public ApiResult<List<CmsAppType>> getTabList(Integer ap) {
        String channelId = getChannelId();
        List<CmsAppType> apList = appApiService.getApList(channelId, ap);
        return ApiResult.success(apList);
    }

    @GetMapping("/apTypelist")
    @ApiOperation("根据子分类获取")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "apType", value = "子分类id", required = true, dataType = "Long", dataTypeClass = Long.class),
            @ApiImplicitParam(name = "pageNo", value = "页码，从1开始，每页30个", required = true, dataType = "Integer", dataTypeClass = Integer.class),
            @ApiImplicitParam(name = "channel", value = "渠道号", dataType = "String", paramType = "header"),
    })
    @Anonymous
    public ApiResult<List<CmsApkInfoVo>> apTypelist(Long apType, Integer pageNo) {
        String channelId = getChannelId();
        Integer pageSize = 30;
        Integer start = (pageNo - 1) * pageSize;
        List<CmsApkInfo> cmsApkInfos = appApiService.getApkListByApType(channelId, apType, start, pageSize);
        List<CmsApkInfoVo> infoVoList = cmsApkInfos.stream().map(CmsApkInfo::vo2CmsApkInfo).collect(Collectors.toList());
        return ApiResult.success(infoVoList);
    }


    @GetMapping("/search")
    @ApiOperation("app搜索")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyWord", value = "游戏名字", required = true, dataType = "String", dataTypeClass = String.class),
    })
    @Anonymous
    public ApiResult<List<CmsApkInfoVo>> searchAppList(String keyWord) {
        List<CmsApkInfo> cmsApkInfos = appApiService.searchAppList(keyWord);
        List<CmsApkInfoVo> infoVoList = cmsApkInfos.stream().map(CmsApkInfo::vo2CmsApkInfo).collect(Collectors.toList());
        return ApiResult.success(infoVoList);
    }

    /**
     * 检查渠道号，如果渠道不存在，则获取默认渠道数据
     * @return
     */
    private String getChannelId() {
        String channelId = getRequestHeaderChennlId();
        if (UserConstants.CHANNEL_ID.equals(channelId)) {
            return channelId;
        }
        CmsChannel channel = channelService.selectCmsChannelBySn(channelId);
        if (channel == null) {
            return UserConstants.CHANNEL_ID;
        }
        return channelId;
    }

}
