package com.ruoyi.api.controller;

import com.ruoyi.api.service.IBannerApiService;
import com.ruoyi.api.service.IRecommendApiService;
import com.ruoyi.api.service.ITabApiService;
import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.ApiResult;
import com.ruoyi.core.domain.CmsBanner;
import com.ruoyi.core.domain.CmsChannel;
import com.ruoyi.core.domain.CmsRecommend;
import com.ruoyi.core.domain.CmsTab;
import com.ruoyi.core.enums.ClickTypeEnums;
import com.ruoyi.core.service.ICmsChannelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * apk信息Controller
 *
 * @author ruoyi
 * @date 2023-08-11
 */
@Api(tags = "应用商店推荐-接口")
@RestController
@RequestMapping("/appStore")
public class AppStoreApiController extends BaseController {

    @Autowired
    private ITabApiService tabApiService;
    @Autowired
    private IRecommendApiService recommendApiService;
    @Autowired
    private IBannerApiService bannerApiService;
    @Autowired
    private ICmsChannelService channelService;


    @GetMapping("/banner")
    @ApiOperation("banner信息")
    @Anonymous
    @ApiImplicitParams({
            @ApiImplicitParam(name = "channel", value = "渠道号", dataType = "String", paramType = "header"),
    })
    public ApiResult<List<CmsBanner>> getBannerList() {
        //页签信息
        String type = ClickTypeEnums.APK.getName();
        String channelId = getChannelId();
        List<CmsTab> tabList = tabApiService.getTabList(channelId, type);
        Long id = tabList.get(0).getId();

        //banner信息
        List<CmsBanner> bannerList = bannerApiService.getBannerList(id);

        return ApiResult.success(bannerList);
    }


    @GetMapping("/recommend")
    @ApiOperation("推荐信息")
    @Anonymous
    @ApiImplicitParams({
            @ApiImplicitParam(name = "channel", value = "渠道号", dataType = "String", paramType = "header"),
    })
    public ApiResult<List<CmsBanner>> getTabList() {
        //页签信息
        String type = ClickTypeEnums.APK.getName();
        String channelId = getChannelId();
        List<CmsTab> tabList = tabApiService.getTabList(channelId, type);
        Long id = tabList.get(0).getId();

        //推荐信息信息
        List<CmsRecommend> recommendList = recommendApiService.getRecommendList(id);

        return ApiResult.success(recommendList);
    }

    private String getChannelId() {
        String channelId = getRequestHeaderChennlId();
        CmsChannel channel = channelService.selectCmsChannelBySn(channelId);
        if (channel == null) {
            return UserConstants.CHANNEL_ID;
        }
        return channelId;
    }

}
