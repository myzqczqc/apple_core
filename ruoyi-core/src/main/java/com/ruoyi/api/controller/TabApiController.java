package com.ruoyi.api.controller;

import com.ruoyi.api.service.ITabApiService;
import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.ApiResult;
import com.ruoyi.core.domain.CmsChannel;
import com.ruoyi.core.domain.CmsTab;
import com.ruoyi.core.service.ICmsChannelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * apk信息Controller
 *
 * @author ruoyi
 * @date 2023-08-11
 */
@Api(tags = "页签-接口")
@RestController
@RequestMapping("/tab")
public class TabApiController extends BaseController {

    @Autowired
    private ITabApiService tabApiService;

    @Autowired
    private ICmsChannelService channelService;

    @GetMapping("/list")
    @ApiOperation("页签的列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "(broadcast:直播流)（movies：影视剧）", required = true, dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "channel", value = "渠道号", dataType = "String", paramType = "header"),
    })
    @Anonymous
    public ApiResult<List<CmsTab>> getTabList(String type) {
        String channelId = getChannelId();
        List<CmsTab> tabList = tabApiService.getTabList(channelId, type);
        return ApiResult.success(tabList);
    }

    private String getChannelId() {
        String channelId = getRequestHeaderChennlId();
        CmsChannel channel = channelService.selectCmsChannelBySn(channelId);
        if (channel == null) {
            return UserConstants.CHANNEL_ID;
        }
        return channelId;
    }

}
