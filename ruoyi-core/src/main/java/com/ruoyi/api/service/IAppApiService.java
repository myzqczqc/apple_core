package com.ruoyi.api.service;

import com.ruoyi.core.domain.CmsApkInfo;
import com.ruoyi.core.domain.CmsAppType;
import com.ruoyi.core.domain.CmsTab;
import com.ruoyi.core.domain.vo.CmsApkInfoVo;

import java.util.List;

public interface IAppApiService {

    /**
     * 获取子分类信息
     * @param channelId
     * @param ap
     * @return
     */
    List<CmsAppType> getApList(String channelId, Integer ap);

    /**
     * 子分类配置的数据信息
     * @param channelId
     * @param apType
     * @return
     */
    List<CmsApkInfoVo> getAppList(String channelId, Long apType);


    /**
     * 根据子分类查询apk信息
     * @param channelId
     * @param apType
     * @return
     */
    List<CmsApkInfo> getApkListByApType(String channelId,Long apType,Integer start,Integer pageSize );

    /**
     * 搜索接口
     * @param keyWord
     * @return
     */
    List<CmsApkInfo> searchAppList(String keyWord);

}
