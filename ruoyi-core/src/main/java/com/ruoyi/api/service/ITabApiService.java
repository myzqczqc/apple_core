package com.ruoyi.api.service;

import com.ruoyi.core.domain.CmsTab;

import java.util.List;

public interface ITabApiService {

    /**
     * 获取页签，   影视剧，直播流
     * @param channelId
     * @param type
     * @return
     */
    List<CmsTab> getTabList(String channelId,String type);


}
