package com.ruoyi.api.service.impl;

import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.api.service.ITabApiService;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.core.domain.CmsTab;
import com.ruoyi.core.mapper.CmsTabMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class TabApiServiceImpl implements ITabApiService {

    @Autowired
    private CmsTabMapper cmsTabMapper ;

    @Autowired
    private RedisCache redisCache;


    @Override
    public List<CmsTab> getTabList(String channelId, String type) {
        String tabKey = RedisKey.getTabKey(type,channelId);
        List<CmsTab> cacheList = redisCache.getCacheList(tabKey);
        if(cacheList.isEmpty()){
            CmsTab cmsTab = new CmsTab();
            cmsTab.setType(type);
            cmsTab.setChannelId(channelId);
            cacheList = cmsTabMapper.selectCmsTabList(cmsTab);
            if(cacheList.isEmpty()){
                //如果数据库中没有配置数据，则获取默认的数据
                cacheList = getDefaultTabList(type);
            }
            if( ! cacheList.isEmpty()){
                //放入自己的缓存，
                redisCache.setCacheList(tabKey , cacheList);
                redisCache.expire(tabKey,RedisKey.TIMEOUT, TimeUnit.MINUTES);
            }
        }
        return cacheList;
    }
    /**
     * 获取默认的数据信息（如果某个渠道没有配置数据，则返回默认的渠道的配置信息）
     * @param type
     * @return
     */
    public List<CmsTab> getDefaultTabList(String type) {
        String channelId = UserConstants.CHANNEL_ID;
        String tabKey = RedisKey.getTabKey(type, channelId);
        List<CmsTab> cacheList = redisCache.getCacheList(tabKey);
        if(cacheList.isEmpty()){
            CmsTab cmsTab = new CmsTab();
            cmsTab.setType(type);
            cmsTab.setChannelId(channelId);
            cacheList = cmsTabMapper.selectCmsTabList(cmsTab);
            if( ! cacheList.isEmpty()){
                redisCache.setCacheList(tabKey,cacheList);
            }
        }
        return cacheList;
    }


}
