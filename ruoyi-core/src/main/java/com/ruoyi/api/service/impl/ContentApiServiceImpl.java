package com.ruoyi.api.service.impl;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.api.service.IBannerApiService;
import com.ruoyi.api.service.IContentApiService;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.core.domain.*;
import com.ruoyi.core.domain.vo.CmsApkInfoVo;
import com.ruoyi.core.domain.vo.CmsMoviesVo;
import com.ruoyi.core.domain.vo.IntroductionVo;
import com.ruoyi.core.enums.ClickTypeEnums;
import com.ruoyi.core.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class ContentApiServiceImpl implements IContentApiService {

    @Autowired
    private RedisCache redisCache;
    @Autowired
    private CmsMoviesMapper cmsMoviesMapper;
    @Autowired
    private CmsBroadcastInfoMapper cmsBroadcastInfoMapper;
    @Autowired
    private CmsApkInfoMapper cmsApkInfoMapper;
    @Autowired
    private CmsMoviesTypeMapper cmsMoviesTypeMapper;
    @Autowired
    private CmsMtypeMapper cmsMtypeMapper;
    @Autowired
    private CmsEpisodeMapper cmsEpisodeMapper;

    private static final Integer TIMEOUT = RedisKey.TIMEOUT * 10 ;

    @Override
    public CmsMoviesVo getMoviesInfo(Long id) {
        String moviesKey = RedisKey.getMoviesKey(id);
        CmsMoviesVo cmsMoviesVo = redisCache.getCacheObject(moviesKey);
        if(StringUtils.isNull(cmsMoviesVo)){
            CmsMovies cmsMovies = cmsMoviesMapper.selectCmsMoviesById(id);
            if(StringUtils.isNull(cmsMovies)){
                return null ;
            }
            cmsMoviesVo = new CmsMoviesVo();
            BeanUtils.copyBeanProp(cmsMoviesVo,cmsMovies);
            //简介
            List<IntroductionVo> introductionVos = JSON.parseArray(cmsMovies.getIntroduction(), IntroductionVo.class);
            cmsMoviesVo.setIntroduction(introductionVos);

            //专辑的剧集信息
            CmsEpisode cmsEpisode = new CmsEpisode();
            cmsEpisode.setMoviesId(id);
            List<CmsEpisode> cmsEpisodes = cmsEpisodeMapper.selectCmsEpisodeList(cmsEpisode);
            cmsMoviesVo.setEpisodeList(cmsEpisodes);


            //放入缓存
            redisCache.setCacheObject(moviesKey,cmsMoviesVo);
        }
        //某个影视剧的类型信息
        List<CmsMtype> mtypeList = getMtypeByMoviesId(id);
        cmsMoviesVo.setMtypeList(mtypeList);
        return cmsMoviesVo;
    }

    /**
     * 某个影视剧的类型信息
     * @param moviesId
     * @return
     */
    private List<CmsMtype> getMtypeByMoviesId(Long moviesId){
        String moviesTypeKey = RedisKey.getMoviesTypeKey(moviesId);
        List<CmsMtype> cacheList = redisCache.getCacheList(moviesTypeKey);
        if(cacheList.isEmpty()){
            //影视剧类型字典信息
            List<CmsMtype> mtypeList = getCmsMType();
            //某个影视剧的类型
            CmsMoviesType cmsMoviesType = new CmsMoviesType();
            cmsMoviesType.setMoviesId(moviesId);
            List<CmsMoviesType> cmsMoviesTypes = cmsMoviesTypeMapper.selectCmsMoviesTypeList(cmsMoviesType);
            List<Long> typeIds = cmsMoviesTypes.stream().map(a -> a.getTypeId()).collect(Collectors.toList());
            cacheList = mtypeList.stream().filter(a -> typeIds.contains(a.getId())).collect(Collectors.toList());
            //放入缓存
            redisCache.setCacheList(moviesTypeKey , cacheList);
//            redisCache.expire(moviesTypeKey,TIMEOUT , TimeUnit.MINUTES);
        }

        return cacheList;
    }

    /**
     * 影视剧类型信息
     * @return
     */
    private List<CmsMtype> getCmsMType(){
        String moviesTypeKey = RedisKey.getMTypeKey();
        List<CmsMtype> cacheList = redisCache.getCacheList(moviesTypeKey);
        if(cacheList.isEmpty()){
            cacheList = cmsMtypeMapper.selectCmsMtypeList(null);
            if( ! cacheList.isEmpty()){
                redisCache.setCacheList(moviesTypeKey , cacheList);
//                redisCache.expire(moviesTypeKey,TIMEOUT , TimeUnit.MINUTES);
            }
        }
        return cacheList;
    }

    /**
     * 直播流详情
     * @return
     */
    @Override
    public CmsBroadcastInfo getCmsBroadcastInfo(Long id) {
        String broadcastKey = RedisKey.getBroadcastKey(id);
        CmsBroadcastInfo cacheObject = redisCache.getCacheObject(broadcastKey);
        if(StringUtils.isNull(cacheObject)){
            cacheObject = cmsBroadcastInfoMapper.selectCmsBroadcastInfoById(id);
            if(StringUtils.isNotNull(cacheObject)){
//                redisCache.setCacheObject(broadcastKey,cacheObject,TIMEOUT  , TimeUnit.MINUTES);
                redisCache.setCacheObject(broadcastKey,cacheObject);
            }
        }
        return cacheObject;
    }
    /**
     * apk的信息
     * @param id
     * @return
     */
    @Override
    public CmsApkInfoVo getCmsApkInfo(Long id) {
        String apkKey = RedisKey.getApkKey(id);
        CmsApkInfoVo cacheObject = redisCache.getCacheObject(apkKey);
        if(StringUtils.isNull(cacheObject)){
            CmsApkInfo cmsApkInfo = cmsApkInfoMapper.selectCmsApkInfoById(id);
            if(StringUtils.isNotNull(cmsApkInfo)){
                cacheObject = cmsApkInfo.vo2CmsApkInfo();
                redisCache.setCacheObject(apkKey,cacheObject);
            }
        }
        return cacheObject;
    }

//    @Override
//    public Object getObjectByClickId(String clickType, Long clickId) {
//        if(clickType.equals(ClickTypeEnums.BROADCAST.getName())){
//            return getCmsBroadcastInfo(clickId);
//        }
//
//        if(clickType.equals(ClickTypeEnums.MOVIES.getName())){
//            return getcms(clickId);
//        }
//
//        if(clickType.equals(ClickTypeEnums.BROADCAST.getName())){
//            return getCmsBroadcastInfo(clickId);
//        }
//
//        return null;
//    }

}
