package com.ruoyi.api.service;

import com.ruoyi.core.domain.CmsApkInfo;
import com.ruoyi.core.domain.CmsBroadcastInfo;
import com.ruoyi.core.domain.CmsMovies;
import com.ruoyi.core.domain.CmsTab;
import com.ruoyi.core.domain.vo.CmsApkInfoVo;
import com.ruoyi.core.domain.vo.CmsMoviesVo;

import java.util.List;

public interface IContentApiService {

    /**
     * 影视剧详情
     * @param id
     * @return
     */
    CmsMoviesVo getMoviesInfo(Long id);

    /**
     * 直播流详情
     * @return
     */
    CmsBroadcastInfo getCmsBroadcastInfo(Long id);

    /**
     * apk的信息
     * @param id
     * @return
     */
    CmsApkInfoVo getCmsApkInfo(Long id);


    /**
     * 推荐，banner ，根据点击类型和点击id获取具体数据信息
     * @param clickType
     * @param clickId
     * @return
     */
//    Object getObjectByClickId(String clickType,Long clickId);

}
