package com.ruoyi.api.service.impl;

import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.api.service.IAppApiService;
import com.ruoyi.api.service.IContentApiService;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.core.domain.CmsApkInfo;
import com.ruoyi.core.domain.CmsApp;
import com.ruoyi.core.domain.CmsAppType;
import com.ruoyi.core.domain.CmsChannel;
import com.ruoyi.core.domain.vo.CmsApkInfoVo;
import com.ruoyi.core.mapper.CmsApkInfoMapper;
import com.ruoyi.core.mapper.CmsAppMapper;
import com.ruoyi.core.mapper.CmsAppTypeMapper;
import com.ruoyi.core.mapper.CmsChannelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class AppApiServiceImpl implements IAppApiService {

    @Autowired
    private CmsAppMapper cmsAppMapper;
    @Autowired
    private CmsAppTypeMapper cmsAppTypeMapper;
    @Autowired
    private CmsApkInfoMapper cmsApkInfoMapper;
    @Autowired
    private CmsChannelMapper cmsChannelMapper;
    @Autowired
    private RedisCache redisCache;
    @Autowired
    private IContentApiService contentApiService;

    private static final int APP_SIZE = 6;

    /**
     * 获取子分类信息
     *
     * @param channelId
     * @param ap
     * @return
     */
    @Override
    public List<CmsAppType> getApList(String channelId, Integer ap) {
        //子分类信息
        String apListKey = RedisKey.getApListKey(ap);
        List<CmsAppType> cacheList = redisCache.getCacheList(apListKey);
        if (cacheList.isEmpty()) {
            CmsAppType cmsAppType = new CmsAppType();
            cmsAppType.setAp(ap);
            cacheList = cmsAppTypeMapper.selectCmsAppTypeList(cmsAppType);
            if (!cacheList.isEmpty()) {
                redisCache.setCacheList(apListKey, cacheList);
                redisCache.expire(apListKey, RedisKey.TIMEOUT, TimeUnit.MINUTES);
            }
        }

        if (!cacheList.isEmpty()) {
            //子分类配置的数据信息
            for (int i = 0; i < cacheList.size(); i++) {
                CmsAppType cmsAppType = cacheList.get(i);
                List<CmsApkInfoVo> appList = getAppList(channelId, cmsAppType.getId());
                // 限制最多显示 APP_SIZE 个应用
                if (appList.size() > APP_SIZE) {
                    appList = appList.stream().limit(APP_SIZE).collect(Collectors.toList());
                }
                cmsAppType.setApkList(appList);
            }
        }
        return cacheList;
    }

    /**
     * 子分类配置的数据信息
     *
     * @param channelId
     * @param apType
     * @return
     */
    @Override
    public List<CmsApkInfoVo> getAppList(String channelId, Long apType) {
        String appInfoKey = RedisKey.getAppListKey(apType, channelId);
        List<CmsApp> cacheList = redisCache.getCacheList(appInfoKey);
        if (cacheList.isEmpty()) {
            //获取自己渠道号的数据，并放入缓存
            cacheList = getAppList2(channelId, apType);
            // 如果自己渠道号下没有数据，则获取默认渠道号下的数据
            if (cacheList.isEmpty()) {
                //获取默认的渠道号的信息
                cacheList = getAppList2(UserConstants.CHANNEL_ID, apType);
                if (!cacheList.isEmpty()) {
                    //把默认渠道号信息放入到自己的缓存
                    redisCache.setCacheList(appInfoKey, cacheList);
                    redisCache.expire(appInfoKey, RedisKey.TIMEOUT, TimeUnit.MINUTES);
                }
            }
        }
        return cacheList.stream().map(a ->
                contentApiService.getCmsApkInfo(a.getApkId())
        ).collect(Collectors.toList());
    }


    /**
     * 子分类配置的数据信息
     *
     * @param channelId
     * @param apType
     * @return
     */
    private List<CmsApp> getAppList2(String channelId, Long apType) {
//        String appInfoKey = RedisKey.getAppInfoKey(appType, UserConstants.CHANNEL_ID);
        String appInfoKey = RedisKey.getAppListKey(apType, channelId);
        List<CmsApp> cacheList = redisCache.getCacheList(appInfoKey);
        if (cacheList.isEmpty()) {
            CmsApp cmsApp = new CmsApp();
            cmsApp.setApType(apType);
            cmsApp.setChannelId(channelId);
            cacheList = cmsAppMapper.selectCmsAppList(cmsApp);
            if (!cacheList.isEmpty()) {
                redisCache.setCacheList(appInfoKey, cacheList);
                redisCache.expire(appInfoKey, RedisKey.TIMEOUT, TimeUnit.MINUTES);
            }
        }
        return cacheList;
    }

    /**
     * 根据子分类查询apk信息
     *
     * @param channelId
     * @param apType
     * @return
     */
    @Override
    public List<CmsApkInfo> getApkListByApType(String channelId, Long apType, Integer start, Integer pageSize) {
        //根据channelId，或者apk的创建者
        CmsChannel cmsChannels = getChannelByChannelId(channelId);

        String appListKey = RedisKey.getAppListKey(apType, channelId, start, pageSize);
        List<CmsApkInfo> cacheList = redisCache.getCacheList(appListKey);
        if (cacheList.isEmpty()) {
            Map<String, Object> map = new HashMap<>();
            map.put("apType", apType);
            map.put("createBy", cmsChannels.getUserName());
            map.put("start", start);
            map.put("pageSize", pageSize);
//            cacheList = cmsApkInfoMapper.selectCmsApkInfoByApi(map);

            List<CmsApp> appList = cmsAppMapper.selectCmsAppPage(map);
            cacheList = appList.stream().map(app -> cmsApkInfoMapper.selectCmsApkInfoById(app.getApkId())).collect(Collectors.toList());

            if (!cacheList.isEmpty()) {
                redisCache.setCacheList(appListKey, cacheList);
                redisCache.expire(appListKey, RedisKey.TIMEOUT, TimeUnit.MINUTES);
            }
        }
        return cacheList;
    }


    private CmsChannel getChannelByChannelId(String channelId) {

        String key = RedisKey.getCmsChannelKey(channelId);
        CmsChannel cacheObject = redisCache.getCacheObject(key);
        if (StringUtils.isNull(cacheObject)) {
            cacheObject = cmsChannelMapper.selectCmsChannelBySn(channelId);
            redisCache.setCacheObject(key, cacheObject);
        }
        return cacheObject;
    }


    @Override
    public List<CmsApkInfo> searchAppList(String keyWord) {
        CmsApkInfo cmsApkInfo = new CmsApkInfo();
        cmsApkInfo.setName(keyWord);
        return cmsApkInfoMapper.selectCmsApkInfoList(cmsApkInfo);
    }
}
