package com.ruoyi.api.service.impl;

import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.api.service.IAppCoreApiService;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.core.domain.CmsCoreVersion;
import com.ruoyi.core.mapper.CmsCoreVersionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AppCoreApiServiceImpl implements IAppCoreApiService {

    @Autowired
    private CmsCoreVersionMapper cmsCoreVersionMapper;
    @Autowired
    private RedisCache redisCache;

    @Override
    public CmsCoreVersion getCoreVersion(Integer typeId) {

        String coreVersion = RedisKey.getCoreVersionKey(typeId);
        CmsCoreVersion cacheObject = redisCache.getCacheObject(coreVersion);
        if(StringUtils.isNull(cacheObject)){
            CmsCoreVersion cmsCoreVersion = new CmsCoreVersion();
            cmsCoreVersion.setTypeId(typeId.longValue());
            List<CmsCoreVersion> cmsCoreVersions = cmsCoreVersionMapper.selectCmsCoreVersionList(cmsCoreVersion);
            if( ! cmsCoreVersions.isEmpty()){
                cmsCoreVersions = cmsCoreVersions.stream().sorted(Comparator.comparing(CmsCoreVersion::getVersionCode).reversed()).collect(Collectors.toList());
                cacheObject = cmsCoreVersions.get(0);
                redisCache.setCacheObject(coreVersion,cacheObject);
            }
        }
        return cacheObject;
    }
}
