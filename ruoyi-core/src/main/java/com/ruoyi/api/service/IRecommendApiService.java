package com.ruoyi.api.service;

import com.ruoyi.core.domain.CmsBanner;
import com.ruoyi.core.domain.CmsRecommend;

import java.util.List;

public interface IRecommendApiService {


    /**
     * banner列表信息
     * @param tabId
     * @return
     */
    List<CmsRecommend> getRecommendList(Long tabId);


}
