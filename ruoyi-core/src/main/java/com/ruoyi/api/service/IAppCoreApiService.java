package com.ruoyi.api.service;

import com.ruoyi.core.domain.CmsCoreVersion;
import com.ruoyi.core.domain.CmsTab;

import java.util.List;

public interface IAppCoreApiService {

    /**
     * 获取最新版本
     * @param typeId
     * @return
     */
    CmsCoreVersion getCoreVersion( Integer typeId);


}
