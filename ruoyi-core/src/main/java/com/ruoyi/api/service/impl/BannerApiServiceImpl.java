package com.ruoyi.api.service.impl;

import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.api.service.IBannerApiService;
import com.ruoyi.api.service.IContentApiService;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.core.domain.CmsApkInfo;
import com.ruoyi.core.domain.CmsBanner;
import com.ruoyi.core.domain.vo.CmsApkInfoVo;
import com.ruoyi.core.enums.ClickTypeEnums;
import com.ruoyi.core.mapper.CmsBannerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class BannerApiServiceImpl implements IBannerApiService {

    @Autowired
    private RedisCache redisCache;
    @Autowired
    private CmsBannerMapper cmsBannerMapper;
    @Autowired
    private IContentApiService contentApiService;

    @Override
    public List<CmsBanner> getBannerList(Long tabId) {
        String bannerKey = RedisKey.getBannerKey(tabId);
        List<CmsBanner> cacheList = redisCache.getCacheList(bannerKey);
        if(cacheList.isEmpty()){
            CmsBanner cmsBanner = new CmsBanner();
            cmsBanner.setTabId(tabId);
            cacheList = cmsBannerMapper.selectCmsBannerList(cmsBanner);
            if( ! cacheList.isEmpty()){
                //排序
                cacheList = cacheList.stream().sorted(Comparator.comparing(CmsBanner::getSeq)).collect(Collectors.toList());
                redisCache.setCacheList(bannerKey,cacheList);
                redisCache.expire(bannerKey,RedisKey.TIMEOUT, TimeUnit.MINUTES);
            }
        }
        if( ! cacheList.isEmpty()){
            //获取apk的数据信息
            for (int i = 0; i < cacheList.size(); i++) {
                CmsBanner cmsBanner = cacheList.get(i);
                if(cmsBanner.getClickType().equals(ClickTypeEnums.APK.getName())){
                    CmsApkInfoVo cmsApkInfo = contentApiService.getCmsApkInfo(cmsBanner.getClickId());
                    cmsBanner.setClickInfo(cmsApkInfo);
                }
            }
        }
        return cacheList;
    }
}
