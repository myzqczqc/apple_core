package com.ruoyi.api.service.impl;

import com.ruoyi.api.constant.RedisKey;
import com.ruoyi.api.service.IContentApiService;
import com.ruoyi.api.service.IRecommendApiService;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.core.domain.CmsApkInfo;
import com.ruoyi.core.domain.CmsBanner;
import com.ruoyi.core.domain.CmsRecommend;
import com.ruoyi.core.domain.vo.CmsApkInfoVo;
import com.ruoyi.core.enums.ClickTypeEnums;
import com.ruoyi.core.mapper.CmsRecommendMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 推荐页信息
 */
@Service
public class RecommendApiServiceImpl implements IRecommendApiService {

    @Autowired
    private CmsRecommendMapper cmsRecommendMapper;
    @Autowired
    private RedisCache redisCache;
    @Autowired
    private IContentApiService contentApiService;

    @Override
    public List<CmsRecommend> getRecommendList(Long tabId) {
        String recommendKey = RedisKey.getRecommendKey(tabId);
        List<CmsRecommend> cacheList = redisCache.getCacheList(recommendKey);
        if(cacheList.isEmpty()){
            CmsRecommend cmsRecommend = new CmsRecommend();
            cmsRecommend.setTabId(tabId);
            cacheList = cmsRecommendMapper.selectCmsRecommendList(cmsRecommend);
            if( ! cacheList.isEmpty()){
                //排序
                cacheList = cacheList.stream().sorted(Comparator.comparing(CmsRecommend::getSeq)).collect(Collectors.toList());
                redisCache.setCacheList(recommendKey , cacheList);
                redisCache.expire(recommendKey,RedisKey.TIMEOUT, TimeUnit.MINUTES);
            }

        }
        if( ! cacheList.isEmpty()){
            //获取apk的数据信息
            for (int i = 0; i < cacheList.size(); i++) {
                CmsRecommend cmsRecommend = cacheList.get(i);
                if(cmsRecommend.getClickType().equals(ClickTypeEnums.APK.getName())){
                    CmsApkInfoVo cmsApkInfo = contentApiService.getCmsApkInfo(cmsRecommend.getClickId());
                    cmsRecommend.setClickInfo(cmsApkInfo);
                }
            }
        }
        return cacheList;
    }
}
