package com.ruoyi.api.service;

import com.ruoyi.core.domain.CmsBanner;

import java.util.List;

public interface IBannerApiService {


    /**
     * banner列表信息
     * @param tabId
     * @return
     */
    List<CmsBanner> getBannerList(Long tabId);


}
