package com.intelliset.apk.parser.struct.resource;

import com.intelliset.apk.parser.struct.ChunkHeader;
import com.intelliset.apk.parser.struct.ChunkType;

public class NullHeader extends ChunkHeader {
    public NullHeader(int headerSize, int chunkSize) {
        super(ChunkType.NULL, headerSize, chunkSize);
    }
}
