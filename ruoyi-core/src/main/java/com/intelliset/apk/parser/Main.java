package com.intelliset.apk.parser;

import com.intelliset.apk.parser.bean.ApkMeta;

import java.io.File;
import java.io.IOException;

public class Main {


    public static void main(String[] args)
    {
        // APK文件路径
        String apkFilePath = "C:\\Users\\86159\\Downloads\\123\\movie1.0.1.apk";

        try {
            ApkParser apkParser = new ApkParser(new File(apkFilePath));

            ApkMeta apkMeta = apkParser.getApkMeta();

            String packageName = apkMeta.getPackageName();
            System.out.println("Package Name: " + packageName);

            String versionName = apkMeta.getVersionName();
            System.out.println("Version Name: " + versionName);

            String iconPath = apkMeta.getIcon();
            System.out.println("Icon Path: " + iconPath);

            String name = apkMeta.getName();
            System.out.println("name: " + name);

            System.out.println("getVersionCode:"+apkMeta.getVersionCode());

            ApkFile apkFile = new ApkFile(new File(apkFilePath));
            byte[] iconData = apkFile.getFileData(iconPath);

            System.out.println("iconData: " + iconData.length);


            FileUtil.writeFileData("C:\\Users\\86159\\Downloads\\lll.png",iconData);

            apkParser.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
