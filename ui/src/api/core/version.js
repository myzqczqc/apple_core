import request from '@/utils/request'

// 查询core应用历史版本信息列表
export function listVersion(query) {
  return request({
    url: '/core/version/list',
    method: 'get',
    params: query
  })
}

// 查询core应用历史版本信息详细
export function getVersion(id) {
  return request({
    url: '/core/version/' + id,
    method: 'get'
  })
}

// 新增core应用历史版本信息
export function addVersion(data) {
  return request({
    url: '/core/version',
    method: 'post',
    data: data
  })
}

// 修改core应用历史版本信息
export function updateVersion(data) {
  return request({
    url: '/core/version',
    method: 'put',
    data: data
  })
}

// 删除core应用历史版本信息
export function delVersion(id) {
  return request({
    url: '/core/version/' + id,
    method: 'delete'
  })
}
