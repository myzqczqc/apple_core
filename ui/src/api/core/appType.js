import request from '@/utils/request'

// 查询app子分类列表
export function listAppType(query) {
  return request({
    url: '/core/appType/list',
    method: 'get',
    params: query
  })
}

// 查询app子分类详细
export function getAppType(id) {
  return request({
    url: '/core/appType/' + id,
    method: 'get'
  })
}

// 新增app子分类
export function addAppType(data) {
  return request({
    url: '/core/appType',
    method: 'post',
    data: data
  })
}

// 修改app子分类
export function updateAppType(data) {
  return request({
    url: '/core/appType',
    method: 'put',
    data: data
  })
}

// 删除app子分类
export function delAppType(id) {
  return request({
    url: '/core/appType/' + id,
    method: 'delete'
  })
}

// 修改顺序号
export function updateSeq(id, seq) {
  return request({
    url: '/core/appType/updateSeq/' + id + '/' + seq,
    method: 'post'
  })
}
