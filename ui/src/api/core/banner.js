import request from '@/utils/request'

// 查询tab的banner信息列表
export function listBanner(query) {
  return request({
    url: '/core/banner/list',
    method: 'get',
    params: query
  })
}

// 查询tab的banner信息详细
export function getBanner(id) {
  return request({
    url: '/core/banner/' + id,
    method: 'get'
  })
}

// 新增tab的banner信息
export function addBanner(data) {
  return request({
    url: '/core/banner',
    method: 'post',
    data: data
  })
}

// 修改tab的banner信息
export function updateBanner(data) {
  return request({
    url: '/core/banner',
    method: 'put',
    data: data
  })
}

// 删除tab的banner信息
export function delBanner(id) {
  return request({
    url: '/core/banner/' + id,
    method: 'delete'
  })
}


// 修改顺序号
export function updateSeq(id,seq) {
  return request({
    url: '/core/banner/updateSeq/' + id+'/'+seq,
    method: 'post'
  })
}
