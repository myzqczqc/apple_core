import request from '@/utils/request'

// 查询语言字典信息列表
export function listLanguage(query) {
  return request({
    url: '/core/language/list',
    method: 'get',
    params: query
  })
}

// 查询语言字典信息详细
export function getLanguage(id) {
  return request({
    url: '/core/language/' + id,
    method: 'get'
  })
}

// 新增语言字典信息
export function addLanguage(data) {
  return request({
    url: '/core/language',
    method: 'post',
    data: data
  })
}

// 修改语言字典信息
export function updateLanguage(data) {
  return request({
    url: '/core/language',
    method: 'put',
    data: data
  })
}

// 删除语言字典信息
export function delLanguage(id) {
  return request({
    url: '/core/language/' + id,
    method: 'delete'
  })
}
