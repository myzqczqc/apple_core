import request from '@/utils/request'

// 查询剧集信息列表
export function listEpisode(query) {
  return request({
    url: '/core/episode/list',
    method: 'get',
    params: query
  })
}

// 查询剧集信息详细
export function getEpisode(id) {
  return request({
    url: '/core/episode/' + id,
    method: 'get'
  })
}

// 新增剧集信息
export function addEpisode(data) {
  return request({
    url: '/core/episode',
    method: 'post',
    data: data
  })
}

// 修改剧集信息
export function updateEpisode(data) {
  return request({
    url: '/core/episode',
    method: 'put',
    data: data
  })
}

// 删除剧集信息
export function delEpisode(id) {
  return request({
    url: '/core/episode/' + id,
    method: 'delete'
  })
}

// 修改顺序号
export function updateSeq(id,seq) {
  return request({
    url: '/core/episode/updateSeq/' + id+'/'+seq,
    method: 'post'
  })
}
