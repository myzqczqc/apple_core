import request from '@/utils/request'

// 查询app配置列表
export function listCmsApp(query) {
  return request({
    url: '/core/cmsApp/list',
    method: 'get',
    params: query
  })
}

// 查询app配置详细
export function getCmsApp(id) {
  return request({
    url: '/core/cmsApp/' + id,
    method: 'get'
  })
}

// 新增app配置
export function addCmsApp(data) {
  return request({
    url: '/core/cmsApp',
    method: 'post',
    data: data
  })
}

// 修改app配置
export function updateCmsApp(data) {
  return request({
    url: '/core/cmsApp',
    method: 'put',
    data: data
  })
}
// 修改app配置
export function updateCmsApps(data) {
  return request({
    url: '/core/cmsApp/updateRelations',
    method: 'put',
    data: data
  })
}
// 删除app配置
export function delCmsApp(id) {
  return request({
    url: '/core/cmsApp/' + id,
    method: 'delete'
  })
}
