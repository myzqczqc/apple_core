import request from '@/utils/request'

// 查询核心应用信息列表
export function listCore(query) {
  return request({
    url: '/core/core/list',
    method: 'get',
    params: query
  })
}

// 查询核心应用信息详细
export function getCore(id) {
  return request({
    url: '/core/core/' + id,
    method: 'get'
  })
}

// 新增核心应用信息
export function addCore(data) {
  return request({
    url: '/core/core',
    method: 'post',
    data: data
  })
}

// 修改核心应用信息
export function updateCore(data) {
  return request({
    url: '/core/core',
    method: 'put',
    data: data
  })
}

// 删除核心应用信息
export function delCore(id) {
  return request({
    url: '/core/core/' + id,
    method: 'delete'
  })
}
