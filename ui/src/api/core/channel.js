import request from '@/utils/request'

// 查询渠道商管理列表
export function listChannel(query) {
  return request({
    url: '/core/channel/list',
    method: 'get',
    params: query
  })
}

// 查询渠道商管理详细
export function getChannel(id) {
  return request({
    url: '/core/channel/' + id,
    method: 'get'
  })
}

// 新增渠道商管理
export function addChannel(data) {
  return request({
    url: '/core/channel',
    method: 'post',
    data: data
  })
}

// 修改渠道商管理
export function updateChannel(data) {
  return request({
    url: '/core/channel',
    method: 'put',
    data: data
  })
}

// 删除渠道商管理
export function delChannel(id) {
  return request({
    url: '/core/channel/' + id,
    method: 'delete'
  })
}
