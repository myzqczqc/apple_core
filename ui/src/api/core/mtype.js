import request from '@/utils/request'

// 查询影视类型列表
export function listMtype(query) {
  return request({
    url: '/core/mtype/list',
    method: 'get',
    params: query
  })
}

// 查询影视类型详细
export function getMtype(id) {
  return request({
    url: '/core/mtype/' + id,
    method: 'get'
  })
}

// 新增影视类型
export function addMtype(data) {
  return request({
    url: '/core/mtype',
    method: 'post',
    data: data
  })
}

// 修改影视类型
export function updateMtype(data) {
  return request({
    url: '/core/mtype',
    method: 'put',
    data: data
  })
}

// 删除影视类型
export function delMtype(id) {
  return request({
    url: '/core/mtype/' + id,
    method: 'delete'
  })
}
