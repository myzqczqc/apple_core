import request from '@/utils/request'

// 查询tab信息列表
export function listTab(query) {
  return request({
    url: '/core/tab/list',
    method: 'get',
    params: query
  })
}

// 查询tab信息详细
export function getTab(id) {
  return request({
    url: '/core/tab/' + id,
    method: 'get'
  })
}

// 新增tab信息
export function addTab(data) {
  return request({
    url: '/core/tab',
    method: 'post',
    data: data
  })
}

// 修改tab信息
export function updateTab(data) {
  return request({
    url: '/core/tab',
    method: 'put',
    data: data
  })
}

// 删除tab信息
export function delTab(id) {
  return request({
    url: '/core/tab/' + id,
    method: 'delete'
  })
}
