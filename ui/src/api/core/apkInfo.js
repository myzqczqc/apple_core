import request from '@/utils/request'

// 查询apk信息列表
export function listApkInfo(query) {
  return request({
    url: '/core/apkInfo/list',
    method: 'get',
    params: query
  })
}

// 查询apk信息详细
export function getApkInfo(id) {
  return request({
    url: '/core/apkInfo/' + id,
    method: 'get'
  })
}

// 新增apk信息
export function addApkInfo(data) {
  return request({
    url: '/core/apkInfo',
    method: 'post',
    data: data
  })
}

// 修改apk信息
export function updateApkInfo(data) {
  return request({
    url: '/core/apkInfo',
    method: 'put',
    data: data
  })
}

// 删除apk信息
export function delApkInfo(id) {
  return request({
    url: '/core/apkInfo/' + id,
    method: 'delete'
  })
}
