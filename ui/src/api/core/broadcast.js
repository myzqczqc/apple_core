import request from '@/utils/request'

// 查询直播流信息列表
export function listBroadcast(query) {
  return request({
    url: '/core/broadcast/list',
    method: 'get',
    params: query
  })
}

// 查询直播流信息详细
export function getBroadcast(id) {
  return request({
    url: '/core/broadcast/' + id,
    method: 'get'
  })
}

// 新增直播流信息
export function addBroadcast(data) {
  return request({
    url: '/core/broadcast',
    method: 'post',
    data: data
  })
}

// 修改直播流信息
export function updateBroadcast(data) {
  return request({
    url: '/core/broadcast',
    method: 'put',
    data: data
  })
}

// 删除直播流信息
export function delBroadcast(id) {
  return request({
    url: '/core/broadcast/' + id,
    method: 'delete'
  })
}
