import request from '@/utils/request'

// 查询影视数据信息列表
export function listMovies(query) {
  return request({
    url: '/core/movies/list',
    method: 'get',
    params: query
  })
}

// 查询影视数据信息详细
export function getMovies(id) {
  return request({
    url: '/core/movies/' + id,
    method: 'get'
  })
}

// 新增影视数据信息
export function addMovies(data) {
  return request({
    url: '/core/movies',
    method: 'post',
    data: data
  })
}

// 修改影视数据信息
export function updateMovies(data) {
  return request({
    url: '/core/movies',
    method: 'put',
    data: data
  })
}

// 删除影视数据信息
export function delMovies(id) {
  return request({
    url: '/core/movies/' + id,
    method: 'delete'
  })
}
