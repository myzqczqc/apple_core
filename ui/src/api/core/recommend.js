import request from '@/utils/request'

// 查询tab的recommend信息列表
export function listRecommend(query) {
  return request({
    url: '/core/recommend/list',
    method: 'get',
    params: query
  })
}

// 查询tab的recommend信息详细
export function getRecommend(id) {
  return request({
    url: '/core/recommend/' + id,
    method: 'get'
  })
}

// 新增tab的recommend信息
export function addRecommend(data) {
  return request({
    url: '/core/recommend',
    method: 'post',
    data: data
  })
}

// 修改tab的recommend信息
export function updateRecommend(data) {
  return request({
    url: '/core/recommend',
    method: 'put',
    data: data
  })
}

// 删除tab的recommend信息
export function delRecommend(id) {
  return request({
    url: '/core/recommend/' + id,
    method: 'delete'
  })
}
// 修改顺序号
export function updateSeq(id,seq) {
  return request({
    url: '/core/recommend/updateSeq/' + id+'/'+seq,
    method: 'post'
  })
}
