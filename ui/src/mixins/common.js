/**
 * 常用方法
 */

const mixin = {
  methods: {
    //判断字符中是否包含中文
    containsChineseCharacters(str) {
      if(!str){
        return false;
      }
      for (var i = 0; i < str.length; i++) {
        var charCode = str.charCodeAt(i);
        if (charCode >= 0x4e00 && charCode <= 0x9fa5) {
          return true;
        }
      }
      return false;
    },
  }
}
export default mixin;
